<xmp>
Add to the framework the possibility for defining new tags
-----------------------------------------------------------

    Like this:

        <TagDefinition name="Tag1">
                <Attrib name="attr1" />
                <Attrib name="attr2" default="val1"/>
            <!--# the template of the tag  #-->
            attr1='{{attr1}}'<br />
            attr2='{{attr2}}'<br />
            Tag Body:
            <xmp> {{ElementContent}} </xmp>
            <p>
              The variable tplVar='{{tplVar}}'
              is declared in the PHP code of
              the tag definition.
            </p>
            <!--# . . . . . . . . . . . #-->
        </TagDefinition>

    Tag declarations:

        <Tag1 attr1="val1" attr2="val2">
           <!--# element content here #-->
        </Tag1>

        <Tag1 attr1="xxxx">
           <!--# element content here #-->
        </Tag1>

    The PHP code of the tag definition (file 'Tag1.php'):
        <?
          class Tag1 extends Tag
          {
            function onParse()
              {
              }
            function onRender()
              {
                WebApp::addVar("tplVar", "*******");
              }
          }
        ?>

    Example:
        <TagDefinition name="Box">
                <Attrib name="title" />
                <Attrib name="border_color" default="#aaaaff" />
                <Attrib name="body_color" default="white" />
            <table border="0" cellspacing="1" cellpadding="3" bgcolor="{{border_color}}">
              <tr>
                <td align="middle">
                  <strong>{{title}}</strong>
                </td>
              </tr>
              <tr>
                <td bgcolor="{{body_color}}">
                  {{ElementContent}}
                </td>
              </tr>
            </table>
        </TagDefinition> 

        <Box title="Select By Status">
          <Repeat rs="status"> 
            <input type="checkbox" name="{{status_name}}"
                   value="{{status_id}}" onClick="update_selectedStatus()">
            {{status_name}}
          </Repeat>
        </Box>

        <Box title="Test Box" border_color="blue">
          <p>
            This is the <b>content</b> of the element (tag) <Box>.
          </p>
          <p>
            It will be displayed inside a <b>blue</b> box, with 
            a blue border and a blue title bar, while the background
            of the body will be <b>white</b> (the default value).
          </p>
          <p>
            The title in the titlebar will be "Test Box".
          </p>
        </Box>

    The <TagDefinition> will be like a template that will be
    used to transform a simple tag (the tag that it defines) into
    a more complex HTML code. This will hopefully make easier the
    work of graphical designer and web developer. It will also be
    a step towards using XML content in the application (the
    transformation to HTML will be done by the framework using the
    tag definitions for each tag of the XML (for each tag mentioned
    in the DTD of the document).

    So, while the <Repeat> etc. are usually used to display DB content
    (to convert a query result into HTML), the <TagDefinition> can be
    used to display XML content (to transform an XML content to HTML).
</xmp>
