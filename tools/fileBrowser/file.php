<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package     tools
   * @subpackage  fileBrowser
   */
class file extends WebObject
{
  function init()
  {
    $current_path = WebApp::getVar("./");
    WebApp::setSVar("fileView->file", $current_path."sample.html");
  }

  function onParse()
  {
    global $event;
    if ($event->target=='folderListing' and $event->name=='view_file')
      {
        $file = WebApp::getSVar('folderListing->selectedFile');
        WebApp::setSVar('fileView->file', $file);
      }
  }

  function onRender()
  {
    $tpl_file = WebApp::getSVar("fileView->file");
    $this->load_cfg_file($tpl_file);
    $file = ereg_replace("^".APP_PATH, '', $tpl_file);
    WebApp::addVar("file", $file);
  }
  
  function load_cfg_file($tpl_file)
  {
    $path = dirname($tpl_file);
    $fname = basename($tpl_file);
    if (!ereg("\.html?$", $fname))
      {
        //not a template file
        return;
      }

    //get the config file name
    $fname = ereg_replace("\.html?$", ".cfg.php", $fname);
    $cfg_file = $path."/".$fname;

    //load the config file (if it exists)
    if (file_exists($cfg_file))
      {
        include $cfg_file;
        $func_name = ereg_replace("\.html?$", "", basename($tpl_file));
        $func_name .= "_config";
        $func_name(); //call the config function
      }
  }
}
?>
