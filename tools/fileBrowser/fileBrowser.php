<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */
 
  /**
   * The 'fileBrowser' tool of WebApp is a WebBox which is used to 
   * preview the templates and WebBox-es of a WebApp application.
   * It allows the users to browse the folders of the application
   * and to display the files. The files are displayed according
   * to their types, e.g. a php file is displayed as code, an image
   * file is displayed as an image, an HTML file can be displayed
   * both as HTML code and as rendered HTML, etc.
   *
   * @package tools
   * @subpackage fileBrowser
   */
class fileBrowser extends WebObject
{
  function on_file($event_args)
  {
    $root = WebApp::getSVar("folderListing->root");
    $path = WebApp::getSVar("folderListing->currentPath");
    $tpl_file = $event_args["file_name"];
    WebApp::setSVar("fileView->file", $root.$path.$tpl_file);
  }
}
?>