#!/usr/bin/php -q
<?php
   /**
    * @package    misc
    * @subpackage convert
    */

include 'class.Template.php';
include 'class.Parser.php';
include 'class.Render.php';

$tpl_collection = array();
$parser = new Parser;
$render = new Render;

for ($i=1; $i < $argc; $i++)
  {
    $fname = $argv[$i];
    print "$fname\n";

    $tpl = $parser->parse_file($fname);
    $render->render_tpl($tpl);
    $tpl_str = $render->get_result();
    $tpl_str = reformat($tpl_str);

    //backup and write the file
    print shell_exec("cp $fname $fname.orig");
    $fp = fopen($fname, 'w');
    fputs($fp, $tpl_str);
    fclose($fp);
    //print $tpl_str;
  }

function reformat($str)
{
  $str = str_replace('<>', '!=', $str);

  //match any starting or ending tag and make it lowercase
  $pattern = '#</?(\w+)[^<>]*>#';
  $str = preg_replace_callback($pattern, 'lowercase', $str);

  //handle empty tags
  $empty_tag = '(br|hr|img|input|meta|link|include|parameter)';
  $pattern = '#(<'.$empty_tag.'([^>]*[^/-])?)>#';
  $str = preg_replace($pattern, '$1 />', $str);

  //match any attribute name and make it lowercase
  $pattern = '#\s+(\w+)\s*=\s*(["\'])[^"\']*["\']#';
  $str = preg_replace_callback($pattern, 'lowercase', $str);

  //replace {{checked}} by checked="{{checked}}"
  $pattern = '#(<input[^<>]+type=.(checkbox|radio)[^<>]+\s)(\{\{[^\}]+\}\})#';
  $str = preg_replace($pattern, '${1}checked="$3"', $str);

  //replace {{selected}} by selected="{{selected}}"
  $pattern = '#(<option[^<>]+)(\{\{[^\}]+\}\})#';
  $str = preg_replace($pattern, '${1}selected="$2"', $str);

  //replace compact attributes by their xhtml equivalent
  $compact = '(nowrap|checked|disabled|readonly|selected|noresize)';
  $pattern = '#(<\w+[^<>]+\s+)'.$compact.'(?!\s*=)#';
  $str = preg_replace($pattern, '${1}$2="$2"', $str);

  //replace the xml builtin entities
  $str = preg_replace('#&(lt|gt|quot|apos);#', '&amp;$1;', $str);
  //convert also &amp; but not those that are
  //the result of a previous conversion
  $str = preg_replace('#&amp;(?!(amp|lt|gt|apos);)#', '&amp;amp;', $str);

  return $str;
}

function lowercase($matches)
{
  $str = str_replace($matches[1], strtolower($matches[1]), $matches[0]);
  return $str;
}
?>