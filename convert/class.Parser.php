<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package    misc
   * @subpackage convert
   */
class Parser
{
  /** A stack of templates that are being parsed. */
  var $tpl_stack;       

  /** The template that is being parsed currently. */
  var $current_tpl;     

  /** file pointer */
  var $fp;

  function Parser()
  {
    $this->tpl_stack = array();
    $this->current_tpl = UNDEFINED;
  }

  function parse_file($filename)
  {   
    if ( !file_exists($filename) )
      {
        print "'$filename' not found\n";
        return UNDEFINED;  
      }

    $tpl = new Template;
      
    $this->fp = fopen($filename, "r");  //open file

    //parse the file into $tpl
    $this->push($tpl);
    $this->parse();
    $tpl = $this->pop();

    //close the file
    fclose($this->fp);

    return $tpl;
  }

  /**
   * Loads the current template from
   * the file that is being parsed currently.
   */
  function parse()
  {
    $closing_tag = false;
    while (!$closing_tag and !feof($this->fp))
      {
        $line = $this->get_line();
        if ($line=='')  continue;
        $closing_tag = $this->parse_line($line);
      }
  }

  /**
   * Return true when the line is the closing tag of a template;
   * if the line contains the opening tag of another template, call
   * a function to parse this template, else add the line to the
   * current template.
   */
  function parse_line($line)
  {
    $tag = Parser::get_tag_name($line);
    switch ($tag)
      {
      default:
        $this->add_line($line);
        break;

      case "<Repeat":
        $this->parse_Repeat($line);
        break;
      case "</Repeat":
        $this->add_line($line);
        return true;
        break;

      case "<Header":
      case "<Footer":
        $this->parse_Repeat_subtpl($line);
        break;
                                
      case "</Header":
      case "</Footer":
        return true;
        break;
      }
    return false;
  }

  /**
   * Make the given $tpl current, what will be
   * parsed from now on goes to this template.
   */
  function push(&$tpl)
  {
    //push the current_tpl on tpl_stack
    array_push($this->tpl_stack, $this->current_tpl);

    //make current the new template
    $this->current_tpl = &$tpl;
  }

  /**
   * The parsing of the current template has finished;
   * make current the top of the stack.
   */
  function pop()
  {
    //pop the last template from tpl_stack into current_tpl
    $tpl = $this->current_tpl;
    $last = count($this->tpl_stack) - 1;
    $this->current_tpl = &$this->tpl_stack[$last];
    array_pop($this->tpl_stack);

    //add the template in the template collection
    global $tpl_collection;
    $tpl_collection[$tpl->id] = $tpl;

    return $tpl;
  }

  function parse_Repeat($line)
  {
    $tpl = new Template('RepeatTpl');
    $tpl->header = new Template;
    $tpl->footer = new Template;

    //set a pointer from this template to the new template
    $this->add_line("##\n");
    $this->add_line($tpl->id."\n");

    //parse the new template into $tpl
    $this->push($tpl);
    $this->add_line($line);
    $this->parse();
    $this->pop();
  }

  /** Parse <Header> and <Footer> */
  function parse_Repeat_subtpl($line)
  {
    $tag = Parser::get_tag_name($line);
    $tag = substr($tag, 1);
    $tpl = new Template;

    $this->push($tpl);
    $this->parse();
    $tpl = $this->pop();

    switch ($tag)
      {
      case "Header":
        $this->current_tpl->header = $tpl;
        break;
      case "Footer":
        $this->current_tpl->footer = $tpl;
        break;
      }
  }

  /** returns a line from the file that is being parsed */
  function get_line()
  {
    $line = fgets($this->fp,896);
    return $line;
  }

  /** add a line to the current template */
  function add_line($line)
  {
    $this->current_tpl->content[] = $line;
  }

  /**
   * If the $line starts with an opening or closing tag
   * ('<tag_name' or '</tag_name') then it returns it,
   * otherwise returns UNDEFINED.
   */
  function get_tag_name($line)
  {
    $line = trim($line);
    if ( ereg("^(</?[^[:space:] >]*)", $line, $regs) )
      {
        return $regs[1];
      }
    else
      {
        return UNDEFINED;
      }
  }
        
  /**
   * Returns the value of the given attribute from the given
   * line (assuming that the line is a start element), or
   * UNDEFINED if not found.
   */
  function get_attr_value($line, $attrib)
  {
    $line = trim($line);
    $pattern = '[[:space:]]+'.$attrib.'[[:space:]]*=[[:space:]]*"([^"]*)"';
    if ( ereg($pattern, $line, $regs) )
      {
        $attr = $regs[1];
        return $attr;
      }
    else
      {
        return UNDEFINED;
      }
  }
}
?>