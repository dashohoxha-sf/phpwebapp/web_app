<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package    misc
   * @subpackage convert
   */
class Render
{
  var $result;

  function Render()
  {
    $this->result = '';
  }

  function get_result()
  {
    $result = $this->result;
    $this->result = '';
    return $result;
  }

  /** Renders the given template including all subtemplates. */
  function render_tpl($tpl)
  {
    global $tpl_collection;;

    $i = 0;
    while ($i < sizeof($tpl->content))
      {
        $line = $tpl->content[$i++];
        if (chop($line)<>"##")
          {
            $this->result .= $line;
          }
        else  //subtemplate indicator found
          {
            //get the subtemplate
            $line = $tpl->content[$i++];
            $subtpl_id = trim($line);
            $subtpl = $tpl_collection[$subtpl_id];

            //render the subtemplate
            if ($subtpl->type=='RepeatTpl')
              $this->render_RepeatTpl($subtpl);
            else
              $this->render_tpl($subtpl);
          }
      }
  }

  function render_RepeatTpl($tpl)
  {
    $header = $tpl->header;
    $footer = $tpl->footer;
    if (sizeof($header->content) > 0 or sizeof($footer->content) > 0)
      {
        //decrease the indentation of header
        for ($i=0; $i < sizeof($header->content); $i++)
          {
            $line = $header->content[$i];
            $line = ereg_replace('^    ', '', $line);
            $header->content[$i] = $line;
          }
        //decrease the indentation of footer
        for ($i=0; $i < sizeof($footer->content); $i++)
          {
            $line = $footer->content[$i];
            $line = ereg_replace('^    ', '', $line);
            $footer->content[$i] = $line;
          }
        //increase the indentation of <Repeat> and <RepeatBody>
        for ($i=0; $i < sizeof($tpl->content); $i++)
          {
            $line = $tpl->content[$i];
            if (ereg('^ *</?Repeat', $line))
              {
                $tpl->content[$i] = '  '.$line;
              }
          }
      }

    $this->render_tpl($header);
    $this->render_tpl($tpl);
    $this->render_tpl($footer);
  }
}
?>