<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * @package     components
   * @subpackage  folderEditing
   */
class folderEditing extends WebObject
{
  function eventHandler($event)
  {
    global $session;
    extract($event->args);
        
    $root = WebApp::getSVar('folderListing->root');
    $path = WebApp::getSVar('folderListing->currentPath');
    $currentFolder = $root.$path;
    switch ($event->name)
      {
      case "rename":
        $fromFolder = $currentFolder.$fromFolder;
        $toFolder = $currentFolder.$toFolder;
        rename($fromFolder, $toFolder);
        break;
      case "delete":
        rmdir($currentFolder.$delFolder);
        break;
      case "addNew":
        mkdir($currentFolder.$newFolder, 0700);
        break;
      }
  }
}
?>