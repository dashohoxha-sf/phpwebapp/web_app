<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * The 'docbook' webbox is used just to display the cache (HTML) files
   * of a DocBookWiki application. It is useful for including a docbook
   * document in an application.
   *
   * If the state var 'docbook->webnotes' is 'true', then webnotes
   * will be appended at the end of each section.
   *
   * @package     components
   * @subpackage  docbook
   */
class docbook extends WebObject
{
  function init()
  {
    $this->addSVar('cache_path', UNDEFINED);
    $this->addSVar('book_id', UNDEFINED);
    $this->addSVar('node_path', './');
    $this->addSVar('lng', 'en');

    $this->addSVar('webnotes', 'false');
  }

  function on_set_node($event_args)
  {
    $node_path = $event_args['node_path'];
    $this->setSVar('node_path', $node_path);
  }

  function onParse()
  {
    $cache_path = $this->getSVar('cache_path');
    $book_id = $this->getSVar('book_id');
    $node_path = $this->getSVar('node_path');
    $lng = $this->getSVar('lng');

    //add the variables {{content_html}} and {{subnodes_html}}
    $book_path = $cache_path.$book_id.'/'.$lng.'/';
    $content_html = $book_path.$node_path."content.html";
    $subnodes_html = $book_path.$node_path."subnodes.html";
    WebApp::addVars(compact('content_html', 'subnodes_html'));

    //set webnotes->page_id
    $this->set_webnotes_pageid();
  }

  /** set the state variable webnotes->page_id */
  function set_webnotes_pageid()
  {
    $webnotes = $this->getSVar('webnotes');
    if ($webnotes=='false')
      {
        WebApp::setSVar('webnotes->page_id', '');
      }
    else
      {
        $book_id = $this->getSVar('book_id');
        $node_path = $this->getSVar('node_path');
        $lng = $this->getSVar('lng');
        ereg('([^/]+)/$', $node_path, $regs);
        $node_id = $regs[1];
        $page_id = "$book_id/$node_id/$lng";
        WebApp::setSVar('webnotes->page_id', $page_id);
      }
  }

  function onRender()
  {
    //add navigation variables
    $node_path = $this->getSVar('node_path');
    $vars = $this->get_arr_navigation($node_path);
    if ($vars['this_full_title']=='') 
      $vars['this_full_title'] = T_("Table Of Contents");
    WebApp::addVars($vars);
    WebApp::addVar('info_path', './INFO/');
    WebApp::addVar('toc_path', './');
  }

  /**
   * Reads from cache 'navigation.txt' for the current node and 
   * returns an associative array with the template variables
   * this_path, next_path, prev_path, up_path, 
   * this_title, next_title, prev_title, up_title.
   */
  function get_arr_navigation()
  {
    $cache_path = $this->getSVar('cache_path');
    $book_id = $this->getSVar('book_id');
    $node_path = $this->getSVar('node_path');
    $lng = $this->getSVar('lng');

    $arr_navigation = array();

    //get the navigation file
    $book_path = $cache_path.$book_id.'/'.$lng.'/';
    $fname = $book_path.$node_path."navigation.txt";
    if (!file_exists($fname))
      {
        $arr_navigation['next_path'] = './';
        $arr_navigation['prev_path'] = './';
        $arr_navigation['up_path']   = './';
        return $arr_navigation;
      }

    //open it
    $lines = file($fname);

    //parse it
    for ($i=0; $i < sizeof($lines); $i++)
      {
        $line = $lines[$i];
        list($var_name,$var_value) = split('=', $line, 2);
        if ($var_name=='')  continue;
        $arr_navigation[$var_name] = trim($var_value);
      }

    return $arr_navigation;
  }
}
?>
