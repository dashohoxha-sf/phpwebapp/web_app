<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

/**
 * The webbox 'webnotes' can be used to append comments or notes
 * to documentation pages or to other web pages. 
 * This component uses the database; the connection parameters are at 
 * 'webnotes/DB.php'.
 * The application or the webbox that includes it should also give a
 * value to the state variable 'webnotes->page_id', which is used to
 * select from the database the notes that will be displayed.
 *
 * @package     components
 * @subpackage  webnotes
 */
class webnotes extends WebObject
{
  function webnotes()
  {
    /** include DB support */
    $page_id = WebApp::getSVar('webnotes->page_id');
    if ($page_id != '') 
      {
	global $cnn;
	if (!isset($cnn))
	  {
	    //include database classes 
	    include_once DB_PATH."class.MySQLCnn.php";
	    include_once DB_PATH."class.Recordset.php";
	    include_once DB_PATH."class.EditableRS.php";
	    include_once DB_PATH."class.PagedRS.php";
	  }
	//set the connection
	$mdb2_dsn = WebApp::getSVar('webnotes->dsn');
	$mdb2_options = array(
			      'debug'       => 2,
			      'portability' => MDB2_PORTABILITY_ALL,
			      );
	$this->setConnection($mdb2_dsn, $mdb2_options);
      }
  }

  function init()
  {
    //list(display) only the notes related to this page
    $this->addSVar('page_id', '');

    //used to show/hide webnotes, can be changed by the users
    $this->addSVar('visible', 'true');

    //if true, then the new notes that are submitted
    //are approved automatically
    $this->addSVar('unmoderated', 'true');

    //if true, then a notification email will be sent
    //for each note that needs to be approved
    $this->addSVar('notify', 'false');

    //a comma separated list of emails to which to send notifications
    $this->addSVar('emails', '');

    //if true, then notes can be approved/moderated
    $this->addSVar('approve', 'false');

    //notes can be deleted and the admin interface is accessible
    $this->addSVar('admin', 'false');

    //an SQL filter about the pages or notes that
    //can be administered in the admin interface
    $this->addSVar('admin_pageid_filter', '1=1');

    //DB parameters (in case that the parent application does not use DB)
    $this->addSVar('dbhost', 'localhost');
    $this->addSVar('dbuser', 'root');
    $this->addSVar('dbpasswd', '');
    $this->addSVar('dbname', 'webnotes');
  }

  function on_set_visible($event_args)
  {
    $visible = $event_args['visible'];
    $this->setSVar('visible', $visible);
  }

  function on_next($event_args)
  {
    $page = $event_args["page"];
    WebApp::setSVar("get_all_page_notes->current_page", $page);
    WebApp::setSVar("get_approved_page_notes->current_page", $page);
  }

  function on_set_status($event_args)
  {
    WebApp::execDBCmd("set_note_status", $event_args);
  }

  function on_delete($event_args)
  {
    $admin = $this->getSVar('admin');
    if ($admin=='true')
      {
        WebApp::execDBCmd("delete_note", $event_args);
      }
    else
      {
        $msg = T_("You can delete a note only if you have admin rights!");
        WebApp::message($msg);
      }
  }

  function onRender()
  {
    $page_id = WebApp::getSVar('webnotes->page_id');
    if ($page_id == '') return;

    //add {{notes_rs}}
    $approve = $this->getSVar('approve');
    $admin = $this->getSVar('admin');
    if ($approve=='true' or $admin=='true')
      {
        $notes_rs = "get_all_page_notes";
      }
    else
      {
        $notes_rs = "get_approved_page_notes";
      }
    WebApp::addVar('notes_rs', $notes_rs);
    WebApp::setSVar($notes_rs.'->recount', "true");

    //change the date format
    $rs = WebApp::openRS($notes_rs);
    while (!$rs->EOF())
      {
        $date = $rs->Field('date_modified');
        $date = date('d-M-Y G:i', strtotime($date));
        $rs->setFld('date_modified', $date);
        $rs->MoveNext();
      }
    global $webPage;
    $webPage->addRecordset($rs);

    //add {{admin_page}}
    $admin_page = dirname(__FILE__).'/admin/notesadmin.html';
    WebApp::addVar('admin_page', $admin_page);
  }
}
?>