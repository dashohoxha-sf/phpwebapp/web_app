// -*-C-*-
/*
  This file  is part of phpWebApp,  which is a  framework for building
  web application based on relational databases.

  Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  phpWebApp is free software; you can redistribute it and/or modify it
  under the  terms of the GNU  General Public License  as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  phpWebApp is  distributed in  the hope that  it will be  useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along with phpWebApp; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function set_visible(visible)
{
  SendEvent('webnotes', 'set_visible', 'visible='+visible);
}

function edit_note(note_id)
{
  var event_args = 'note_id='+note_id+';'+'status='+status;
  SendEvent('editnote', 'edit', event_args);
}

function delete_note(note_id)
{
  var msg = T_("You are deleting the note #v_note_id from the database!");
  msg = msg.replace(/v_note_id/, note_id);
  if (confirm(msg))
    {
      SendEvent('webnotes', 'delete', 'note_id='+note_id);
    }
}

function set_note_status(listbox, note_id)
{
  var idx = listbox.selectedIndex;
  var status = listbox.options[idx].value;
  var event_args = 'note_id='+note_id+';'+'status='+status;

  SendEvent('webnotes', 'set_status', event_args);
}

function admin_notes(admin_page)
{
  var win_features = 'innerWidth=790,outerHeight=600,resizable,scrollbars';
  window.open('', 'admin_notes_win', win_features);
  wGoTo('admin_notes_win', admin_page);  
}
