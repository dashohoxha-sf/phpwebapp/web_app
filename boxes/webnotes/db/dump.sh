#!/bin/bash

### modify them as appropriate
host=localhost
user=inima
dbname=webnotes

### dump the database $dbname into the file webnotes_backup.sql
mysqldump --add-drop-table --allow-keyword \
          --complete-insert --extended-insert \
          --compress --host=$host --user=$user -p \
          --result-file=webnotes_backup.sql --databases $dbname

### dump the structure of the database as well
mysqldump --allow-keyword --no-data \
          --host=$host --user=$user -p \
          --result-file=webnotes.sql --databases $dbname

### create the database with the command: 
### $ mysql -h $host -u $user -p -D $dbname < webnotes.sql
###
### restore the database with the command: 
### $ mysql -h $host -u $user -p -D $dbname < webnotes_backup.sql

