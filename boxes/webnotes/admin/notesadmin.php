<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /** include DB support */
include_once dirname(dirname(__FILE__)).'/DB.php';

/**
 * @package     components
 * @subpackage  webnotes
 */
class notesadmin extends WebObject
{
  function on_set_status($event_args)
  {
    WebApp::execDBCmd("set_note_status", $event_args);
  }

  function on_delete($event_args)
  {
    WebApp::execDBCmd("delete_notes", $event_args);
  }

  function onParse()
  {
    WebApp::addGlobalVar('approve', 'true');
    WebApp::addGlobalVar('admin', 'true');
    WebApp::addGlobalVar('notesadmin', 'true');
  }
}
?>