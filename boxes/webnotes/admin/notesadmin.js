// -*-C-*-
/*
  This file  is part of phpWebApp,  which is a  framework for building
  web application based on relational databases.

  Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  phpWebApp is free software; you can redistribute it and/or modify it
  under the  terms of the GNU  General Public License  as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  phpWebApp is  distributed in  the hope that  it will be  useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along with phpWebApp; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/** check all the notes that are displayed in the page */
function select_all()
{
  var form = document.notes;
  var i;

  for (i=0; i < form.length; i++)
    {
      elem = form.elements[i];
      if (elem.type=='checkbox')
        {
          elem.checked = 1;
        }
    }
}

/** uncheck all the notes that are displayed in the page */
function unselect_all()
{
  var form = document.notes;
  var i;

  for (i=0; i < form.length; i++)
    {
      elem = form.elements[i];
      if (elem.type=='checkbox')
        {
          elem.checked = 0;
        }
    }
}

/** return a comma separated list with ids of the notes that are checked */
function get_selected_notes()
{
  var arr_checked_notes = new Array();
  var form = document.notes;
  var i;

  for (i=0; i < form.length; i++)
    {
      elem = form.elements[i];
      if (elem.type=='checkbox')
        {
          if (elem.checked==1)  arr_checked_notes.push(elem.value);
        }
    }

  return arr_checked_notes.join();
}

/** set the status of the selected notes */
function set_note_status()
{
  var listbox = document.notes.select_status;
  var idx = listbox.selectedIndex;
  var selected_status = listbox.options[idx].value;
  var selected_notes = get_selected_notes();
  var event_args = 'status='+selected_status+';note_list='+selected_notes;
  SendEvent('notesadmin', 'set_status', event_args);
}

/** delete the selected notes */
function del_notes()
{
  var selected_notes = get_selected_notes();
  SendEvent('notesadmin', 'delete', 'note_list='+selected_notes);
}

