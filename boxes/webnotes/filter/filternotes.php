<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package     components
   * @subpackage  webnotes
   */
class filternotes extends WebObject
{
  function init()
  {
    $this->addSVar('email', '');
    $this->addSVar('page_id', '');
    $this->addSVar('status', '');
    $this->addSVar('body', '');
    $this->addSVar('condition', '(1=1)');
  }

  function onParse()
  {
    $this->setSVar('condition', $this->get_filter_condition());
  }

  function onRender()
  {
    //add the recordset status
    $rs = new EditableRS('status_rs');

    $rs->addRec( array('id'=>'edit', 'label'=>'edit') );
    $rs->addRec( array('id'=>'queued', 'label'=>'queued') );
    $rs->addRec( array('id'=>'approved', 'label'=>'approved') );
    $rs->addRec( array('id'=>'declined', 'label'=>'declined') );
    $rs->addRec( array('id'=>'archived', 'label'=>'archived') );

    global $webPage;
    $webPage->addRecordset($rs);
  }

  function get_filter_condition()
  {
    $email = $this->getSVar('email');
    $page_id = $this->getSVar('page_id');
    $status = $this->getSVar('status');
    $body = $this->getSVar('body');

    $conditions = array();
    if ($email!='')  $conditions[] = "(email LIKE '%$email%')";
    if ($page_id!='')  $conditions[] = "(page_id LIKE '%$page_id%')";
    if ($status!='')  $conditions[] = "(status = '$status')";
    if ($body!='')  $conditions[] = "(note_text LIKE '%$body%')";

    $filter_condition = implode(' AND ', $conditions);
    if (trim($filter_condition)=='')  $filter_condition = '1=1';

    return $filter_condition;
  }
}
?>