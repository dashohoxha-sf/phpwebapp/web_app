// -*-C-*-
/*
  This file  is part of phpWebApp,  which is a  framework for building
  web application based on relational databases.

  Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  phpWebApp is free software; you can redistribute it and/or modify it
  under the  terms of the GNU  General Public License  as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  phpWebApp is  distributed in  the hope that  it will be  useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along with phpWebApp; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function set_email(txtbox)
{
  var email = txtbox.value;
  session.setVar('filternotes->email', email);
  refresh();
}

function set_pageid(txtbox)
{
  var page_id = txtbox.value;
  session.setVar('filternotes->page_id', page_id);
  refresh();
}

function set_status(lstbox)
{
  var idx = lstbox.options.selectedIndex;
  var status = lstbox.options[idx].value;
  session.setVar('filternotes->status', status);
  refresh();
}

function set_body(txtbox)
{
  var body = txtbox.value;
  session.setVar('filternotes->body', body);
  refresh();
}

function refresh()
{
  GoTo('thisPage');
}
