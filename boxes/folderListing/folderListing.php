<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * This box allows the browsing of a folder. It displays a list of
   * subfolders and files that are contained in the folder. It also
   * allows the user to browse the subfolders, to go to the parent folder,
   * to select a file, etc. The files that are displayed are those that 
   * match a certain filter specified by the programmer.
   *
   * @package     components
   * @subpackage  folderListing
   */
class folderListing extends WebObject
{
  function init()
  {
    $this->addSVar('fileFilter', '.*');
    $this->addSVar('root', APP_PATH);
    $this->addSVar('currentPath', '');
    $this->addSVar('selectedFile', '{{FOLDERLISTING_PATH}}empty.html');
  }

  function on_changeRoot($event_args)
  {
    $root_folder = $event_args['root_folder'];
    if (substr($root_folder, -1) != '/')    $root_folder .= '/';
    $this->setSVar('root', $root_folder);
    $this->setSVar('currentPath', '');
  }

  function on_set_path($event_args)
  {
    $path = $event_args['path'];
    $this->setSVar('currentPath', $path);
  }

  function on_goto_parent_folder($event_args)
  {
    $current_path = $this->getSVar('currentPath');
    $current_path = ereg_replace('[^/]*/$', '', $current_path);
    $this->setSVar('currentPath', $current_path);
  }

  function on_goto_root_folder($event_args)
  {
    $this->setSVar('currentPath', '');
  }

  function on_goto_folder($event_args)
  {
    $folder_name = $event_args['folder_name'];
    $current_path = $this->getSVar('currentPath');
    $current_path .= $folder_name.'/';
    $this->setSVar('currentPath', $current_path);
  }

  function on_view_file($event_args)
  {
    $file_name = $event_args['file_name'];
    $root = $this->getSVar('root');
    $path = $this->getSVar('currentPath');
    $this->setSVar('selectedFile', $root.$path.$file_name);
  }

  function onRender()
  {
    $root = $this->getSVar('root');
    $path = $this->getSVar('currentPath');
    $filter = $this->getSVar('fileFilter');
    list($rs_folders,$rs_files) = $this->readFolder($root.$path, $filter);
    $rs_paths = $this->get_path_rs($path);
    $rs_paths->ID = 'paths';
    $rs_folders->ID = 'folders';
    $rs_files->ID = 'files';
    global $webPage;
    $webPage->addRecordset($rs_paths);
    $webPage->addRecordset($rs_folders);
    $webPage->addRecordset($rs_files);
  }

  /** 
   * Returns a recordset of folders and a recordset of files that 
   * are in the given $folder_path (the files are filtered by $filter)
   */
  function readFolder($folder_path, $filter)
  {
    if (filetype($folder_path) != 'dir')
      {
        $msg = T_("'v_folder_path' is not a folder.");
        $msg = str_replace('v_folder_path', $folder_path, $msg);
        print WebApp::error_msg($msg);
        return;
      }

    $dir = opendir($folder_path);
    if (!$dir)
      {
        $msg = T_("Cannot open folder 'v_folder_path'.");
        $msg = str_replace('v_folder_path', $folder_path, $msg);
        print WebApp::error_msg($msg);
        return;
      }

    $rs_folders = new EditableRS;
    $rs_folders->Open();
    $rs_files = new EditableRS;
    $rs_files->Open();
    if ( substr($folder_path, -1) != '/' )    $folder_path .= '/';
    while ($fname = readdir($dir))
      {
        if (filetype($folder_path.$fname) == 'dir')
          {
            if ($fname=='.' or $fname=='..' or $fname=='CVS')  continue;
            $rs_folders->addRec(array('folder'=>$fname));
          }
        else if (filetype($folder_path.$fname) == 'file')
          {
            if ( eregi($filter,$fname) )
              {
                $rs_files->addRec(array('file'=>$fname));
              }
          }
      }
    closedir($dir);

    return array($rs_folders, $rs_files);
  }

  /** $path is a string like this: 'folder1/folder2/folder3/' */
  function get_path_rs($path)
  {
    $rs_paths = new EditableRS;
    $rs_paths->Open();
    $rec = array('folder'=>'Root', 'path'=>'');
    $rs_paths->addRec($rec);

    $folders = explode('/', $path);
    $folder_path = '';
    for ($i=0; $i < sizeof($folders) - 1; $i++)
      {
        $folder_path .= $folders[$i] . '/';
        $rec = array('folder'=>$folders[$i], 'path'=>$folder_path);
        $rs_paths->addRec($rec);
      }

    return $rs_paths;
  }
}
?>