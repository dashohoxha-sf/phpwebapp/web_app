<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * @package     components
   * @subpackage  fileView
   */
class fileView extends WebObject
{
  function init()
  {
    $curr_path = dirname(__FILE__);
    $this->addSVar("file", $curr_path."/sample_file.html");
  }

  function onParse()
  {
    $file_name = $this->getSVar("file");
    $file_type = $this->get_file_type($file_name);
    $template = $this->get_template($file_type);
    //set the template that will be used to display the file
    WebApp::addVar("template", $template);
  }

  /** Returns the type of the given file. */
  function get_file_type($file_name)
  {
    ereg('(\.[^.]*)$', $file_name, $regs);
    $ext = $regs[1];
    switch ($ext)
      {
      case ".html":
      case ".htm":
        $file_type = "HTML";
        break;
      case ".gif":
      case ".jpg":
      case ".png":
        $file_type = "IMG";
        break;
      case ".txt":
        $file_type = "TXT";
        break;
      case ".php":
      case ".php3":
      case ".php4":
        $file_type = "PHP";
        break;
      case ".js":
        $file_type = "JS";
        break;
      case ".css":
        $file_type = "CSS";
        break;
      default:
        $file_type = "UNKNOWN";
        break;
      }
    return $file_type;
  }

  /** Returns the template that will be used to display 
   * a file of the given type */
  function get_template($file_type)
  {
    global $session;

    $path = WebApp::getVar("./");
    switch ($file_type)
      {
      case "PHP":
        //PHP is always displayed as code
        $template = $path."viewPHP.html";
        break;

      case "IMG":
        //Images are always displayed in preview mode
        $template = $path."viewImage.html";
        break;

      case "JS":
      case "CSS":
      case "TXT":
        //Text is always displayed in code view mode
        $file_name = $this->getSVar("file");
        WebApp::setSVar("codeViewer->file", $file_name);
        $template = CODEVIEWER_PATH."codeViewer.html";
        break;

      case "HTML":
        //Html is displayed either in code view
        //or in preview mode, depending on 'codeView' variable
        $codeView = WebApp::getSVar("panel->codeView");
        if ($codeView=="true")
          {
            $file_name = $this->getSVar("file");
            WebApp::setSVar("codeViewer->file", $file_name);
            $template = CODEVIEWER_PATH."codeViewer.html";
          }
        else
          {
            $template = $path."viewHtml.html";
          }
        break;

      default:
      case "UNKNOWN":
        $template = $path."viewUnknown.html";
        break;        
      }

    return $template;
  }
}
?>