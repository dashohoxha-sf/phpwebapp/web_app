<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * The 'codeViewer' component can be used to display the PHP, HTML,
   * JavaScript, etc., code files in HTML format. 
   *
   * @package     components
   * @subpackage  codeViewer
   */
class codeViewer extends WebObject
{
  function init()
  {
    $this->addSVar('file', UNDEFINED);
    $this->addSVar('lines', '');
    $this->addSVar('highlights', '');
    $this->addSVar('show_nr', false);
  }

  function onRender()
  {
    $file = $this->getSVar('file');
    $lines = $this->getSVar('lines');
    $highlights = $this->getSVar('highlights');
    $show_nr = $this->getSVar('show_nr');

    $this->add_line_recordset($file, $show_nr, $lines, $highlights);

    WebApp::addVar('show_nr', ($show_nr ? 'true' : 'false'));
  }

  /**
   * Creates and adds to webPage a recordset with the lines that
   * will be displayed.
   */
  function add_line_recordset($file, $show_nr, $lines, $highlights)
  {
    ////create a code colorizer object
    //$highlighter = $this->initColorizer();

    $code_lines = file($file);
    if ($lines=='')   //display all the lines
      {
        $display_ranges[] = array('start'=>1, 'end'=>sizeof($code_lines) );
      }
    else
      {
        $display_ranges = $this->getRanges($lines);
      }
    $highlight_ranges = $this->getRanges($highlights);

    $rs = new EditableRS('lines');

    //for each range of lines
    $nr_ranges = sizeof($display_ranges);
    for ($r=1; $r <= $nr_ranges; $r++) 
      {
        $range = $display_ranges[$r-1];
        $start_line = $range['start'];
        $end_line = $range['end'];
        //for each line in the range
        for ($line_nr=$start_line; $line_nr<=$end_line; $line_nr++)
          {
            $line = $code_lines[$line_nr-1];
            $html_line = $this->get_html_line($line);

            //if the line is highlighted use other styles
            if ($this->inRange($line_nr, $highlight_ranges))
              {
                $cssClass_nr = 'codeViewer-nr-highlighted';
                $cssClass_line = 'codeViewer-line-highlighted';
              }
            else
              {
                $cssClass_nr = 'codeViewer-nr';
                $cssClass_line = 'codeViewer-line';
              }

            //add line to recordset
            $rec = array(
                         'nr'         => $line_nr,
                         'class-nr'   => $cssClass_nr,
                         'line'       => $html_line,
                         'class-line' => $cssClass_line
                         );
            $rs->addRec($rec);
          }

        //add a separating line after each range
        //(except for the last one)
        if ($r < $nr_ranges)
          {
            $rec = array(
                         'nr'         => '--',
                         'class-nr'   => 'codeViewer-nr',
                         'line'       => '----------',
                         'class-line' => 'codeViewer-line'
                         );
            $rs->addRec($rec);
          }
      }

    global $webPage;
    $webPage->addRecordset($rs);
  }

  function get_html_line($line)
  {
    $line = chop($line);
    $line = str_replace("\t", "    ", $line);
    $line = str_replace('{{', '{{#', $line);
    $line = htmlentities($line);
    $line = str_replace(' ', '&nbsp;', $line);
    if ($line=='')  $line='<span class="h_space"/>';
    return $line;
  }

  /**
   * gets a string of ranges, like this: "3-7,15,20-21"
   * constructs and returns a range array
   */
  function getRanges($line_ranges)
  {
    $ranges = explode(',', $line_ranges);
    while (list($i,$range) = each($ranges))
      {
        list($start,$end) = explode('-', $range);
        if ($end=='') $end = $start;
        $arr_ranges[$i]['start'] = $start;
        $arr_ranges[$i]['end'] = $end;
      }

    return $arr_ranges;
  }

  /**
   * returns true if the given number is in the
   * given array of ranges
   */
  function inRange($number, $arr_ranges)
  {
    while (list($i,$range) = each($arr_ranges))
      {
        $start = $range['start'];
        $end = $range['end'];
        if (($number >= $start) and ($number <= $end))
          {
            return true;
          }
      }
    return false;
  }

  /** load, initialize and return the code colorizer */
  function initColorizer()
  {
    global $BEAUT_PATH;

    $BEAUT_PATH = CODEVIEWER_PATH.'codeColorizer';
    require_once "$BEAUT_PATH/Beautifier/Core.php";
    require_once "$BEAUT_PATH/HFile/HFile_mixedphp.php";
    require_once "$BEAUT_PATH/Output/Output_HTML.php";
    $highlighter = new Core(new HFile_plain(), new Output_HTML());

    return $highlighter;
  }
}
?>