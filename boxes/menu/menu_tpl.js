// -*-C-*- //tell emacs to use the C mode

var MENU_POS = [
  {
    // item sizes
    'width': 128,
    'height': 18,
    // menu block offset from the origin:
    //  for root level origin is upper left corner of the page
    //  for other levels origin is upper left corner of parent item
    'block_top': 85,
    'block_left': 0,
    // offsets between items of the same level
    'top': 20,
    'left': 0,
    // time in milliseconds before menu is hidden after cursor has gone out
    // of any items
    'hide_delay': 200,
    'css' : {
      'inner' : 'minner',
      'outer' : ['moout', 'moover', 'modown']
    }
  },
  {
    'width': 188,
    'block_top': 3,
    'block_left': 120
  },
  {
    'block_left': 180
  }
  ];

