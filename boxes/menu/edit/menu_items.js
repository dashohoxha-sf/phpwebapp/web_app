// -*-C-*- //tell emacs to use the C mode

var EDIT_MENU_ITEMS = 
  [
   ['Botimet', 'javascript:edit(\'botimet\')', null,
    ['Listo Kartat', 'javascript:edit(\'botimet/kartat/listo\')', null],
    ['Krijo Karte', 'javascript:edit(\'botimet/kartat/krijo\')', null],
    ['Listo Kontratat', 'javascript:edit(\'botimet/kontratat/listo\')', null],
    ['Krijo Kontrate', 'javascript:edit(\'botimet/kontratat/krijo\')', null],
    ],
   ['Buletinet', 'javascript:edit(\'buletinet\')', null,
    ['Ofseti', 'javascript:edit(\'ofseti\')', null,
     ['Listo', 'javascript:edit(\'buletinet/offseti/listo\')', null],
     ['Krijo', 'javascript:edit(\'buletinet/offseti/krijo\')', null],
     ],
    ['Parapregatitja', 'javascript:edit(\'parapregatitja\')', null,
     ['Listo', 'javascript:edit(\'buletinet/parapreg/listo\')', null],
     ['Krijo', 'javascript:edit(\'buletinet/parapreg/krijo\')', null],
     ],
    ['Liberlidhja', 'javascript:edit(\'liberlidhja\')', null,
     ['Listo', 'javascript:edit(\'buletinet/lidhja/listo\')', null],
     ['Krijo', 'javascript:edit(\'buletinet/lidhja/krijo\')', null],
     ],
    ],
   ['Magazina', 'javascript:edit(\'magazina\')', null,
    ['Leter', 'javascript:edit(\'leter\')', null,
     ['Listo Faturat', 'javascript:edit(\'magazina/leter/faturat\')', null],
     ['Bej Hyrje', 'javascript:edit(\'magazina/leter/hyrje\')', null],
     ['Bej Dalje', 'javascript:edit(\'magazina/leter/dalje\')', null],
     ['Gjendja', 'javascript:edit(\'magazina/leter/gjendja\')', null],
     ],
    ['Boje', 'javascript:edit(\'boje\')', null,
     ['Listo Faturat', 'javascript:edit(\'magazina/tjeter/faturat\')', null],
     ['Bej Hyrje', 'javascript:edit(\'magazina/tjeter/hyrje\')', null],
     ['Bej Dalje', 'javascript:edit(\'magazina/tjeter/dalje\')', null],
     ['Gjendja', 'javascript:edit(\'magazina/tjeter/gjendja\')', null],
     ],
    ],
   ['Raportet', 'javascript:edit(\'raportet\')', null,
    ['Shpenzimet', 'javascript:edit(\'shpenzimet\')', null,
     ['Permbledhese', 'javascript:edit(\'rpt_shpenzimet\')', null],
     ['Sasi', 'javascript:edit(\'sasi\')', null,
      ['Parapregatitja', 'javascript:edit(\'rpt_shpenzimet/sasi/parapreg\')', null],
      ['Offseti', 'javascript:edit(\'rpt_shpenzimet/sasi/offseti\')', null],
      ['Liberlidhja', 'javascript:edit(\'rpt_shpenzimet/sasi/lidhja\')', null],
      ['Te Gjitha', 'javascript:edit(\'rpt_shpenzimet/sasi\')', null],
      ],
     ['Vlere', 'javascript:edit(\'vlere\')', null,
      ['Parapregatitja', 'javascript:edit(\'rpt_shpenzimet/vlere/parapreg\')', null],
      ['Offseti', 'javascript:edit(\'rpt_shpenzimet/vlere/offseti\')', null],
      ['Liberlidhja', 'javascript:edit(\'rpt_shpenzimet/vlere/lidhja\')', null],
      ['Te Gjitha', 'javascript:edit(\'rpt_shpenzimet/vlere\')', null],
      ],
     ['Sasi-Vlere', 'javascript:edit(\'sasi-vlere\')', null,
      ['Parapregatitja', 'javascript:edit(\'rpt_shpenzimet/sasi_vlere/parapreg\')', null],
      ['Offseti', 'javascript:edit(\'rpt_shpenzimet/sasi_vlere/offseti\')', null],
      ['Liberlidhja', 'javascript:edit(\'rpt_shpenzimet/sasi_vlere/lidhja\')', null],
      ['Te Gjitha', 'javascript:edit(\'rpt_shpenzimet/sasi_vlere\')', null],
      ],
     ],
    ['Puna e Kryer', 'javascript:edit(\'rpt_punet\')', null,
     ['Parapregatitja', 'javascript:edit(\'rpt_punet/parapreg\')', null],
     ['Offseti', 'javascript:edit(\'rpt_punet/offseti\')', null],
     ['Liberlidhja', 'javascript:edit(\'rpt_punet/lidhja\')', null],
     ['Permbledhese', 'javascript:edit(\'rpt_punet/permbledhese\')', null],
     ],
    ],
   ['Koeficientet', 'javascript:edit(\'koeficientet\')', null,
    ['Teknologjike', 'javascript:edit(\'teknologjike\')', null,
     ['Parapregatitja', 'javascript:edit(\'koeficientet/parapreg\')', null],
     ['Offseti', 'javascript:edit(\'koeficientet/offseti\')', null],
     ['Liberlidhja', 'javascript:edit(\'koeficientet/lidhja\')', null],
     ],
    ['Financiare', 'javascript:edit(\'koeficientet/financiare\')', null],
    ],
   ['Administrim', 'javascript:edit(\'administrim\')', null,
    ['Perdoruesit', 'javascript:edit(\'perdoruesit\')', null],
    ['Punetoret', 'javascript:edit(\'punetoret\')', null],
    ['Punetor Paraperg.', 'javascript:edit(\'punetoret/parapreg\')', null],
    ['Punetor Offseti', 'javascript:edit(\'punetoret/offseti\')', null],
    ['Punetor Liberlidhje', 'javascript:edit(\'punetoret/lidhja\')', null],
    ['Backup/Restore', 'javascript:edit(\'backup_restore\')', null],
    ['Tabelat', 'javascript:edit(\'tabelat\')', null],
    ],
   ];
