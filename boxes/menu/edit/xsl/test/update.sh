#!/bin/bash

id=ofseti
new_id=new_offseti
new_caption='New Offseti'
xsl_file=../update.xsl
xml_file=../../menu.xml

xsltproc  --stringparam id "$id" \
          --stringparam new_id "$new_id" \
          --stringparam new_caption "$new_caption" \
          $xsl_file  $xml_file
