<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This  file is  part  of SMEWebApp.   SMEWebApp  is a  web application  that
displays and edits DocBook documents.

Copyright (c) 2004 Dashamir Hoxha, dashohoxha@users.sourceforge.net

SMEWebApp is free software; you  can redistribute it and/or modify it under
the  terms of  the GNU  General  Public License  as published  by the  Free
Software Foundation; either  version 2 of the License,  or (at your option)
any later version.

SMEWebApp is  distributed in the hope  that it will be  useful, but WITHOUT
ANY  WARRANTY; without  even  the implied  warranty  of MERCHANTABILITY  or
FITNESS FOR A  PARTICULAR PURPOSE.  See the GNU  General Public License for
more details.

You should  have received a  copy of the  GNU General Public  License along
with SMEWebApp;  if not,  write to the  Free Software Foundation,  Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- Outputs subitems of the selected item                    -->
<!-- Is called with a parameter 'id', like this:              -->
<!-- xsltproc -stringparam id $item_id subitems.xsl menu.xml  -->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" indent="no" />

<!--
  Find the item with the given id and then output the id and
  caption for this item and for its subitems.
-->
<xsl:template match="*[@id=$id]">
  <xsl:value-of select="concat(@id, ' ', @caption)" />
  <xsl:text>
</xsl:text>

  <xsl:for-each select="./item">
    <xsl:value-of select="concat(@id, ' ', @caption)" />
    <xsl:text>
</xsl:text>
  </xsl:for-each>
</xsl:template>

<!-- ignore text nodes -->
<xsl:template match="text()" />

</xsl:transform>
