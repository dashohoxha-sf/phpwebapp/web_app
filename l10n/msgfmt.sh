#!/bin/bash
### convert translation files (*.po) to binary format (*.mo)

### go to this directory
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 ll_CC"
  echo "where ll_CC is the language code, like en_US or sq_AL"
  exit 1
fi

lng=$1

### convert the *.po file of the application
dir=$lng/LC_MESSAGES
msgfmt --output-file=$dir/web_app.mo  $dir/web_app.po

### convert the *.po files of the components
path_list="../boxes ../webobjects ../tools"
for path in $path_list
do
  module_list=$(ls $path/)
  for module in $module_list
  do
    dir=$path/$module/l10n/$lng/LC_MESSAGES/
    if [ -f $dir/$module.po ]
    then
      msgfmt --output-file=$dir/$module.mo $dir/$module.po
    fi
  done
done
