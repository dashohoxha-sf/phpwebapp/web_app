#!/bin/bash
### delete translation files for the components

### go to this directory
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 [ll_CC | all]"
  echo "where ll_CC is the language code, like en_US or sq_AL"
  exit 1
fi

if [ "$1" = "all" ]; then lng=""; else lng=$1; fi

path_list="../boxes ../webobjects ../tools"
for path in $path_list
do
  module_list=$(ls $path/)
  for module in $module_list
  do
    dir=$path/$module/l10n/$lng
    rm -rf $dir
  done
done
