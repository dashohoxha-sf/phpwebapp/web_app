#!/bin/bash

### go to this dir
cd $(dirname $0)

function get_translatable_strings
{
  dir=$1
  module=$2

  mkdir -p $dir/l10n/
  touch $dir/l10n/$module.po

  find $dir -name '*.php' -o -name '*.js' -o -name '*.html' \
    | xargs xgettext -C --keyword=T_ --join-existing \
                     --output-dir=$dir/l10n/ --output=$module.po

  # if it is not greater than zero, delete it
  if [ ! -s $dir/l10n/$module.po ]
  then
    rm -rf $dir/l10n/
  fi
}

### get the translatable strings of webobjects, webboxes and tools,
### from HTML, JS and PHP files
path_list="../boxes ../webobjects ../tools"
for path in $path_list
do
  module_list=$(ls $path/)
  for module in $module_list
  do
    get_translatable_strings $path/$module $module
  done
done


### get the translatable strings for the rest of the application
app_name=$(./get_app_name.sh)
touch web_app.po
find .. -path '../boxes'      -prune \
     -o -path '../webobjects' -prune \
     -o -path '../tools'      -prune \
     -o -name '*.php'  -print  \
     -o -name '*.js'   -print  \
     -o -name '*.html' -print  \
  | xargs xgettext -C --keyword=TF_ --join-existing --output=web_app.po

