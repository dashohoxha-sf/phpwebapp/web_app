#!/bin/bash
### create initial translation files for a language

### go to this directory
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 ll_CC"
  echo "where ll_CC is the language code, like en_US or sq_AL"
  exit 1
fi

lng=$1

### create an initial *.po file for web_app
mkdir -p $lng/LC_MESSAGES/
msginit --input=web_app.po --locale=$lng --no-translator \
        --output-file=$lng/LC_MESSAGES/web_app.po

### create initial *.po files for the components
path_list="../boxes ../webobjects ../tools"
for path in $path_list
do
  module_list=$(ls $path/)
  for module in $module_list
  do
    dir=$path/$module/l10n
    if [ -f $dir/$module.po ]
    then
      mkdir -p $dir/$lng/LC_MESSAGES/
      msginit --input=$dir/$module.po --locale=$lng --no-translator \
              --output-file=$dir/$lng/LC_MESSAGES/$module.po
    fi
  done
done
