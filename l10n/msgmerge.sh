#!/bin/bash
### update old *.po files with new messages extracted by xgettext.sh

### go to this directory
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 ll_CC"
  echo "where ll_CC is the language code, like en_US or sq_AL"
  exit 1
fi

lng=$1

### convert the *.po file of the application
dir=$lng/LC_MESSAGES
msgmerge --update $dir/web_app.po  web_app.po

### convert the *.po files of the components
path_list="../boxes ../webobjects ../tools"
for path in $path_list
do
  module_list=$(ls $path/)
  for module in $module_list
  do
    dir=$path/$module/l10n
    if [ -f $dir/$lng/LC_MESSAGES/$module.po ]
    then
      msgmerge --update $dir/$lng/LC_MESSAGES/$module.po $dir/$module.po
    fi
  done
done
