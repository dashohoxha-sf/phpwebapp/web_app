<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * Renders an HTML page from the templates and structures of the $webPage.
   * @package parser 
   * @see WebPage
   */
class Render
{
  /** If $collect is true, then the page is not 
   * outputed directly to the browser... */
  var $collect;

  /** ...but is collected to the variable $html_page */
  var $html_page;

  /** Helps to render a well-indented page. */
  var $indent;

  function Render($collect =false)
  {
    $this->collect = $collect;
    $this->html_page = "";
    $this->indent = "";
  }

  /** 
   * Replace all {{tpl_vars}} in the given string 
   * and then print it out.
   */
  function render_string($str, $indent =true)
  {
    //translate all T_("xyz") and TF_("xyz") strings
    global $l10n;
    if (ereg('TF?_\\("[^"]*"\\)', $str))  $str = $l10n->translate($str);

    //replace all {{tpl_vars}}
    $str = WebApp::replaceVars($str);

    //extra replacements
    $str = $this->other_replaces($str);

    if ($indent)
      {
        //add indentation at the beginning of each line
        $str = preg_replace('/^/m', $this->indent, $str);

        //remove the indentation at the end of the string
        $str = preg_replace("/\n$this->indent\$/", "\n", $str);
      }

    $this->output($str);
  }

  /** Make some extra processing and replacements in the output. */
  function other_replaces($str)
  {
    //remove checked="no" or checked="false" or checked=""
    $pattern = '/ checked=["\'](no|false)?["\']/';
    $str = preg_replace($pattern, '', $str);
    //rplace checked="yes" or checked="true" with checked="checked"
    $pattern = '/checked=["\'](yes|true)["\']/';
    $str = preg_replace($pattern, 'checked="checked"', $str);

    //remove selected="no" or selected="false" or selected=""
    $pattern = '/ selected=["\'](no|false)?["\']/';
    $str = preg_replace($pattern, '', $str);
    //rplace selected="yes" or selected="true" with selected="selected"
    $pattern = '/selected=["\'](yes|true)["\']/';
    $str = preg_replace($pattern, 'selected="selected"', $str);

    return $str;
  }

  /** Usually prints the given string. */
  function output($str)
  {
    //print the string
    if ($this->collect)   $this->html_page .= $str;
    else                  print $str;
  }

  /** 
   * Returns the tpl_id from a string like this: '&&tpl_id;;'
   * If this pattern does not match, than it returns UNDEFINED.
   */
  function get_tpl_id($str)
  {
    $pattern = '/^&&(.+);;$/';
    if (preg_match($pattern, $str, $matches))
      return $matches[1];
    else
      return UNDEFINED;
  }

  /**
   * Renders the given template, replacing all variables
   * and including all subtemplates.
   */
  function render_tpl($tpl, $indent =true)
  {
    global $webPage, $tplVars;

    $tplVars->pushScope();
    $tplVars->addVars($tpl->getVars());

    $arr_contents = $tpl->get_arr_contents();
    for ($i=0; $i < sizeof($arr_contents); $i++)
      {
        $chunk = $arr_contents[$i];
        $tpl_id = $this->get_tpl_id($chunk);
        if ($tpl_id==UNDEFINED)
          {
            $this->render_string($chunk, $indent);
          }
        else
          {
            //get and render the subtemplate
            $subtpl = $webPage->getTemplate($tpl_id);
            $prev_indent = $this->indent;
            $this->indent = ($indent ? $this->indent.$subtpl->indent : '');
            $this->render_subtpl($subtpl);
            $this->indent = $prev_indent;
          }
      }
                
    $tplVars->popScope();
  }

  /**
   * Dispatcher function: according to the type of 
   * the given $tpl, calls the appropriate rendering function.
   */
  function render_subtpl($tpl)
  {
    switch ($tpl->type)
      {
      default:
        $this->render_tpl($tpl);
        break;
      case "UnindentedTpl":
        $this->render_tpl($tpl, false);
        break;
      case "PreTpl":
      case "XmpTpl":
      case "TextAreaTpl":
        $this->render_preformated_tpl($tpl);
        break;
      case "ExampleTpl":
        $this->render_ExampleTpl($tpl);
        break;
      case "IfTpl":
        $this->render_IfTpl($tpl);
        break;
      case "FileTpl":
        $this->render_FileTpl($tpl);
        break;
      case "MainTpl":
        $this->render_MainTpl($tpl);
        break;
      case "WebObjectTpl":
        $this->render_WebObjectTpl($tpl);
        break;
      case "RSNavigTpl":
        $this->render_RSNavigTpl($tpl);
        break;
      case "RepeatTpl":
        $this->render_RepeatTpl($tpl);
        break;
      }
  }

  function render_ExampleTpl($tpl)
  {
    $attr_list = $tpl->attr_list;
    $contents = $tpl->contents;
    $this->output("<xmp$attr_list>".$contents."</xmp>");
  }

  function render_preformated_tpl($tpl)
  {
    $tag = substr($tpl->type, 0, -3);
    $tag = strtolower($tag);
    $attr_list = $tpl->attr_list;
    $this->output("<$tag$attr_list>");
    $this->render_tpl($tpl, false);
    $this->output("</$tag>");
  }

  /**
   * Renders the given IfTpl, if its $condition evaluates to true.
   */
  function render_IfTpl($if_tpl)
  {
    $expr = $if_tpl->condition;
    $expr = WebApp::replaceVars($expr);
    $val = WebApp::evaluate($expr);
    if ($val) $this->render_tpl($if_tpl);
  }

  /**
   * Renders the given $file_tpl. Defines the variable {{./}} 
   * that contains the path of the file.
   */
  function render_FileTpl($file_tpl)
  {
    $relative_filename = str_replace(APP_PATH, '', $file_tpl->filename);
    if (DEBUG_INCLUDES and $file_tpl->type!='MainTpl')
      {
        $this->output("<span class='webapp_filetpl_filename'>\n");
        $this->output($relative_filename);
        $this->output("</span>\n");
        $this->output("<div class='webapp_filetpl'>\n");
      }

    //use another translation file, if such a file exists
    global $l10n;
    $l10n->pushState();
    $dir = dirname($file_tpl->filename);
    $filename = basename($file_tpl->filename);
    $id = ereg_replace('\\..*$', '', $filename);
    $l10n->set_translation_file($dir, $id);

    //create a new variable scope
    global $tplVars;
    $tplVars->pushScope();

    //add the variable {{./}} which contains the current path
    $tpl_path = dirname($file_tpl->filename) . "/";
    $tpl_path = WebApp::to_url($tpl_path);
    $tplVars->addVar("./", $tpl_path);


    //render the FileTpl
    $this->render_tpl($file_tpl);

    $tplVars->popScope();
    $l10n->popState();

    if (DEBUG_INCLUDES)  $this->output("</div>\n");
  }

  /**
   * Renders the given $main_tpl.
   */
  function render_MainTpl($main_tpl)
  {
    //the body element was not closed in order to append easily other things
    $main_tpl->body->contents .= '</body>';
    $this->render_FileTpl($main_tpl);
  }

  /**
   * Renders the given WebObjectTpl. Before rendering it, declares 
   * its state vars and params as template vars, and calls the function 
   * onRender() of the WebObject.
   */
  function render_WebObjectTpl($wobj_tpl)
  {
    if (DEBUG_WEBOBJECTS)
      {
        $this->output("<span class='webapp_webobjtpl_id'>\n");
        $this->output($wobj_tpl->id);
        $this->output("</span>\n");
        $this->output("<div class='webapp_webobjtpl'>\n");
      }

    global $tplVars, $l10n;
    $l10n->pushState();
    $dir = $wobj_tpl->class->path;
    $id = $wobj_tpl->class->id;
    $l10n->set_translation_file($dir, $id);

    $tplVars->pushScope(); //create a new variable scope

    $wobj_tpl->before_render();
    $this->render_tpl($wobj_tpl); //render it to HTML
    $wobj_tpl->after_render();

    $tplVars->popScope();
    $l10n->popState();

    if (DEBUG_WEBOBJECTS)  $this->output("</div>\n");
  }

  /**
   * Render it as a normal template, but add in the scope of the
   * template the page (navigation) variables of the recordset,
   * which are: {{NextPage}}, {{PrevPage}}, etc.
   */
  function render_RSNavigTpl($tpl)
  {
    global $webPage, $tplVars;

    //get the recordset id
    $rs_id = $tpl->rsId;
    $rs_id = WebApp::replaceVars($rs_id);
    //get a reference to the recordset of the RepeatTpl
    if (isset($webPage->rs_collection[$rs_id]))
      {
        //we cannot use "$webPage->getRecordset($rs_id);" here
        //because we need a reference to the recordset,  
        //and PHP functions cannot return a reference
        $rs = &$webPage->rs_collection[$rs_id];
      }
    else //such a recordset does not exist
      {
        $msg = TF_("Recordset 'v_rs_id' is undefined.");
        $msg = str_replace('v_rs_id', $rs_id, $msg);
        $this->render_string(WebApp::error_msg($msg));
        $this->render_tpl($tpl);
        return;
      }

    if ($rs->type<>"PagedRS")
      {
        $msg = TF_("Recordset 'v_rs_id' is not of type PagedRS.");
        $msg = str_replace('v_rs_id', $rs_id, $msg);
        $this->render_string(WebApp::error_msg($msg));
        $this->render_tpl($tpl);
        return;
      }

    $tplVars->pushScope();    //a new scope for the RSNavig element

    //add page vars (PrevPage, NextPage, etc)
    $rs->Open();
    $tplVars->addVars($rs->getPageVars());

    //render the template
    $this->render_tpl($tpl);

    $tplVars->popScope();
  }

  /**
   * Renders the given $tpl of type RepeatTpl.
   */
  function render_RepeatTpl($tpl)
  {
    global $webPage, $tplVars;
                
    //get the recordset id
    $rs_id = $tpl->rsId;
    $rs_id = WebApp::replaceVars($rs_id);
    //get a reference to the recordset of the RepeatTpl
    if (isset($webPage->rs_collection[$rs_id]))
      {
        //we cannot use "$webPage->getRecordset($rs_id);" here
        //because we need a reference to the recordset,  
        //and PHP functions cannot return a reference
        $rs = &$webPage->rs_collection[$rs_id];
      }
    else //such a recordset does not exist
      {
        $msg = TF_("Recordset 'v_rs_id' is undefined.");
        $msg = str_replace('v_rs_id', $rs_id, $msg);
        $this->render_string(WebApp::error_msg($msg));
        $this->render_tpl($tpl);
        return;
      }

    //open the recordset
    $rs->Open();
    if ($rs->EOF())
      {
        //recordset has no records, it is empty
        $this->render_tpl($tpl->if_empty);
        return;
      }

    //find the first record nr, if the recordset is a PagedRS
    $firstRecNr = 1;
    if ($rs->type=='PagedRS')
      {
        $page_vars = $rs->getPageVars();
        $firstRecNr = $page_vars['FirstRec'];
      }

    //render the body of repeat for each record
    $row_nr = 1;
    while ( !$rs->EOF() )
      {
        $tplVars->pushScope();

        //add variables {{CurrentRecNr}} and {{CurrentRowNr}}
        WebApp::addVar("CurrentRecNr", $firstRecNr + $row_nr - 1);
        WebApp::addVar("CurrentRowNr", $row_nr);

        //render the body of the repeat
        $tplVars->addVars($rs->Fields());
        $tplVars->addVars($tpl->getVars());
        $this->render_tpl($tpl);

        if ($row_nr < $rs->count) 
          {
            //render the separator
            $this->render_tpl($tpl->separator);
            $this->output("\n");
          }

        //get ready for the next iteration
        $tplVars->popScope();
        $rs->MoveNext();
        $row_nr++;
      }
  }
}
?>