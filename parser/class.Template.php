<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * @package parser
   */
class Template
{
  /** Template id. */
  var $id;
      
  /** Template type:
   *  'Template', 'If', 'Repeat', 'WebBox', 
   *  'IfEmpty', 'Header', 'Footer' */
  var $type;    

  /** The parent template */
  var $parent;  
      
  /** Array that keeps the variables of the template. */
  var $vars;    

  /** The contents of the template. */
  var $contents; 

  /** The indentation of the template relative to the parent. */
  var $indent;


  /** Constructor */
  function Template($tplId =UNDEFINED, $type ="Template")
  {
    static $auto_template_id = "Template_01";
                
    $this->id = ($tplId==UNDEFINED ? $auto_template_id++ : $tplId);
    $this->type = $type;
    $this->vars = array();
  }

  /** Adds a new variable in the array of variables. */
  function addVar($var_name, $expression)
  {
    $var_name = $var_name;
    $this->vars[$var_name] = $expression;
  }

  /** Returns an array with all the evaluated variables. */
  function getVars()
  {
    reset($this->vars);
    $tplVars = $this->vars;
    while ( list($var_name,$expr) = each($tplVars) )
      {
        $expr = WebApp::replaceVars($expr);
        $tplVars[$var_name] = WebApp::evaluate($expr);
      }
    return $tplVars;
  }

  /**
   * Returns an array of ids of the templates that are referenced
   * from the contents of this template.
   */
  function get_subtemplates()
  {
    $pattern = '/&&([^;]+);;/';
    preg_match_all($pattern, $this->contents, $matches);
    $arr_tpl_id = $matches[1];
    return $arr_tpl_id;
  }

  /** Split the contents to an array, according to the references. */
  function get_arr_contents()
  {
    $pattern = '/(&&[^;]+;;)/';
    $flags = PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE;
    $arr_contents = preg_split($pattern, $this->contents, -1, $flags);
    return $arr_contents;
  }

  /**
   * Replaces multiple empty lines by just one empty line,
   * trims the indentation from the beginning of each line, etc.
   */
  function normalize_space()
  {
    $str = $this->contents;

    //if there are 2 or more empty lines, replace them by a single one
    $str = preg_replace('/\n([ \t]*\n){2,}/', "\n\n", $str);

    //remove any empty lines in the begining
    $str = preg_replace('/^\s*\n/', '', $str);

    //remove any empty lines in the end
    $str = preg_replace('/\n\s*$/', "\n", $str);

    //Trim the indentation from the beginning of each line.
    //get the indentation of each line
    preg_match_all("/^( *)[^\n]/m", $str, $arr_matches); 
    $arr_indents = $arr_matches[1];
    //sort them
    asort($arr_indents);
    $arr_indents = array_values($arr_indents);
    //get the smallest one
    $smallest_indent = $arr_indents[0];
    //remove it from the begining of each line
    $str = preg_replace("/^$smallest_indent/m", '', $str);

    //set the indentation of the subtemplates
    global $webPage;
    $arr_tpl_id = $this->get_subtemplates();
    for ($i=0; $i < sizeof($arr_tpl_id); $i++)
      {
        $tpl_id = $arr_tpl_id[$i];

        //get the indentation of the reference
        $pattern = "|^( *)&&{$tpl_id};;|m";
        preg_match($pattern, $str, $matches);
        $indent = $matches[1];

        //remove it from the line of the reference
        $str = preg_replace($pattern, "&&$tpl_id;;", $str);

        //set it as the indentation of the subtemplate
        $tpl = $webPage->getTemplate($tpl_id);
        $tpl->indent = $indent;
        $webPage->addTemplate($tpl);
      }
      
    //save the normalized string back to contents
    $this->contents = $str;
  }
                
  /**#@+ Debug function. */
  function toHtmlTable()
  {
    $indent = strtr($this->indent, ' ', '#');
    $htmlTable = "
<br />
<a name='$this->id' id='$this->id'/>[<a href='javascript: back()'>Back</a>]
<table class='webapp' width='90%' bgcolor='#aaaaaa' border='0' cellspacing='1' cellpadding='2'>". $this->attribs2html() . $this->vars2html() . "
  <tr><td colspan='2' bgcolor='#f9f9ff'>
" . $this->contents2html() . "
  </td></tr>
</table>
";
                
    return $htmlTable;
  }

  function attribs2html()
  {
    $indent = strtr($this->indent, ' ', '#');
    $html = "
  <tr>
    <td bgcolor='#eeeeee' align='right' width='1%'>ID:</td>
    <td bgcolor='#eeeeff'>$this->id</td>
  </tr>
  <tr>
    <td bgcolor='#eeeeee' align='right'>Type:</td>
    <td bgcolor='#f9f9f9'>$this->type</td>      
  </tr>
  <tr>
    <td bgcolor='#eeeeee' align='right'>Indent:</td>
    <td bgcolor='#f9f9f9'>
      <span style='background-color: #888888;'>$indent</span>
    </td>      
  </tr>
";
    return $html;
  }

  /** @see toHtmlTable() */
  function vars2html()
  { 
    reset($this->vars);
    while ( list($vname,$value) = each($this->vars) )
      {
        $vars .= " $vname=($value); ";
      }

    $html = "
  <tr>
    <td bgcolor='#eeeeee' valign='top' align='right'>Vars:</td>
    <td bgcolor='#f9f9f9'> $vars </td>
  </tr>";
    return $html;
  }

  /** @see toHtmlTable() */
  function contents2html()
  {
    global $webPage;

    $arr_contents = $this->get_arr_contents();
    $html = "\n<pre class='webapp'>";
    for ($i=0; $i < sizeof($arr_contents); $i++)
      {
        $str = $arr_contents[$i];
        if (preg_match('/^&&(.+);;$/', $str, $matches))
          {
            $tpl_id = $matches[1];
            //$tpl = $webPage->getTemplate($tpl_id);
            //$html .= $tpl->indent;
            $html .= "<a href='#$tpl_id'>&amp;&amp;$tpl_id;;</a>";
          }
        else
          {
            $html .= htmlentities($str);
          }
      }
    $html .= "</pre>\n";

    return $html;
  }
        
  /** Returns the structure of loaded templates as a tree. */
  function to_tree($indent)
  {
    //don't output the appended boxes
    if (ereg('append/append.html$', $this->id))  return;

    global $webPage;
                
    $tree = $indent."|\n";
    $tree .= $indent."+--<a href='#".$this->id."'>".$this->id."</a>"
      . " (".$this->type.")\n";

    $arr_tpl_id = $this->get_subtemplates();
    for ($i=0; $i < sizeof($arr_tpl_id); $i++)
      {
        $tpl_id = $arr_tpl_id[$i];
        $tpl = $webPage->getTemplate($tpl_id);
        $tree .= $tpl->to_tree($indent."|  ");
      }
    return $tree;
  }
  /**#@-*/
}
?>