<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * This is like an interface or a general class that should be 
   * extended by all the PHP classes of the <WebClass> and <WebBox>
   * elements.
   * @package parser 
   */
class WebObject extends WebObjectTpl
{
  /** 
   * This is an abstract function 
   * that can be overriden by subclasses.
   * It is called only the first time that
   * the webobj is created (only once in a session)
   * and can be used to initialize the state variables.
   */
  function init() {}

  /**
   * This is an abstract function 
   * that can be overriden by subclasses.
   * It is called by the framework when
   * an event is sent to the webobj.
   */
  function eventHandler($event) {}

  /**
   * This is an abstract function 
   * that can be overriden by subclasses.
   * This is called by the framework before
   * the webobj is parsed.
   */
  function onParse() {}

  /**
   * This is an abstract function 
   * that can be overriden by subclasses.
   * This is called by the framework after
   * the webobj is parsed.
   */
  function afterParse() {}

  /**
   * This is an abstract function 
   * that can be overriden by subclasses.
   * This is called by the framework before
   * the webobj is rendered. It is usually used
   * to add variables that are used in the template.
   */
  function onRender() {}

  /**
   * This is an abstract function 
   * that can be overriden by subclasses.
   * This is called by the framework after
   * the webobj is rendered.
   */
  function afterRender() {}

  /** empty constructor */
  function WebObject()
  {
    //if the class that extends WebObject defines a constructor,
    //this one will not be called during the creation of the object,
    //and if it does not define a constructor this one will be called;
    //that is why the construction of this object is not done here
    //but is done in another function that is called explicitly from
    //the parser
  }

  function construct_WebObject($id)
  {
    //call the constructor of the superclass
    $this->WebObjectTpl($id);

    //save the current scope
    global $tplVars;
    $this->scope = $tplVars->getCurrentScope();
  }

  function getStateVars()
  {
    $sVars = $this->getSVars();
    if (!isset($sVars['initialized']))
      {
        //this is the first time that this webobj is
        //created (during the session), call init()
        $this->init();

        //this variable is set so that the next time
        //init is not called again
        $this->addSVar("initialized", "true");
      }
  }

  /** Add state var (session var). */
  function addSVar($var_name, $var_value, $db =false)
  {
    global $session;
    $v_name = $this->id."->".$var_name;
    $session->addVar($v_name, $var_value, $db);
  }

  /** $arr_vars is a list of vars (an associative array). */
  function addSVars($arr_vars, $db =false)
  {
    while (list($var_name,$var_value) = each($arr_vars))
      {
        $this->addSVar($var_name, $var_value, $db);
      }
  }

  /** get a state var of the webobj */
  function getSVar($var_name)
  {
    $v_name = $this->id."->".$var_name;
    global $session;
    $var_value = $session->getVar($v_name);
    return $var_value;
  }

  /** set a state var of the webobj */
  function setSVar($var_name, $var_value)
  {
    //set it in the session
    $v_name = $this->id."->".$var_name;
    WebApp::setSVar($v_name, $var_value);

    //change it to the var stack as well
    global $tplVars;
    $tplVars->addVar($var_name, $var_value, $this->scope);
  }

  /**
   * Get from $session the state variables that belong
   * to this webobj and return them as an associated array.
   * The optional parameter $type accepts the values "DB"
   * and "JS", and depending on this parameter the function
   * returns DB vars, JS vars or all of them (if undefined).
   */
  function getSVars($type =UNDEFINED)
  {
    global $session;
    $stateVars = $session->getObjVars($this->id, $type);
    return $stateVars;
  }

  /** $arr_vars is an associative array */
  function setSVars($arr_vars)
  {
    while (list($var_name,$var_value) = each($arr_vars))
      {
        $this->setSVar($var_name, $var_value);
      }
  }

  /**
   * Builds an HTML table containing the state vars
   * and returns it as a string (for debug).
   */
  function state2html()
  {
    $html = "
<br />
<a name='$this->id' id='$this->id' />[<a href='tree'>Top</a>]
<table bgcolor='#aaaaaa' border='0' cellspacing='1' cellpadding='2'>
  <tr>
    <td bgcolor='#eeeeff' colspan='3'>$this->id</td>
  </tr>
  <tr>
    <td bgcolor='#eeeeee'><span class='h_space'/></td>
    <td bgcolor='#eeeeee'>var_name</td>
    <td bgcolor='#eeeeee'>var_value</td>
  </tr>";

    $sVars = $this->getSVars("JS");
    while (list($var_name,$var_value) = each($sVars))
      {
        $html .= "
  <tr>
    <td bgcolor='#f9f9f9'><span class='h_space'/></td>
    <td bgcolor='#f9f9f9'>$var_name</td>
    <td bgcolor='#f9f9ff'>$var_value</td>
  </tr>";
      }
    $sVars = $this->getSVars("DB");
    while (list($var_name,$var_value) = each($sVars))
      {
        $html .= "
  <tr>
    <td bgcolor='#f9f9f9'>DB</td>
    <td bgcolor='#f9f9f9'>$var_name</td>
    <td bgcolor='#f9f9ff'>$var_value</td>
  </tr>";
      }
    $html .= "</table>";

    return $html;
  }
}
?>