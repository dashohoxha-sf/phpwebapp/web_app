<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * Parses the templates and constructs $webPage, which
   * keeps the structure of the page.
   *
   * @package parser 
   * @see WebPage, Render
   */
class Parser
{
  /** A stack of templates that are being parsed. */
  var $tpl_stack;       
  /** The template that is being parsed currently. */
  var $current_tpl;     

  /** When $this->copy_mode is true, then the parser works in copy mode. */
  var $copy_mode;
  /** Helper for nested WebBox-es and WebClass-es to be copied correctly */
  var $copy_level;

  /** An array that keeps a stack of the files that are being parsed. */
  var $files;

  /** The doctype declaration etc. of the main template, which are used
   *  for the other templates as well. */
  var $xml_prolog;

  /** If true, outputs debug messages about parsing. */
  var $debug = false;
  /** Indent the debug messages. */
  var $dbg_indent = '';

  function Parser()
  {
    $this->tpl_stack = array();
    $this->current_tpl = UNDEFINED;
    $this->copy_mode = false;
    $this->files = array();
    //$this->debug = true;  //outputs debug messages about parsing
  }

  /** Parse the main (root) template of the page. */
  function parse_main($tpl_file)
  {
    global $webPage;
    $webPage->tpl_file = $tpl_file;

    $tpl = new MainTpl($tpl_file);
    if ($this->debug)  print "<xmp>\n";
    $tpl = $this->parse_file($tpl_file, $tpl);
    if ($this->debug)  print "</xmp>\n";

    if ($tpl<>UNDEFINED)
      {
        $webPage->rootTpl = &$webPage->tpl_collection[$tpl->id];
      }
  }

  /**
   * Parse the filename into the template given as parameter.
   *
   * @param $filename the file to be parsed
   * @param $tpl      (optional) the template or template reference
   *                  to be filled by parsing this file.
   * @return FileTpl  an object that contains the parsed $filename,
   *                  return UNDEFINED if $filename is not found.
   */
  function parse_file($filename, $tpl =UNDEFINED)
  {
    global $tplVars;

    if (!file_exists($filename))
      {
        $msg = TF_("file 'v_filename' does not exist");
        $msg = str_replace('v_filename', $filename, $msg);
        $err_msg = WebApp::error_msg("Parser::parse_file(): $msg.");
        $this->append($err_msg);
        return UNDEFINED;  
      }

    if ($tpl===UNDEFINED)   $tpl = new FileTpl($filename);
     
    array_push($this->files, $filename);
    $tplVars->pushScope();            

    //add the variable {{./}} in the current scope
    $path = dirname($filename).'/';
    $tplVars->addVar('./', $path);

    //parse the contents of the file
    $file_contents = file_get_contents($filename);
    $tpl = $this->parse_string($file_contents, $tpl);

    $tplVars->popScope();
    array_pop($this->files);

    return $tpl;
  }

  /** Parse an xml string into the template given as parameter. */
  function parse_string($xml_string, $tpl =UNDEFINED)
  {
    if ($tpl===UNDEFINED)   $tpl = new Template;

    //The contents of the string usually is not well-formed xml 
    //(does not have <?xml version="1.0" ? > declaration, does not
    //have DOCTYPE declaration, and may not have a single root).
    //In this case the xml parser may not parse it correctly.
    //We make it well-formed by adding additional things taken from
    //the main template. Then the parser discards these things.
    if (substr($xml_string, 0, 5) != '<?xml')
      {
        $xml_string = $this->xml_prolog . $xml_string . '</html>';
      }

    //parse the file into $tpl
    $this->push_current_tpl($tpl);

    //create a new parser
    $parser = $this->new_xml_parser();

    if ($this->debug)
      {
        $this->dbg_indent = '| '.$this->dbg_indent;
        print $this->dbg_indent;
        print "<< start xml parser: $parser\n";
      }

    //parse the string with the xml parser
    $this->parse_xml($parser, $xml_string);

    if ($this->debug)
      {
        print $this->dbg_indent;         
        print ">> end xml parser: $parser\n";
        $this->dbg_indent = substr($this->dbg_indent, 2);
      }

    //free the parser and get the template from the stack
    xml_parser_free($parser);
    $tpl = $this->pop_current_tpl();

    return $tpl;
  }

  /** 
   * Parse the given string using the given $xml_parser.
   * Return false in case of error, true otherwise.
   */
  function parse_xml($xml_parser, $content)
  {
    if (xml_parse($xml_parser, $content, true))
      {
        return true;
      }
    else
      {
        $error_code = xml_get_error_code($xml_parser);
        $error_string = xml_error_string($error_code);

        $fname = $this->files[sizeof($this->files)-1];
        $line_nr = xml_get_current_line_number($xml_parser);
        $col_nr  = xml_get_current_column_number($xml_parser);
        $err_idx  = xml_get_current_byte_index($xml_parser);

        //get the part of the file around the error
        $start = ( (($err_idx-40) < 0) ? 0 : ($err_idx-40) );
        $str_1 = substr($content, $start, $err_idx - $start);
        $str_2 = substr($content, $err_idx, 10);
        $bad_xml = "... $str_1^err^$str_2 ...";

        $xml_err_msg = WebApp::xml_err_msg($fname, $line_nr, $col_nr,
                                           $error_string, $bad_xml);
        $this->append($xml_err_msg);

        $this->clean_tpl_stack();

        return false;
      }
  }

  /**
   * When an xml parser encounters an error, it stops parsing at all.
   * Remove from the template stack all the templates that were pushed
   * by this parser, but cannot be poped (since the parser has stoped
   * working and ending tags cannot be matched).  This ensures that 
   * the previous parser can continue its work normally.
   */
  function clean_tpl_stack()
  {
    if ($this->debug)
      {
        $id = $this->current_tpl->id;
        $type = $this->current_tpl->type;
        print $this->dbg_indent;
        print "** error: $fname: $id ($type)\n";
        //$this->print_stack();
      }

    $type = $this->current_tpl->type; 
    while ($type!='MainTpl' and $type!='FileTpl' and $type!='WebObjectTpl')
      {
        $tpl = $this->pop_current_tpl();
        $this->link_template($tpl);
        $type = $this->current_tpl->type; 
      }

    //reset parsing modes to default and safe values
    $this->copy_mode = false;
  }

  /** Create and return a new xml parser. */
  function new_xml_parser()
  {
    $xml_parser = xml_parser_create();
    //xml_parser_set_option($xml_parser, XML_OPTION_CASE_FOLDING, 1);
    xml_set_object($xml_parser, $this);
    xml_set_character_data_handler($xml_parser, "xml_character_data");
    xml_set_default_handler($xml_parser, "xml_default_handler");
    xml_set_element_handler($xml_parser,
                            "xml_start_element",
                            "xml_end_element");
    return $xml_parser;
  }

  function xml_character_data($parser, $data)
  {
    $this->append($data);
  }

  function xml_default_handler($parser, $data)
  {
    if (substr($data, 0, 5)=='<!--#')
      {
        //ignore framework comments, do nothing
      }
    else if ( (substr($data, 0, 9)=='<![CDATA[') 
              or (substr($data, 0, 3)==']]>') ) 
      {
        $type = $this->current_tpl->type;
        if ($type=='QueryTpl')
          {
            //ommit them
          }
        else
          {
            //keep them
            $this->append($data);
          }
      }
    else
      {
        //output anything else
        $this->append($data);
      }
  }

  function xml_start_element($parser, $name, $attribs)
  {
    if ($this->copy_mode)
      {
        $this->xml_start_element_copy($parser, $name, $attribs);
        return;
      }

    switch ($name)
      {
      case 'HTML':
      case 'HEAD':
      case 'BODY':
      case 'INCLUDE':
      case 'TEMPLATE':
      case 'IF':
      case 'REPEAT':
      case 'IFEMPTY':
      case 'SEPARATOR':
      case 'RSNAVIG':
      case 'WEBBOX':
      case 'WEBCLASS':
      case 'WEBOBJECT':
      case 'RECORDSET':
      case 'DBCOMMAND':
      case 'QUERY':
      case 'VAR':
      case 'PRE':
      case 'XMP':
      case 'EXAMPLE':
      case 'TEXTAREA':
      case 'UNINDENTED':
        $start_ELEMENT = 'start_'.$name;
        $this->$start_ELEMENT($attribs);
        break;

      case 'REPEATBODY':
        //do nothing
        break;

      case 'BR':
      case 'HR':
      case 'IMG':
      case 'INPUT':
      case 'META':
      case 'LINK':
        $name = strtolower($name);
        $attr_list = $this->get_attr_list($attribs);
        $this->append("<${name}${attr_list} />");
        break;

      default:
        $name = strtolower($name);
        $attr_list = $this->get_attr_list($attribs);
        $this->append("<${name}${attr_list}>");
        break;
      }
  }

  function xml_end_element($parser, $name)
  {
    if ($this->copy_mode)
      {
        $this->xml_end_element_copy($parser, $name);
        return;
      }

    switch ($name)
      {
      case 'HTML':
      case 'HEAD':
      case 'BODY':
      case 'IFEMPTY':
      case 'SEPARATOR':
      case 'RECORDSET':
      case 'DBCOMMAND':
      case 'QUERY':
      case 'VAR':
      case 'PRE':
      case 'XMP':
      case 'TEXTAREA':
      case 'UNINDENTED':
        $end_ELEMENT = 'end_'.$name;
        $this->$end_ELEMENT();
        break;

      case 'TEMPLATE':
      case 'IF':
      case 'REPEAT':
      case 'RSNAVIG':
        $tpl = $this->pop_current_tpl();
        $this->link_template($tpl);
        break;

      case 'WEBAPP_XHTML_TEMPLATE':
      case 'REPEATBODY':
      case 'WEBOBJECT':
        //do nothing, just ignore them
        break;

      case 'INCLUDE':
      case 'BR':
      case 'HR':
      case 'IMG':
      case 'INPUT':
      case 'META':
      case 'LINK':
        //do nothing, just ignore them
        break;

      default:
        $name = strtolower($name);
        $this->append("</$name>");
        break;
      }
  }

  function xml_start_element_copy($parser, $name, $attribs)
  {
    switch ($name)
      {
      case 'WEBBOX':
      case 'WEBCLASS':
      case 'EXAMPLE':
        $this->copy_level++;
        $name = strtolower($name);
        $attr_list = $this->get_attr_list($attribs);
        $this->append("<$name$attr_list>");
        break;

      case 'PARAMETER':
        //do not copy it, process it instead
        $start_ELEMENT = 'start_'.$name;
        $this->$start_ELEMENT($attribs);
        break;

      default:
        $name = strtolower($name);
        $attr_list = $this->get_attr_list($attribs);
        $this->append("<$name$attr_list>");
        break;
      }
  }

  function xml_end_element_copy($parser, $name)
  {
    switch ($name)
      {
      case 'WEBBOX':
      case 'WEBCLASS':
      case 'EXAMPLE':
        if ($this->copy_level==0)
          {
            $end_ELEMENT = 'end_'.$name;
            $this->$end_ELEMENT();
          }
        else
          {
            $name = strtolower($name);
            $this->append("</$name>");
            $this->copy_level--;
          }
        break;

      case 'PARAMETER':
        //do nothing
        break;

      default:
        $name = strtolower($name);
        $this->append("</$name>");
        break;
      }
  }

  /** Return a string with the attributes in the given array. */
  function get_attr_list($attribs)
  {
    while (list($attr_name, $attr_value) = each($attribs))
      {
        $attr_name = strtolower($attr_name);
        $attr_list .= " $attr_name=\"$attr_value\"";
      }

    return $attr_list;
  }

  /** Append $data to the contents of the current_tpl. */
  function append($data)
  {
    if ($this->current_tpl==UNDEFINED)
      {
        print $data;
        return;
      }

    $this->current_tpl->contents .= $data;
  }

  /** Push current_tpl to the stack and set $tpl as current_tpl. */
  function push_current_tpl(&$tpl)
  {
    //push the current_tpl on tpl_stack
    array_push($this->tpl_stack, $this->current_tpl);

    //make current the new template
    $this->current_tpl = &$tpl;

    //Add the current template to the collection
    //of the templates of the $webPage.
    //Normally it should be done in the pop_current_tpl() function
    //however it is done here as well so that they are
    //stored in $webPage in the parsing order, not in 
    //reverse order.
    global $webPage;
    $webPage->addTemplate($this->current_tpl);

    if ($this->debug)
      {
        $id = $this->current_tpl->id;
        $type = $this->current_tpl->type;
        $this->dbg_indent = '| '.$this->dbg_indent;
        print $this->dbg_indent;
        print "++ push: $type: $id\n";
      }
  }

  /**
   * Parsing of the current_tpl has finished;
   * pop it and store it in template collection (in $webPage).
   * Normalize the empty space of the template as well. 
   */
  function pop_current_tpl($normalize =true)
  {
    if ($this->debug)
      {
        $id = $this->current_tpl->id;
        $type = $this->current_tpl->type;
        print $this->dbg_indent;
        print "-- pop: $type: $id\n";
        $this->dbg_indent = substr($this->dbg_indent, 2);
        //print $this->current_tpl->toHtmlTable();  //debug
      }
        
    //pop the last template from tpl_stack into current_tpl
    $tpl = $this->current_tpl;
    $last = count($this->tpl_stack) - 1;
    $this->current_tpl = &$this->tpl_stack[$last];
    array_pop($this->tpl_stack);

    if ($normalize)  $tpl->normalize_space();

    //add the template to the collection of templates
    global $webPage;
    $webPage->addTemplate($tpl);

    return $tpl;
  }
   
  /** Create links between the current_tpl and the given template. */
  function link_template($tpl)
  {
    //set a reference from the current_tpl to the new template;
    $this->append('&&'.$tpl->id.';;');
  }

  function start_HTML($attribs)
  {
    if ($this->current_tpl->type=='MainTpl')
      {
        $attr_list = $this->get_attr_list($attribs);
        $this->append('<html'.$attr_list.'>');

        //save the prolog of the main template for the other templates
        $this->xml_prolog = $this->current_tpl->contents;
      }
    else
      {
        //discard the xml prolog parsed up to now
        $this->current_tpl->contents = '';
      }
  }
  function end_HTML()
  {
    if ($this->current_tpl->type=='MainTpl')  $this->append('</html>');
  }
        
  function start_INCLUDE($attribs)
  {
    //get the filename
    $tpl_filename = $attribs['SRC'];
    $tpl_filename = WebApp::replaceVars($tpl_filename);

    //create a new file template
    $tpl = new FileTpl($tpl_filename);
    $tpl->parent = &$this->current_tpl;

    //parse the file into this FileTpl
    $tpl = $this->parse_file($tpl_filename, $tpl);

    if ($tpl<>UNDEFINED)
      {
        $this->link_template($tpl);
      }
  }

  function start_TEMPLATE($attribs)
  {
    $tpl = new Template;
    $this->push_current_tpl($tpl);
  }

  function start_IF($attribs)
  {
    $tpl = new IfTpl($attribs['CONDITION']);
    $this->push_current_tpl($tpl);
  }

  function start_REPEAT($attribs)
  {
    $rs_id = $attribs['RS'];
    $tpl = new RepeatTpl($rs_id);
    $this->push_current_tpl($tpl);
  }
        
  function start_IFEMPTY($attribs)
  {
    $tpl_id = $this->current_tpl->id.'_IfEmpty';

    //create a new template of this id and this type
    $tpl = new Template($tpl_id, 'IfEmpty');

    //the parent of $tpl is the current_tpl
    $tpl->parent = &$this->current_tpl;

    //set $tpl as current_tpl
    $this->push_current_tpl($tpl);
  }
  function end_IFEMPTY()
  {
    $tpl = $this->pop_current_tpl();

    //add the subtemplate to the Repeat template 
    if ($this->current_tpl->type=="RepeatTpl")
      {
        $this->current_tpl->if_empty = $tpl;
      }
    else
      {
        $msg = TF_("<IfEmpty> element can be used only inside a <Repeat> element");
        $err_msg = WebApp::error_msg($msg);
        $this->append($err_msg);
        $this->link_template($tpl);
      }
  }

  function start_SEPARATOR($attribs)
  {
    $tpl_id = $this->current_tpl->id.'_Separator';

    //create a new template of this id and this type
    $tpl = new Template($tpl_id, 'Separator');

    //the parent of $tpl is the current_tpl
    $tpl->parent = &$this->current_tpl;

    //set $tpl as current_tpl
    $this->push_current_tpl($tpl);
  }
  function end_SEPARATOR()
  {
    $tpl = $this->pop_current_tpl();

    //add the subtemplate to the Repeat template 
    if ($this->current_tpl->type=="RepeatTpl")
      {
        $this->current_tpl->separator = $tpl;
      }
    else
      {
        $msg = TF_("<Separator> element can be used only inside a <Repeat> element");
        $err_msg = WebApp::error_msg($msg);
        $this->append($err_msg);
        $this->link_template($tpl);
      }
  }

  function start_RSNAVIG($attribs)
  {
    $rs_id = $attribs['RS'];

    //create a new template of this id and this type
    $tpl = new RSNavigTpl($rs_id);

    //set $tpl as current_tpl
    $this->push_current_tpl($tpl);
  }

  function start_HEAD($attribs)
  {
    if ($this->current_tpl->type=="MainTpl")
      {
        $tpl = new Template("HeadTpl", "HeadTpl");
        $this->push_current_tpl($tpl);
        $this->append('<head>');
      }
    else
      {
        $tpl = new Template;
        $file = $this->files[sizeof($this->files)-1];
        $url = WebApp::to_url(dirname($file).'/');
        $tpl->addVar('./', "'$url'");
        $this->push_current_tpl($tpl);
      }
  }
  function end_HEAD()
  {
    global $webPage;

    if ($this->current_tpl->type=="HeadTpl")
      {
        $this->append('</head>');
        $tpl = $this->pop_current_tpl();
        $this->link_template($tpl);
        $this->current_tpl->head = &$webPage->tpl_collection[$tpl->id];
      }
    else
      {
        $tpl = $this->pop_current_tpl();
        $tpl->parent = &$webPage->tpl_collection['HeadTpl'];
        $webPage->append_to_head('&&'.$tpl->id.';;');
      }
  }

  function start_BODY($attribs)
  {
    if ($this->current_tpl->type=="MainTpl")
      {
        $tpl = new Template("BodyTpl", "BodyTpl");
        $this->push_current_tpl($tpl);
        $this->append('<body'.$this->get_attr_list($attribs).'>');
      }
    else
      {
        $tpl = new Template;
        $file = $this->files[sizeof($this->files)-1];
        $url = WebApp::to_url(dirname($file).'/');
        $tpl->addVar('./', "'$url'");
        $this->push_current_tpl($tpl);
      }
  }
  function end_BODY()
  {
    $tpl = $this->pop_current_tpl();

    //link it to the MainTpl
    $this->link_template($tpl);
    if ($this->current_tpl->type=="MainTpl")
      {
        global $webPage;
        $this->current_tpl->body = &$webPage->tpl_collection[$tpl->id];
      }
  }

  function start_WEBBOX($attribs)
  {
    $id = $attribs['ID'];

    //create a new WebClassTpl template
    $file = $this->files[sizeof($this->files)-1];
    $webClassTpl = new WebClassTpl($id, dirname($file).'/', $attribs);
    $webClassTpl->parent = &$this->current_tpl;

    $webClassTpl->before_parse();
    $this->push_current_tpl($webClassTpl);

    //parse <WebBox> in copy mode
    $this->copy_mode = true;
    $this->copy_level = 0;
  }
  function end_WEBBOX()
  {
    $this->copy_mode = false;
    $webClassTpl = $this->pop_current_tpl();
    $webClassTpl->after_parse();

    //create a WebObject template for the webbox              
    $box_id = $webClassTpl->id;
    $tpl = new $box_id;
    $tpl->class = $webClassTpl;
    $tpl->params = array();
    $tpl->parent = &$this->current_tpl;
    $tpl->construct_WebObject($box_id);

    //get state vars and call init(), if this is the first time
    $tpl->getStateVars();

    //set a reference from the current template to the new template
    $this->link_template($tpl);

    //parse the content of the class into the object
    global $tplVars;
    $tplVars->pushScope();
    $tplVars->addVar("./", $webClassTpl->path);
    $tplVars->addVars($tpl->getObjVars());
    $tpl->before_parse();
    $webclass_contents = str_replace('&', '&amp;', $webClassTpl->contents);
    $tpl = $this->parse_string($webclass_contents, $tpl);
    $tpl->after_parse();
    $tplVars->popScope();
  }

  function start_WEBCLASS($attribs)
  {
    $id = $attribs['ID'];

    //check that a webclass with such an id is not already loaded
    global $webPage;
    $tpl = $webPage->getTemplate($id);

    //$reinclude is true when the same webclass is included again
    $reinclude = ( $tpl!=UNDEFINED
                   and $tpl->parent->id == $this->current_tpl->id );

    //$redeclaration is true if this is another webclass with the same id
    $redeclaration = ( $tpl!=UNDEFINED
                       and $tpl->parent->id != $this->current_tpl->id );

    if ($redeclaration)
      {
        $msg = TF_("Redeclaration of <WebClass ID=\"v_id\"> in 'v_path'.");
        $msg = str_replace('v_id', $id, $msg);
        $msg = str_replace('v_path', $this->current_tpl->id, $msg);
        $warning_msg = WebApp::warning_msg($msg);
        $this->append($warning_msg);
      }

    //create a new template
    $file = $this->files[sizeof($this->files)-1];
    $tpl = new WebClassTpl($id, dirname($file).'/', $attribs);
    $tpl->parent = &$this->current_tpl;

    $this->push_current_tpl($tpl);
    if (!$reinclude)  $tpl->before_parse();

    //copy the <WebClass> element into $tpl
    $this->copy_mode = true;
    $this->copy_level = 0;
  }
  function end_WEBCLASS()
  {
    $this->copy_mode = false;
    $tpl = $this->pop_current_tpl();
    $tpl->after_parse();
  }

  function start_PARAMETER($attribs)
  {
    $name = $attribs['NAME'];
    $default = $attribs['DEFAULT'];

    if ($this->current_tpl->type=="WebClassTpl")
      {
        $name = strtolower($name);
        $this->current_tpl->params[$name] = $default;
      }
    else
      {
        //give an error message
        $msg = TF_("<parameter/> can be used only inside a <webclass> element.");
        $err_msg = WebApp::error_msg($msg);
        $this->append($err_msg);
      }
  }

  function start_WEBOBJECT($attribs)
  {
    global $webPage;

    //get the attributes of the webobject
    $class_name = $attribs['CLASS'];
    $obj_name = $attribs['NAME'];
    $obj_name = WebApp::replaceVars($obj_name);
    $full_obj_name = $class_name . "::" . $obj_name;

    //get the webclass of this webObj
    if (isset($webPage->tpl_collection[$class_name]))
      {
        $webClassTpl = &$webPage->tpl_collection[$class_name];
      }
    else
      {
        $webobj_elem = '<WebObject'.$this->get_attr_list($attribs).' />';
        $msg = TF_("<WebClass> 'v_class_name' is not defined.");
        $msg = str_replace('v_class_name', $class_name, $msg);
        $this->append(WebApp::error_msg($webobj_elem));
        $this->append(WebApp::error_msg($msg));
        return;
      }

    //create a WebObject template
    $tpl = new $class_name;
    $tpl->class = &$webPage->tpl_collection[$class_name];
    $tpl->parent = &$this->current_tpl->parent;
    $tpl->params = $webClassTpl->params;
    $tpl->construct_WebObject($full_obj_name);

    //get the values of the parameter attributes
    while (list($p_name,$p_default) = each($tpl->params))
      {
        if (isset($attribs[strtoupper($p_name)]))
          {
            $tpl->params[$p_name] = $attribs[strtoupper($p_name)];
          }
      }

    //get state vars and call init(), if this is the first time
    $tpl->getStateVars();

    //set a reference from the current template to the new template
    $this->link_template($tpl);

    //parse the content of the class into the object
    global $tplVars;
    $tplVars->pushScope();
    $tplVars->addVar("./", $webClassTpl->path);
    $tplVars->addVars($tpl->getObjVars());
    $tpl->before_parse();
    $webclass_contents = str_replace('&', '&amp', $webClassTpl->contents);
    $tpl = $this->parse_string($webclass_contents, $tpl);
    $tpl->after_parse();
    $tplVars->popScope();
  }
        
  function start_RECORDSET($attribs)
  {
    static $tpl_id = "Recordset_001";
    $tpl = new Template($tpl_id++, 'RecordsetTpl');
    $id = $attribs['ID'];
    $type = (isset($attribs['TYPE']) ? $attribs['TYPE'] : UNDEFINED);
    $tpl->addVar('ID', $id);
    $tpl->addVar('TYPE', $type);
    if ($type=="PagedRS")
      {
        $recs_per_page = $attribs['RECS_PER_PAGE'];  
        $tpl->addVar('RECS_PER_PAGE', $recs_per_page);
      }

    $this->push_current_tpl($tpl);
  }
  function end_RECORDSET()
  {
    $tpl = $this->pop_current_tpl();

    //$class_id = $this->get_parentclass_id();
    //$id = $class_id.'::'.$tpl->vars['ID'];
    $id = $tpl->vars['ID'];
    $type = $tpl->vars['TYPE'];
    if ($type=="PagedRS")
      {
        $recs_per_page = $tpl->vars['RECS_PER_PAGE'];  
      }
    $query = $tpl->vars['QUERY'];
                
    //create the Recordset object
    switch ($type)
      {
      default:
        $msg = TF_("Recordset 'v_id': type 'v_type' is unknown");
        $msg = str_replace('v_id', $id, $msg);
        $msg = str_replace('v_type', $type, $msg);
        print WebApp::error_msg($msg);
      case UNDEFINED:
      case "EditableRS":
        $rs = new EditableRS($id, $query);
        break;
      case "StaticRS":
        $rs = new StaticRS($id, $query);
        break;
      case "DynamicRS":
        $rs = new Recordset($id, $query);
        break;
      case "PagedRS":
        $rs = new PagedRS($id, $query, $recs_per_page);
        break;
      case "TableRS":
        $rs = new TableRS($id, $query);
        break;
      }

    global $webPage;
    $webPage->addRecordset($rs);
  }
  //return the id of the parent class that contains the current template
  function get_parentclass_id()
  {
    if ($this->current_tpl->type=='WebClassTpl')
      return $this->current_tpl->id;

    $idx = sizeof($this->tpl_stack) - 1;
    while ($idx >= 0)
      {
	if ($this->tpl_stack[$idx]->type=='WebClassTpl')
	  return $this->tpl_stack[$idx]->id;
	$idx--;
      }

    return '';
  }
        
  function start_DBCOMMAND($attribs)
  {
    static $tpl_id = "dbCommand_001";
    $tpl = new Template($tpl_id++, 'dbCommandTpl');
    $id = $attribs['ID'];
    $tpl->addVar('ID', $id);
    $this->push_current_tpl($tpl);
  }
  function end_DBCOMMAND()
  {
    $tpl = $this->pop_current_tpl();
    $id = $tpl->vars['ID'];
    $query = $tpl->vars['QUERY'];

    $rs = new Recordset($id, $query);
    global $webPage;
    $webPage->addRecordset($rs);
  }   

  function start_QUERY($attribs)
  {
    static $tpl_id = "Query_001";
    $tpl = new Template($tpl_id++, 'QueryTpl');
    $this->push_current_tpl($tpl);
  }
  function end_QUERY()
  {
    $tpl = $this->pop_current_tpl();
    $query = $tpl->contents;
    //ToDo: check that current_tpl is a recordset template
    //      otherwise add the query to the first recordset template
    //      in the stack
    $this->current_tpl->addVar('QUERY', $query);
  }
        
  function start_VAR($attribs)
  {
    static $tpl_id = "Var_001";

    $var_name = $attribs['NAME'];
    $tpl = new Template($tpl_id++, 'VarTpl');
    $tpl->addVar('name', $var_name);
    $this->push_current_tpl($tpl);
  }
  function end_VAR()
  {
    $tpl = $this->pop_current_tpl();
    $var_name = $tpl->vars['name'];
    $expression = $tpl->contents;
    $this->current_tpl->addVar($var_name, $expression);
  }

  function start_PRE($attribs)
  {
    static $tpl_id = "Pre_001";
    $tpl = new Template($tpl_id++, 'PreTpl');
    $tpl->attr_list = $this->get_attr_list($attribs);
    $this->push_current_tpl($tpl);
  }
  function end_PRE()
  {
    //the param 'false' in order not to normalize space
    $tpl = $this->pop_current_tpl(false);
    $this->link_template($tpl);
  }

  function start_XMP($attribs)
  {
    static $tpl_id = "Xmp_001";
    $tpl = new Template($tpl_id++, 'XmpTpl');
    $tpl->attr_list = $this->get_attr_list($attribs);
    $this->push_current_tpl($tpl);
  }
  function end_XMP()
  {
    //the param 'false' in order not to normalize space
    $tpl = $this->pop_current_tpl(false);
    $this->link_template($tpl);
  }

  function start_TEXTAREA($attribs)
  {
    static $tpl_id = "TextArea_001";
    $tpl = new Template($tpl_id++, 'TextAreaTpl');
    $tpl->attr_list = $this->get_attr_list($attribs);
    $this->push_current_tpl($tpl);
  }
  function end_TEXTAREA()
  {
    //the param 'false' in order not to normalize space
    $tpl = $this->pop_current_tpl(false);
    $this->link_template($tpl);
  }

  function start_EXAMPLE($attribs)
  {
    static $tpl_id = "Example_001";
    $tpl = new Template($tpl_id++, 'ExampleTpl');
    $tpl->attr_list = $this->get_attr_list($attribs);
    $this->push_current_tpl($tpl);
    $this->copy_mode = true;
  }
  function end_EXAMPLE()
  {
    $this->copy_mode = false;
    //the param 'false' in order not to normalize space
    $tpl = $this->pop_current_tpl(false);
    $this->link_template($tpl);
  }

  function start_UNINDENTED($attribs)
  {
    static $tpl_id = "Unindented_001";
    $tpl = new Template($tpl_id++, 'UnindentedTpl');
    $tpl->attr_list = $this->get_attr_list($attribs);
    $this->push_current_tpl($tpl);
  }
  function end_UNINDENTED()
  {
    //the param 'false' in order not to normalize space
    $tpl = $this->pop_current_tpl(false);
    $this->link_template($tpl);
  }

  /////////////////////////////////////////////////////////////

  /** Output the state of the stack. */
  function print_stack()
  {
    print "The state of the tpl_stack:\n";
    print " * ->: ";
    print $this->current_tpl->type;
    print " (".$this->current_tpl->id.")\n";
    for ($i=sizeof($this->tpl_stack)-1; $i >= 0; $i--)
      {
        if (gettype($this->tpl_stack[$i])=="object")
          {
            print " * $i: ";
            print $this->tpl_stack[$i]->type;
            print " (".$this->tpl_stack[$i]->id.")\n";
          }
        else
          {
            print " * $i: ".$this->tpl_stack[$i]."\n";
          }
      }
  }
}
?>