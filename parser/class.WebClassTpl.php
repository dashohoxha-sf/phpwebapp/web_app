<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * The template of a <WebClass> element.
   * @package parser 
   */
class WebClassTpl extends Template
{
  /** Path where the WebClassTpl is located. */
  var $path;

  /** File that contains the php component. */
  var $php_file;
  /** File that contains the db component. */
  var $db_file;
  /** File that contains the js component. */
  var $js_file;
  /** File that contains the css component. */
  var $css_file;

  /** Array of parameters and their default values. */
  var $params;

  /** 
   * The number of the objects of this class. It is  
   * incremented each time that a new object is created. */
  var $obj_count; 

  function WebClassTpl($id, $path =UNDEFINED, $attribs =array())
  {
    $this->Template($id, "WebClassTpl");
    $this->path = $path;
    $this->params = array();
    $this->obj_count = 0;

    $this->set_component_paths($id, $path, $attribs);
  }

  /**
   * Set the paths of the webclass components (php, db, js, css).
   * $attribs is an associated array with the attributes of the <WebClass>
   * tag.
   */
  function set_component_paths($id, $path, $attribs)
  {
    $component_path = $attribs['PATH'];
    if ($component_path=='')  $component_path = $path;

    $php_file = $attribs['PHP'];
    $php_file = WebApp::replaceVars($php_file);
    if (substr($php_file, -1)=='/')  $php_file .= "$id.php";
    if ($php_file=='') $php_file = $component_path."$id.php";

    $db_file = $attribs['DB'];
    $db_file = WebApp::replaceVars($db_file);
    if (substr($db_file, -1)=='/')  $db_file .= "$id.db";
    if ($db_file=='') $db_file = $component_path."$id.db";

    $js_file = $attribs['JS'];
    $js_file = WebApp::replaceVars($js_file);
    if (substr($js_file, -1)=='/')  $js_file .= "$id.js";
    if ($js_file=='') $js_file = $component_path."$id.js";

    $css_file = $attribs['CSS'];
    $css_file = WebApp::replaceVars($css_file);
    if (substr($css_file, -1)=='/')  $css_file .= "$id.css";
    if ($css_file=='') $css_file = $component_path."$id.css";

    $this->php_file = $php_file;
    $this->db_file  = $db_file;
    $this->js_file  = $js_file;
    $this->css_file = $css_file;
  }

  function before_parse()
  {
    //include the PHP class of the webClass
    $this->include_php_class();

    //save the current database connection and set
    //the connection of this webobject as the current one
    $wobj = new $this->id;
    if (isset($wobj->cnn))
      {
	global $cnn;
	$this->cnn_bak = clone($cnn);
	$cnn = clone($wobj->cnn);
      }
    unset($wobj);

    //load the .db file of the webclass
    $this->parse_db_file();
        
    //include the JS code of the webClass
    $this->include_js_code();

    //include the CSS code of the webClass
    $this->include_css_code();
  }

  function after_parse()
  {
    //restore the saved db connection as the current connection
    if (isset($this->cnn_bak))  
      {
	global $cnn;
	$cnn = clone($this->cnn_bak);    
	unset($this->cnn_bak);
      }
  }
        
  /** Parse the .db file of the webclass, if it exists. */
  function parse_db_file()
  {
    $fname = $this->db_file;
    if (file_exists($fname))
      {
        //parse the file
        global $parser;
        $parser->parse_file($fname);
      }
  }

  /**
   * Look for the PHP file in the same folder as WebClass
   * (with the same name as the webclass' ID) and include it
   * if it exists; if not, define a dummy class for this webclass.
   */
  function include_php_class()
  {
    $fname = $this->php_file;
    if (file_exists($fname))
      {
        include_once $fname;
      }
    else
      {
        //create a dummy class for this WebClass
        $class_name = $this->id;
        $arr_methods = get_class_methods($class_name);
        if (sizeof($arr_methods)==0)
          {
            $dummy_class_definition = 
              "class $class_name extends WebObject 
                 {
                   /** 
                    * We declare a dummy method in this dummy class
                    * so that get_class_methods() doesn't return
                    * an empty array and we can check whether such a
                    * class has been defined before or not.
                    */
                   function $class_name() 
                     {
                     }
                 }";
            eval($dummy_class_definition);
          }   
      }
  }

  /**
   * Looks for the JS file in the same folder as WebClass
   * (with the same name as the webclass' ID) and includes it
   * in the <head> of the page, if it exists.
   */
  function include_js_code()
  {
    $fname = $this->js_file;
    if (file_exists($fname))
      {
        global $webPage;

        //include the JS file in the <head> of the page
        $fname = WebApp::to_url($fname);
        $include_js = "  <script type=\"text/javascript\" language=\"javascript\" src=\"$fname\"></script>\n";
        $webPage->append_to_head($include_js);
      }
  }

  /**
   * Looks for the CSS file in the same folder as WebClass
   * (with the same name as the webclass' ID) and includes it
   * in the <head> of the page, if it exists.
   */
  function include_css_code()
  {
    $fname = $this->css_file;
    if (file_exists($fname))
      {
        global $webPage;
        $fname = WebApp::to_url($fname);
        $include_css = "  <link type=\"text/css\" rel=\"stylesheet\" href=\"$fname\" />\n";
        $webPage->append_to_head($include_css);
      }
  }

  /** for debug */
  function attribs2html()
  {
    reset($this->params);
    while ( list($p_name,$expr) = each($this->params) )
      {
        $params .= " $p_name=($expr); ";
      }
    $html = Template::attribs2html() . "
  <tr>
    <td bgcolor='#eeeeee' valign='top' align='right'>Path:</td>
    <td bgcolor='#f9f9f9'>" . $this->path ." </td>
  </tr>
  <tr>
    <td bgcolor='#eeeeee' valign='top' align='right'>Params:</td>
    <td bgcolor='#f9f9f9'>" . $params ." </td>
  </tr>
";
    return $html;
  }
}
?>