<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /** @package parser */
  /** */
include_once PARSER_PATH."class.WebPage.php";
include_once PARSER_PATH."class.VarStack.php";
include_once PARSER_PATH."class.Parser.php";
include_once PARSER_PATH."class.Render.php";

include_once PARSER_PATH."class.Template.php";
include_once PARSER_PATH."class.FileTpl.php";
include_once PARSER_PATH."class.MainTpl.php";
include_once PARSER_PATH."class.IfTpl.php";
include_once PARSER_PATH."class.RepeatTpl.php";
include_once PARSER_PATH."class.RSNavigTpl.php";
include_once PARSER_PATH."class.WebClassTpl.php";
include_once PARSER_PATH."class.WebObjectTpl.php";
include_once PARSER_PATH."class.WebObject.php";
?>