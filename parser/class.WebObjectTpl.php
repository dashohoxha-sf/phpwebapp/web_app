<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * The template of a <WebObject> tag (which is copied
   * from the corresponding WebClass).
   * @package parser 
   */
class WebObjectTpl extends Template
{
  /** A reference to the WebClassTpl of this objectTpl. 
   * @see WebClassTpl */
  var $class;

  /** An associative array of parameters and their values. */
  var $params;

  /** A reference to the scope of the object in the var stack. */
  var $scope;

  /** The count of the object. */
  var $obj_count;

  /** Helps to render the template with a good indentation. */
  var $indent;

  /** The database connection of the webobject. */
  var $cnn;
  /** Used to backup and restore the global database connection. */
  var $cnn_bak;

  /** Constructor */
  function WebObjectTpl($webObjId)
  {
    //call constructor of the superclass
    $this->Template($webObjId, "WebObjectTpl");
      
    //increment the object counter of the class and set the count
    $this->class->obj_count++;
    $this->obj_count = $this->class->obj_count;
  }

  /** This is called before the template of the WebObject is parsed. */
  function before_parse()
  {   
    //save the current database connection and set
    //the connection of this webobject as the current one
    if (isset($this->cnn))
      {
	global $cnn;
	$this->cnn_bak = clone($cnn);
	$cnn = clone($this->cnn);
      }

    //add the state vars of this webobj to the current scope
    global $tplVars;
    $tplVars->addVars($this->getSVars());
    //save the current scope of the var stack
    $this->scope = $tplVars->getCurrentScope();

    //call the event handler
    $this->handle_event();
      
    //call the function onParse
    $this->onParse();
  }

  /** This is called after the template of the WebObject is parsed. */
  function after_parse()
  {
    //call the function afterParse
    $this->afterParse();

    $this->scope = 0;

    //restore the saved db connection as the current connection
    if (isset($this->cnn_bak))  
      {
	global $cnn;
	$cnn = clone($this->cnn_bak);    
	unset($this->cnn_bak);
      }
  }

  /** This is called before the template is rendered. */
  function before_render()
  {
    global $tplVars, $l10n, $webPage;

    //save the current database connection and set
    //the connection of this webobject as the current one
    if (isset($this->cnn))
      {
	global $cnn;
	$this->cnn_bak = clone($cnn);
	$cnn = clone($this->cnn);
      }

    //use another translation file, if such a file exists
    $l10n->set_translation_file($this->class->path, $this->id);    

    //save the current scope of the var stack
    $this->scope = $tplVars->getCurrentScope();

    //add state vars to the current scope
    $tplVars->addVars($this->getSVars());

    //add some special variables which identify this object
    $tplVars->addVars($this->getObjVars());

    //add parameters to the current scope
    $tplVars->addVars($this->getParams());

    //add the class url variable {{./}}
    $class_path = $this->class->path;
    $class_url = WebApp::to_url($class_path);
    $tplVars->addVar("./", $class_url);

    //extract translatable messages in the JS code
    $this->extract_js_l10n_messages();

    //call the function onRender of this WebObject
    $this->onRender(); 
  }

  /** This is called after the template is rendered. */
  function after_render()
  {
    //call the function afterRender() of this WebObject
    $this->afterRender();

    //restore the saved db connection as the current connection
    if (isset($this->cnn_bak))  
      {
	global $cnn;
	$cnn = clone($this->cnn_bak);
	unset($this->cnn_bak);
      }
  }

  /**
   * Extract translatable messages (which are denoted by T_("...")
   * or TF_("...") ) from a JS file. Called by before_render().
   */
  function extract_js_l10n_messages()
  {
    $fname = $this->class->path . $this->class->id . ".js";
    if (file_exists($fname))
      {
        global $webPage;
        $webPage->extract_js_l10n_messages($fname);
      }
  }

  /**
   * This function is called by beforeParse().
   * - If the event is targeted to this webobj, it calls
   *   the event hadler of the event, which has the same
   *   name as the event.
   * - If a specific event handler does not exist,
   *   it calls eventHandler() of the webclass.
   */
  function handle_event()
  {
    global $event;

    if ( $event->target==$this->id 
         or $event->target=="any" )
      {
        $method_name = "on_".$event->name;
        if (method_exists($this, $method_name))
          {
            //the webclass has declared a specific 
            //event handler for this event, call it
            $this->$method_name($event->args);
          }
        else
          {
            //call the general event handler of the webox
            $this->eventHandler($event);
          }
      }
  }

  /** Returns an array with all the evaluated parameters. */
  function getParams()
  {
    $params = $this->params;
    reset($params);
    while ( list($p_name,$expr) = each($params) )
      {
        $expr = WebApp::replaceVars($expr);
        //$params[$p_name] = WebApp::evaluate($expr);
        $params[$p_name] = $expr;
      }
    return $params;
  }

  function is_webbox()
  {
    $pos = strpos($this->id, "::");
    if ($pos===false)  return true;
    else return false;
  }

  function get_obj_name()
  {
    $pos = strpos($this->id, "::");
    if ($pos===false)  return $this->id; //this is a webbox
    else return substr($this->id, $pos+2);
  }

  function get_class_name()
  {
    $pos = strpos($this->id, "::");
    if ($pos===false) return $this->id; //this is a webbox
    else return substr($this->id, 0, $pos);
  }

  /**
   * Returns an array with some special variables that identify
   * the object, like: obj_id, obj_name, class_name, obj_count, etc.  
   * which can be used in the template of the webclass.
   */
  function getObjVars()
  {
    $vars = array(
                  "obj_count" => $this->obj_count,
                  "obj_id"    => $this->id,
                  "obj_name"  => $this->get_obj_name(),
                  "class_name"=> $this->get_class_name()
                  );
    return $vars;
  }

  /** debug */
  function attribs2html()    {
    reset($this->params);
    while ( list($p_name,$expr) = each($this->params) )
      {
        $params .= " $p_name=($expr); ";
      }
    $html = Template::attribs2html() . "
  <tr>
    <td bgcolor='#eeeeee' valign='top' align='right'>Params:</td>
    <td bgcolor='#f9f9f9'>" . $params ." </td>
  </tr>
  <tr>
    <td bgcolor='#eeeeee' valign='top' align='right'>Scope:</td>
    <td bgcolor='#f9f9f9'>" . $this->scope ." </td>
  </tr>
";
    return $html;
  }




  /** Set the database connection of the object. */
  function setConnection($mdb2_dsn, $mdb2_options =array())
  {
    $this->cnn = new Connection($mdb2_dsn, $mdb2_options);
  }

  /**
   * Open the given recordset, using an apropriate connection.
   */
  function openRS($id, $params =UNDEFINED, $conn =UNDEFINED)
  {
    //$class_id = $this->class->id;
    //$rs_id = $class_id.'::'.$id;
    $rs_id = $id;
    return WebApp::openRS($rs_id, $params, $conn);
  }

  /**
   * Execute the given DB command, using an apropriate connection.
   */
  function execDBCmd($id, $params =UNDEFINED, $conn =UNDEFINED)
  {
    //$class_id = $this->class->id;
    //$rs_id = $class_id.'::'.$id;
    $rs_id = $id;
    return WebApp::execDBCmd($rs_id, $params, $conn);
  }
}
?>