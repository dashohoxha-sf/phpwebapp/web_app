<?php
  /**
   * @package parser
   */

$str = "test
  


   abc&&test1;;def



     xyz
     uvt







    &&test2;;



     ";

//if there are 2 or more empty lines, replace them by a single one
$str = preg_replace('/\n([ \t]*\n){2,}/', "\n\n", $str);

//remove any empty lines in the begining
$str = preg_replace('/^\s*\n/', '', $str);

//remove any empty lines in the end
$str = preg_replace('/\s*$/', '', $str);

//Trim the indentation from the beginning of each line.
//get the indentation of each line
preg_match_all("/^( *)[^\n]/m", $str, $arr_matches); 
$arr_indents = $arr_matches[1];
//sort them
asort($arr_indents);
$arr_indents = array_values($arr_indents);
//get the smallest one
$smallest_indent = $arr_indents[0];
//remove it from the begining of each line
$str = preg_replace("/^$smallest_indent/m", '', $str);

      
//split the string back to the content array
$flags = PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE;
$arr = preg_split('/(&&[^;]+;;)/', $str, -1, $flags);

$str1 = " djdj name='test3' <form jjdjdj name='form1' jd> ksksk name='test2' ";
$pattern = '/<form[^>]*name=("|\')([^\\1]+)\\1/'; //ToDo: test it
preg_match($pattern, $str1, $matches);
$form_name = $matches[2];

$str2 = "  hhhsh  \n jksksks\n     ";
preg_match('/([ \t]*)$/', $str2, $matches);
$indent =  $matches[1];

print "<xmp>\n";
//print "'$form_name'\n";
//print "'$indent'\n";
print "'$str' \n";
print_r($arr);
print "</xmp>\n";
?>