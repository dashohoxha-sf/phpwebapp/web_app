<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * A template that is repeated for each record of a recordset.
   * @package parser 
   */
class RepeatTpl extends Template
{
  /** The id of the recordset. */
  var $rsId;

  /** The template that is displayed if the RS is empty. */
  var $if_empty;

  /** The separator template. */
  var $separator;
        
  function RepeatTpl($rs_id)
  {
    static $id = "RepeatTpl_01";
    Template::Template($id++, "RepeatTpl");   //call the constructor of the superclass        
                
    $this->rsId = $rs_id;
    $this->if_empty = new Template($this->id."_IfEmpty", "IfEmpty");
    $this->separator = new Template($this->id."_Separator", "Separator");
  }

  /** for debug */
  function attribs2html()
  {
    $html = Template::attribs2html() . "
  <tr>
    <td bgcolor='#eeeeee' valign='top' align='right'>RS:</td>
    <td bgcolor='#f9f9f9'>" . $this->rsId ." </td>
  </tr>
";
    return $html;
  }
}
?>