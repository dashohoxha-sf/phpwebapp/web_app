<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /** include timing functions */
include_once TIMER_PATH."class.Timer.php";

/**
 * Keeps the templates, recordsets etc. that are used
 * to build an HTML page.
 *
 * @package parser
 */
class WebPage
{
  /** The file name of the main template. */
  var $tpl_file;

  /** Reference to the main template of the page. */
  var $rootTpl;
        
  /** Collection of all the templates of the page. */
  var $tpl_collection;

  /** Collection of all the recordsets of the page. */
  var $rs_collection;

  /** 
   * Keeps a list of translatable messages that are used
   * in the JavaScript code. It is an associative array
   * with msgid as key and msgstr (translation) as value.
   */
  var $js_l10n_messages;

  /** 
   * Keeps a list of messages that are displayed with alert() 
   * to the user after the page is loaded. 
   * @see WebApp::message($msg)
   */
  var $messages;

  /**
   * Keeps a list of windows that are poped up after the page is loaded.
   * It is a two dimensional array, indexed by name and with columns for
   * 'url', 'html_content', 'features' and 'timeout'.
   * @see WebApp::popup_window($name, $url, $html_content, $features, $timeout)
   */
  var $popup_windows;

  /**
   * Keeps a list of debug messages that are displayed
   * after the page is rendered.
   * @see WebApp::debug_msg()
   */
  var $dbg_messages;

  /** used to measure the execution time of several parts of the application */
  var $timer;
      
  function WebPage()
  {
    $this->collect = false;
    $this->html_page = "";

    $this->tpl_collection = array();
    $this->rs_collection = array();

    $this->js_l10n_messages = array();
    $this->messages = array();
    $this->popup_windows = array();
    $this->dbg_messages = array();

    $this->timer = new Timer;
  }

  /** Append the given string to the content of the <head> element. */
  function append_to_head($str)
  {
    $contents = $this->tpl_collection["HeadTpl"]->contents;
    $contents = ereg_replace('</head>', $str.'</head>', $contents);
    $this->tpl_collection["HeadTpl"]->contents = $contents; 
  }

  /** Append the given string to the content of the <body> element. */
  function append_to_body($str)
  {
    $this->tpl_collection["BodyTpl"]->contents .= $str; 
  }

  /**
   * Append a reference to the given template 
   * at the end of the content of the <body> element.
   */
  function link_to_body(&$tpl)
  {
    $this->tpl_collection["BodyTpl"]->contents .= '&&'.$tpl->id.';;';
  }

  /** Adds a template in the template collection. */
  function addTemplate($tpl)
  {
    if (!isset($tpl->id))
      {
        $msg = TF_("unidentified template");
        print WebApp::error_msg("WebPage::addTemplate(): $msg.\n");
        $tpl->toText();
        return;
      }
    $this->tpl_collection[$tpl->id] = $tpl;
  }

  /**
   * Returns the template with the given id from the collection,
   * or UNDEFINED if a template with this id is not found.
   */
  function getTemplate($tpl_id)
  {
    if ( !isset($this->tpl_collection[$tpl_id]) )
      {
        return UNDEFINED;
      }
    else
      {
        $tpl = &$this->tpl_collection[$tpl_id];
        return $tpl;
      }
  }

  /**
   * Returns the object with the given id from the collection,
   * or UNDEFINED if such a template is not found or it not an WebObjectTpl.
   */
  function getObject($obj_id)
  {
    $tpl = $this->getTemplate($obj_id);
    if ($tpl==UNDEFINED)
      {
        return UNDEFINED;
      }
    else if ($tpl->type=="WebObjectTpl")
      {
        return $tpl;
      }
    else
      {
        return UNDEFINED;
      }
  }
        
  /** Adds a recordset in the recordset collection. */
  function addRecordset(&$rs)
  {
    static $rsId = "recordset_00";
    if (!isset($rs->ID))
      {
        $msg = TF_("unidentified recordset");
        print WebApp::error_msg("WebPage::addRecordset(): $msg.\n");
        print $rs->toHtmlTable();
        return;
      }
    else if ($rs->ID==UNDEFINED)
      {
        $rsId++;
        $rs->ID = $rsId;
      }
    $this->rs_collection[$rs->ID] = $rs;
  }

  /**
   * Returns the recordset with the given id from the collection,
   * or UNDEFINED if a recordset with this id is not found.
   */
  function getRecordset($rs_id)
  {
    if ( !isset($this->rs_collection[$rs_id]) )
      {
        return UNDEFINED;
      }
    else
      {
        $rs = &$this->rs_collection[$rs_id];
        return $rs;
      }
  }

  /** Add a debug message to the list of debug messages. */
  function addDbgMsg($dbg_msg, $comment ="")
  {
    $len = sizeof($this->dbg_messages);
    $this->dbg_messages[$len]["dbg_msg"] = $dbg_msg;
    $this->dbg_messages[$len]["comment"] = $comment;
  }

  /**
   * Extract translatable messages (which are denoted by T_("...")
   * or TF_("...") ) from a JS file, and save them in the 
   * associative array  $this->js_l10n_messages . These messages
   * (and their translations) are later appended at the end of the page
   * in order to enable the translation of JS messages.
   * Called by WebObjTpl::before_render().
   */
  function extract_js_l10n_messages($js_filename)
  {
    //match patterns like this: T_("....") or TF_("....")
    $pattern = '#(TF?_)\("([^"]+)"\)#';
    $js_code = file_get_contents($js_filename);
    preg_match_all($pattern, $js_code, $matches);
    for ($i=0; $i < sizeof($matches[1]); $i++)
      {
        $f_name = $matches[1][$i];  //function name (T_ or TF_)
        $msgid  = $matches[2][$i];  //the string of the message
        $pattern = "\\\\n(\\\\ *\n)"; //replace '\n' by newline 
        $msgid = ereg_replace($pattern, "\n\\1", $msgid);

        //translate; either T_($msgid) or TF_($msgid)
        $msgstr = $f_name($msgid);
        $this->js_l10n_messages[$msgid] = $msgstr;
      }
  }

  /**
   * Convert the messages in $this->js_l10n_messages into
   * JavaScript code that can be used to translate JS messages.
   * Returns a JS code that is appended at the end of the page
   * (this function is called by 'l10n-module/wbJSL10n.php').
   */
  function js_l10n_messages_to_js()
  {
    $arr_msg = array();
    while ( list($msgid, $msgstr) = each($this->js_l10n_messages) )
      {
        //remove an extra slash and newline
        $msgid = str_replace("\\\n", "", $msgid);

        //replace newlines by '\n' (otherwise JS error will occur)
        $msgid = str_replace("\n", "\\n", $msgid);
        $msgstr = str_replace("\n", "\\n", $msgstr);

        //add the translation in the list of messages
        $arr_msg[] = "l10n.addMsg(\"$msgid\", \"$msgstr\");";
      }
    $js_code = implode($arr_msg, "\n  ");

    return $js_code;
  }

  /**
   * Returns JS code that displays an alert for
   * each message in the messages array.
   */
  function messages_to_js()
  {
    if (sizeof($this->messages)==0)  return "";

    $js = "<script type=\"text/javascript\" language=\"javascript\">\n";
    $js .= "//<![CDATA[\n";
    for ($i=0; $i < sizeof($this->messages); $i++)
      {
        $msg = $this->messages[$i];
        $msg = WebApp::js_encode($msg);
        $js .= "\talert('$msg');\n";
      }
    $js .= "//]]>\n";
    $js .= "</script>\n";
    return $js;
  }

  /**
   * Returns JS code that will open a popup window
   * for each window in the list popup_windows
   */
  function popup_windows_to_js()
  {
    if (sizeof($this->popup_windows)==0)  return '';
    
    $js = "<script language='javascript'>\n";
    while ( list($name,$data) = each ($this->popup_windows) )
      {
        //open the window
        $features = $data['features'];
        if ($features==UNDEFINED)
          $js .= "  $name = window.open('', '$name');\n";
        else
          $js .= "  $name = window.open('', '$name', '$features');\n";

        //write the content
        $html_content = $data['html_content'];
        if ($html_content!=UNDEFINED)
          {
            $html_content = WebApp::js_encode($html_content);
            $js .= "  $name.document.write('$html_content');\n";
            $js .= "  $name.document.close();\n";
          }

        //set the timeout of the window
        $timeout = $data['timeout'];
        if ($timeout!=UNDEFINED)
          {
            $js .= "  $name.setTimeout('window.close()', $timeout);\n";
          }

        //load the url in the window, if it is not empty
        $url = $data['url'];
        if ($url!='')
          {
            $js .= "  $name.location.href=\"$url\";\n";
          }
      }
    $js .= "</script>\n";
    return $js;
  }

  function print_dbg_messages()
  {
    $nr = sizeof($this->dbg_messages);
    if ($nr==0) return;

    print "\n<hr/>\n";
    print "<strong>Debug Messages:</strong><br/>\n";
    for ($i=0; $i < $nr; $i++)
      {
        $dbg_msg = $this->dbg_messages[$i]["dbg_msg"];
        $comment = $this->dbg_messages[$i]["comment"];
        if ($comment<>"")  print $comment." : ";
        print $dbg_msg."<br/>\n";
      }
    print "\n<hr/>\n";
  }

  /*------------------ debug functions --------------------------*/

  /** (debug) Outputs the data of each template in $this->tpl_collection. */
  function template_list()
  {
    $html = "<a name='top' id='top' />\n";
    $html .= "<div class='webapp'>\n";
    $html .= "<strong>List of Templates of the Page:</strong>\n";
    $html .= "<ul>\n";
    reset($this->tpl_collection);
    while ( list($tpl_id, $tpl) = each($this->tpl_collection) )
      {
        //don't output the appended templates
        if (ereg('web_app/append/append.html$', $tpl->id))  break;

        $html .= "\t<li><a href='#$tpl->id'>$tpl->id</a></li>\n";
      }
    $html .= "</ul>\n";
    $html .= "</div>\n";

    //print each template
    reset($this->tpl_collection);
    while ( list($tplID, $tpl) = each($this->tpl_collection) )
      {
        //don't output the appended templates
        if (ereg('web_app/append/append.html$', $tpl->id))  break;

        $html .= $tpl->toHtmlTable();
      }
    $html .= "<hr />\n";

    return $html;
  }
        
  /** (debug) Returns the structure of the loaded templates as a tree. */
  function tpl_to_tree()  //for debugging parse()
  {
    $tree = "<hr />\n";
    $tree .= "<a name='top' id='top'/>\n";
    $tree .= "<pre class='webapp'>\n";
    $tree .= "<strong>The tree structure of the templates:</strong>\n\n";
    $tree .= $this->rootTpl->to_tree("");
    $tree .= "</pre>\n";
    return $tree;
  }

  /**
   * Returns a string in html format which displays
   * all the recordsets in $this->rs_collection
   * (for debugging Recordsets).
   */
  function recordsets_to_html()
  {
    $html_recs = "<hr />\n";
    $html_recs .= "<b> THE RECORDSETS OF THE PAGE: </b><br />\n";
    reset($this->rs_collection);
    while ( list($rsId,$rs) = each($this->rs_collection) )
      {
        $html_recs .= $rs->toHtmlTable();
      }
    $html_recs .= "<hr />\n";
    return $html_recs;
  }
}
?>