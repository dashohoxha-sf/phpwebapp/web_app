<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * A stack of associative arrays, keeping variables in nesting scopes.
   * @package parser 
   */
class VarStack
{
  /** array of associative arrays (stack of scopes) */
  var $vars;

  function VarStack()
  {
    $this->vars = array();
    $this->pushScope();
  }        

  function pushScope()
  {
    array_push($this->vars, array());
  }

  function popScope()
  {
    array_pop($this->vars);
  }

  /** Returns a number, which is the index of the current scope. */
  function getCurrentScope()
  {
    $len = count($this->vars);
    $curr_scope = $len - 1;
    return $curr_scope;
  }

  /**
   * Add a variable to the specified scope. $scope_nr is optional,
   * if no scope is given, add it to the current scope.
   */
  function addVar($var_name, $var_value, $scope_nr=UNDEFINED)
  {
    if ($scope_nr==UNDEFINED)  //no scope is given
      {
        $scope_nr = $this->getCurrentScope();
        $current_scope = &$this->vars[$scope_nr];
        $current_scope[$var_name] = $var_value;
      }
    else
      {
        $len = count($this->vars);
        if ($scope_nr >= $len)  return;
        $scope = &$this->vars[$scope_nr];
        $scope[$var_name] = $var_value;
      }
  }
        
  /** Add a variable to the first scope. */
  function addGlobalVar($var_name, $var_value)
  {
    $first_scope = &$this->vars[0];
    $first_scope[$var_name] = $var_value;
  }

  /** Add a list of vars to the current scope. */
  function addVars($arrVars)
  {
    $len = count($this->vars);
    $current_scope = &$this->vars[$len-1];
    $current_scope = array_merge($current_scope, $arrVars);
  }
        
  /** Add a list of vars to the first scope. */
  function addGlobalVars($arrVars)
  {
    $first_scope = &$this->vars[0];
    $first_scope = array_merge($first_scope, $arrVars);
  }

  /**
   * Returns the value of a variable. If it is not found
   * in the current scope, the scope above it is searched,
   * and so on. If it is not found at all, UNDEFINED is returned.
   */
  function getVar($var_name)
  {
    $found = false;
    $len = count($this->vars);
    for ($i = $len-1; $i>=0; $i--)
      {
        $scope = &$this->vars[$i];
        if (isset($scope[$var_name]))
          {
            $found = true;
            $var_value = &$scope[$var_name];
            break;
          }
      }
    if ($found)       return $var_value;
    else              return UNDEFINED;
  }

  /////////////////////////////////////////////////////////////////

  /** For debug; returns the var stack as an HTML table. */
  function toHtmlTable()
  {
    $html = "<strong>VarStack</strong>\n";
    $html .= "<table bgcolor='#aaaaaa' border='0' cellspacing='1' cellpadding='2'>";
    $len = count($this->vars);
    for ($i = $len-1; $i>=0; $i--)
      {
        $html .= "
            <tr>
              <td bgcolor='#eeeeff' colspan='2' align='center'>Scope $i</td>
            </tr>";
        $scope = &$this->vars[$i];
        while (list($var_name,$var_value) = each($scope))
          {
            $html .= "
                <tr>
                  <td bgcolor='#eeeeee' align='right'>$var_name</td>
                  <td bgcolor='#f9f9ff'>$var_value</td>      
                </tr>
                ";
          }
        reset($scope);
      }
    $html .= "</table>\n";

    return $html;
  }


  /** For test and debug */
  function test()
  {
    $vars = new VarStack;

    //$vars->pushScope();
    $vars->addVars(array("a"=>"XXX", "b"=>"BBB", "c"=>""));
    $vars->pushScope();
    $vars->addVar("a", "XXX");
    $vars->pushScope();
    $vars->addVars(array("a"=>"AAA", "b"=>"YYY"));
    $vars->pushScope();
    $vars->addGlobalVar("g1", "GGG");

    /*
     print " var a='".$vars->getVar("a")."' \n";
     print " var c='".$vars->getVar("c")."' \n";
     print " var x='".$vars->getVar("x")."' \n";
     $vars->printOut();
     return;
    */

    $vars->popScope();
    $vars->popScope();

    /*
     print " var a='".$vars->getVar("a")."' \n";
     print " var c='".$vars->getVar("c")."' \n";
     print " var x='".$vars->getVar("x")."' \n";
     $vars->printOut();
     return;
    */

    $vars->pushScope();
    $vars->addVars(array("a"=>"AAA", "b"=>"BBB"));
    $vars->pushScope();
    $vars->addGlobalVar("g2", "GGG2");
    $vars->addGlobalVar("g1", "GGG1");
    $vars->addVar("a", "XXX");
    $vars->pushScope();
    $vars->addVars(array("a"=>"AAA", "b"=>"YYY"));

    print " var g1='".$vars->getVar("g1")."' \n";
    print " var a='".$vars->getVar("a")."' \n";
    print " var c='".$vars->getVar("c")."' \n";
    print " var x='".$vars->getVar("x")."' \n";
    $vars->printOut();
    return;
  }

}
?>