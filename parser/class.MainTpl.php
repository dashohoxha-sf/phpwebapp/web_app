<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * Represents the main (root) template in the tree of templates.
   * @package parser 
   */
class MainTpl extends FileTpl
{
  /** Template of the <head> of the page. */
  var $head;

  /** Template of the <body> of the page. */
  var $body;   
     
  function MainTpl($filename)
  {
    $id = $filename;
    //call the constructor of the superclass
    $this->Template($id, "MainTpl");  
                
    $this->filename = $filename;
    $this->head = new Template("HeadTpl", "HeadTpl");
    $this->body = new Template("BobyTpl", "BodyTpl");         
  }   

  /** Debug function. Returns the structure of loaded templates as a tree. */
  function to_tree()
  {
    global $webPage;
                
    $indent = "  ";
    $tree .= $indent."+--<a href='#".$this->id."'>".$this->id."</a>"
      . " (".$this->type.")\n";
    $i = 0;

    $arr_tpl_id = $this->get_subtemplates();
    for ($i=0; $i < sizeof($arr_tpl_id); $i++)
      {
        $tpl_id = $arr_tpl_id[$i];
        $tpl = $webPage->getTemplate($tpl_id);
        $tree .= $tpl->to_tree($indent."   ");
      }
    return $tree;
  }
}
?>