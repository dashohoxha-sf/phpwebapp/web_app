<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * Contains some functions that are used frequently in the application.
   * These functions could have been defined in the global scope as well,
   * but instead they are defined inside a class just for the sake of
   * the namespace. They are called like this: WebApp::func_name() which
   * makes clear that they are functions of the web application framework.
   */
class WebApp
{
  /**
   * Returns true only the first time that the application
   * is opened, at the begining of the session.
   */
  function first_time()
  {
    global $request;
    return $request->firstTime;
  }

  /**
   * A free event has target 'none', doesn't have a target webobject,
   * so no webobject can handle it.  Instead, it is handled by the
   * file 'on.eventName.php', which is placed in the same folder as the
   * source page of the event.
   */
  function callFreeEvent()
  {
    global $event;

    //look for the event handler in the path of sourcePage
    $path = TPL.dirname($event->sourcePage);
    $file_name = "on.".$event->name.".php";
    $fname = $path."/".$file_name;
    if (file_exists($fname))
      {
        include_once $fname;
      }
    else
      {
        $msg = TF_("File 'v_fname' not found");
        $msg = str_replace('v_fname', $fname, $msg);
        print WebApp::error_msg("WebApp::callFreeEvent(): $msg.");
      }
  }
        
  /*----------- begin construct page functions --------------*/

  /** 
   * Constructs an HTML page which is sent directly to the browser.
   * If $collect is true, however, instead of sending the page to
   * the browser, it returns it as a big string. 
   */
  function constructHtmlPage($tpl_file, $collect =false)
  {
    global $webPage;

    //measure the time of page generation
    $comment = "Time that spends the application for constructing the page";
    $webPage->timer->Start("WebApp", $comment);

    //include 'before_page.php' if it exists           
    $fname = APP_PATH."before_page.php";
    if (file_exists($fname))  include_once $fname;

    //construct the structure of the web page 
    //by parsing the template file
    global $parser;
    $parser->parse_main($tpl_file);

    //append to <head> the JS code that initiates the JS timer
    $webPage->append_to_head($webPage->timer->JSTimer());

    //add also the stylesheet of the framework
    $webapp_styles = '  <link type="text/css" rel="stylesheet" '
      . 'href="'.WEBAPP_URL.'styles.css" />'."\n";
    $webPage->append_to_head($webapp_styles);

    //parse and add the append.html after the MainTpl
    $tpl = $parser->parse_file(APPEND_PATH."append.html");
    $webPage->link_to_body($tpl);

    //render the html page
    global $render;
    $render->collect = $collect;
    $render->render_MainTpl($webPage->rootTpl);

    //include 'after_page.php' if it exists            
    $fname = APP_PATH."after_page.php";
    if (file_exists($fname))  include_once $fname;

    if ($collect)  return $render->html_page;
  }

  /**
   * This function stops constructing the current page
   * and starts loading and constructing the given target page.
   * Similar to the JS function GoTo(), it can also send an event
   * to the new page that is going to be constructed (actually
   * modifies the current event).
   */
  function GoTo($target_page, $target_object =UNDEFINED,
                $event_name =UNDEFINED, $event_args =UNDEFINED)
  {
    global $event, $webPage, $tplVars, $parser, $render;

    //modify $event according to the given params
    if ($target_page<>'thisPage' and $target_page<>'')
      {
        $event->sourcePage = $event->targetPage;
        $event->targetPage = $target_page;
      }
    if ($target_object<>UNDEFINED)  $event->target = $target_object;
    if ($event_name<>UNDEFINED)  $event->name = $event_name;
    if ($event_args<>UNDEFINED)  $event->args = $event_args;

    //create new global vars
    $webPage = new WebPage;
    $tplVars = new VarStack;
    $parser  = new Parser;
    $render  = new Render;

    WebApp::constructHtmlPage(TPL.$event->targetPage);

    //after the page is constructed, stop php execution immediately
    //in order to avoid any side effects of the previous call of
    //constructHtmlPage() etc. (because this function is called
    //somewhere in the middle of some functions, and we don't want
    //the rest of the code of those functions to be executed
    exit(0);
  }

  /**
   * Constructs an HTML page and returns it as a string.
   * Then resumes constructing the previous page.
   * It is useful when you want to construct a page
   * for sending it by e-mail.
   * @see constructHtmlPage()
   */
  function getHtmlPage($tpl_file)
  {
    global $webPage, $tplVars, $parser, $render;

    //save the current webPage and whatever 
    //is already constructed in it
    $old_webPage = clone($webPage);
    $old_tplVars = clone($tplVars);
    $old_parser  = clone($parser);
    $old_render  = clone($render);

    //create a new web page in the global variable $webPage
    $webPage = new WebPage;
    $tplVars = new VarStack;

    //construct it into the $html_page variable
    $collect = true;
    $html_page = WebApp::constructHtmlPage($tpl_file, $collect);  

    //return to the previous web page 
    //and continue with its construction
    $webPage = clone($old_webPage);
    $tplVars = clone($old_tplVars);
    $parser  = clone($old_parser);
    $render  = clone($old_render);

    //return the constructed page
    return $html_page;
  }

  /**
   * Process the given template file and return it as
   * a string where the variables have been replaced, etc.
   * @see getHtmlPage()
   */
  function fill_template($tpl_file, $vars =array())
  {
    global $webPage, $tplVars, $parser, $render;

    //save the current webPage and whatever 
    //is already constructed in it
    $old_webPage = clone($webPage);
    $old_tplVars = clone($tplVars);
    $old_parser  = clone($parser);
    $old_render  = clone($render);

    //create a new web page in the global variable $webPage
    $webPage = new WebPage;
    $webPage->tpl_file = $tpl_file;
    $tplVars = new VarStack;
    $tplVars->addVars($vars);
    $parser = new Parser;
    $parser->xml_prolog = '<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="EN">
';
    $render = new Render;
    $render->collect = true;

    //construct it into the $html_page variable
    $tpl = $parser->parse_file($tpl_file);
    $render->render_tpl($tpl);
    $html_page = $render->html_page;

    //return to the previous web page 
    //and continue with its construction
    $webPage = clone($old_webPage);
    $tplVars = clone($old_tplVars);
    $parser  = clone($old_parser);
    $render  = clone($old_render);

    //return the constructed page
    return $html_page;
  }

  /*------ end construct functions ----------------*/
        
  /*------ begin variable functions ---------------*/

  /**
   * Add a template variable in the current scope 
   * (for the current webbox or template).
   * @see addVars(), getVar(), addGlobalVar(), addGlobalVars()
   */
  function addVar($var_name, $var_value)
  {
    global $tplVars;
    $tplVars->addVar($var_name, $var_value);
  }
        
  /**
   * Add a template variable in the global scope;
   * it will be available in all the application templates.
   * @see  addGlobalVars(), getVar(), addVar(), addVars()
   */
  function addGlobalVar($var_name, $var_value)
  {
    global $tplVars;
    $tplVars->addGlobalVar($var_name, $var_value);
  }
        
  /**
   * Add a list (associative array) of template variables 
   * at once. 
   * @see addVar(), getVar(), addGlobalVar(), addGlobalVars()
   */
  function addVars($arrVars)
  {
    global $tplVars;
    $tplVars->addVars($arrVars);
  }
        
  /**
   * Add a list of global template variables at once.
   * @see  addGlobalVar(), getVar(), addVar(), addVars()
   */
  function addGlobalVars($arrVars)
  {
    global $tplVars;
    $tplVars->addGlobalVars($arrVars);
  }
        
  /**
   * Returns the value of a template variable,
   * or UNDEFINED if such a variable does not exist. 
   * @see addVar(), addVars(), addGlobalVar(), addGlobalVars()
   */
  function getVar($var_name)
  {
    global $tplVars;
    return $tplVars->getVar($var_name);
  }

  /**
   * Add a session variable. 
   * @see getSVar(), setSVar()
   */
  function addSVar($var_name, $var_value, $db =false)
  {
    global $session;
    $session->addVar($var_name, $var_value, $db);
  }

  /**
   * Set a new value to a session variable.
   * @see addSVar(), getSVar()
   */
  function setSVar($var_name, $var_value)
  {
    //set it in the session
    global $session;
    $session->setVar($var_name, $var_value);

    //if it is the state var of a webobject
    //set it in the webobject's scope as well
    if (ereg('(.*)->(.*)', $var_name, $regs))  
      {
        $obj_id = $regs[1];
        $v_name  = $regs[2];
        $webobj = WebApp::getObject($obj_id);
        if ($webobj==UNDEFINED)   return;

        global $tplVars;
        $tplVars->addVar($v_name, $var_value, $webobj->scope);
      }
  }
    
  /**
   * Get the value of a session variable.
   * @see addSVar(), setSVar()
   */
  function getSVar($var_name)
  {
    global $session;
    $var_value = $session->getVar($var_name);
    return $var_value;
  }

  /**
   * Get all the state variables of a webobject
   * and return them as an associative array.
   * The optional parameter $type accepts the values "DB"
   * and "JS", and depending on this parameter the function
   * returns DB vars, JS vars or all of them (if undefined).
   * @see getSVar(), setSVar()
   */
  function getSVars($obj_id, $type =UNDEFINED)
  {
    global $session;
    $arr_vars = $session->getObjVars($obj_id, $type);
    return $arr_vars;
  }

  /**
   * Returns the web object with the given id,
   * or UNDEFINED.
   */
  function getObject($obj_id)
  {
    global $webPage;
    $obj = $webPage->getObject($obj_id);
    return $obj;
  }

  /*------ end variable functions ----------------*/

  /** Returns the value of the given expression. */
  function evaluate($expr)
  {
    //declare as global any variables that are used in $expr
    preg_match_all('/\$[[:alnum:]_]+/', $expr, $matches);
    $var_names = implode(',', $matches[0]);
    if ($var_names <> '')
      {
        eval("global $var_names;");
      }

    @eval("\$webApp_value = $expr;");
    $errormsg = $php_errormsg;
    if ($errormsg<>'')
      {
        $msg = T_("Evaluating expression: '\$value = v_expr;' v_errormsg");
        $msg = str_replace('v_expr', $expr, $msg);
        $msg = str_replace('v_errormsg', $errormsg, $msg);
        print WebApp::error_msg($msg);
      }

    return $webApp_value;
  }
        
  /**
   * Returns the value of a variable by looking first at $tplVars,
   * and then at $session vars, then at PHP constants, and finally
   * at PHP global variables. If this variable is not found, 
   * returns the constant VAR_NOT_FOUND.
   */
  function getVarValue($var_name)
  {
    //search first in the template variables ($tplVars)
    $var_value = WebApp::getVar($var_name);
    if (!($var_value===UNDEFINED))    //found
      {
        return $var_value;    
      }

    //if not found look at session (state) variables
    $var_value = WebApp::getSVar($var_name);
    if (!($var_value===UNDEFINED))
      {
        return $var_value;
      }

    //if not found look at PHP constants
    if ( defined($var_name) )
      {
        eval("\$tmp = $var_name;");
        $var_value = $tmp;
        return $var_value;
      }

    //if not found look at PHP global variables
    if ( isset($GLOBALS[$var_name]) )
      {
        $var_value = $GLOBALS[$var_name];
        return $var_value;
      }

    //if still not found return the constant VAR_NOT_FOUND
    $var_value = str_replace("var_name", $var_name, VAR_NOT_FOUND);
    return $var_value;
  }
        
        
  /**
   * Search in the given string for variable occourences
   * like this: {{var_name}} and replace them by their values.
   *
   * If single (double) quotes surround the var_name, like
   * this: {{'var_name'}} ({{"var_name"}}), then all occourences 
   * of the single (double) quote inside the value of the variable 
   * are escaped, like this: \' (\"). This is done because sometimes
   * the variable is inside a string, and if it contains quotes inside
   * the value, then it may break the string.
   *
   * A variable can be commented by adding a diesis before or after its
   * name.  In this case, the variable will not be expanded.
   *
   * Variables can be nested with each-other, like this: 
   * {{item_{{id}}_checked}}.  The most inner variables are expanded first
   * (in this case {{id}}).
   *
   * Variables are expanded recursively, so, if the value of a variable
   * contains another variable, it will be expanded as well. 
   */
  function replaceVars($str)
  {
    while ( ereg("\{\{([^#{][^{}]*[^#}])\}\}", $str, $regs) )
      {
        $str2replace = $regs[0];
        $var_name = $regs[1];
        $var_value = $regs[1];
        list($var_name, $quote) = WebApp::get_quote($var_name);
        $var_value = WebApp::getVarValue($var_name);
        $var_value = WebApp::escape_quotes($var_value, $quote);
        $str = str_replace($str2replace, $var_value, $str);
      }
    $str = str_replace('{{#', '{{', $str);
    $str = str_replace('#}}', '}}', $str);
    return $str;
  }
  /**
   * Get the quote which surrounds the $var_name:
   * single, double, or none.
   * @see replaceVars()
   */
  function get_quote($var_name)
  {
    if ( ereg("^'(.*)'$", $var_name, $regs) )
      {       //surrounded by single quotes
        $var_name = $regs[1];
        $quote = "'";
      }
    elseif ( ereg('^"(.*)"$', $var_name, $regs) )
      {       //surrounded by double quotes
        $var_name = $regs[1];
        $quote = '"';
      }
    else
      {       //not surrounded by quotes
        $quote = '';
      }
    return array($var_name, $quote);
  }
  /**
   * Escape all the quotes in the $var_value.
   * @see replaceVars(), get_quote()
   */
  function escape_quotes($var_value, $quote)
  {
    switch ($quote)
      {
      case '"':               //double quote
        $var_value = str_replace("'", "\\'", $var_value);
        break;
      case "'":               //single quote
        //$var_value = str_replace('"', '\"', $var_value);
        $var_value = str_replace('"', '&#34;', $var_value);
        break;
      case '':                //no quotes
        //do nothing
        break;
      default:
        $msg = TF_("the quote 'v_quote' is unknown");
        $msg = str_replace('v_quote', $quote, $msg);
        print WebApp::warning_msg("WebApp::escape_quotes(): $msg.<br />\n");
        break;
      }
    return $var_value;
  }
  /*------ end replaceVars() --------------------*/
        
  /*------ begin DB functions -------------------*/

  /**
   * Execute the given query (which is of type select).
   * @return Recordset 
   * @see sqlExec
   */
  function sqlQuery($query, $conn =UNDEFINED)
  {
    $rs = new EditableRS(UNDEFINED, $query, $conn);
    $rs->Open();
    return $rs;
  }

  /**
   * Execute the given query (which is of type
   * insert, update, etc.) and return the number of
   * the affected rows.
   * @see sqlQuery
   */
  function sqlExec($query, $conn =UNDEFINED)
  {
    if ($conn==UNDEFINED)
      {
	global $cnn;
	$conn = $cnn;
      }
    $result = $conn->exec($query);
    return $result;
  }

  /**
   * Open the given table and return the records that
   * satisfy the given $condition. 
   * @return TableRS
   */
  function openTable($tbl_name, $condition ="(1=1)", $conn =UNDEFINED)
  {
    $query = "SELECT * FROM $tbl_name WHERE $condition";
    $rs = new TableRS($tbl_name, $query, $conn);
    $rs->Open();
    return $rs;
  }
        
  /**
   * Execute the DB command with the given id.
   * $params is an array of {{vars}} that are used in the query.
   * Returns TRUE or FALSE indicating success or failure.
   * @see execQuery(), openRS()
   */
  function execDBCmd($cmd_id, $params =UNDEFINED, $conn =UNDEFINED)
  {
    if ($params==UNDEFINED)  $params = array();
    global $webPage, $tplVars;

    $tplVars->pushScope();
    $tplVars->addVars($params);

    $rs = $webPage->getRecordset($cmd_id);
    if ($rs==UNDEFINED)
      {
        $msg = TF_("dbCommand 'v_cmd_id' is undefined");
        $msg = str_replace('v_cmd_id', $cmd_id, $msg);
        print WebApp::error_msg("WebApp::execDBCmd(): $msg.");
        return false;
      }

    $query = WebApp::replaceVars($rs->query);
    $connection = ($conn==UNDEFINED ? $rs->cnn : $conn);
    if (!is_object($connection))
      {
        $errmsg = TF_("Cannot open query because there is no valid connection.");
	Connection::show_error($errmsg, $query);
        return;
      }
    $result = $connection->exec($query, $rs->ID);

    $tplVars->popScope();

    return $result;
  }
        
  /**
   * Open and return the recordset with the given id.
   * $params is an array of {{vars}} that are used in the query.
   * @return Recordset
   * @see execDBCmd()
   */
  function openRS($rs_id, $params =UNDEFINED, $conn =UNDEFINED)
  {
    if ($params==UNDEFINED)  $params = array();
    global $webPage, $tplVars;

    if (isset($webPage->rs_collection[$rs_id]))
      {
        $tplVars->pushScope();
        $tplVars->addVars($params);
        $rs = &$webPage->rs_collection[$rs_id];
        $rs->Open($conn);
        $tplVars->popScope();
        return $rs;
      }
    else
      {
        $msg = TF_("recordset 'v_rs_id' is undefined");
        $msg = str_replace('v_rs_id', $rs_id, $msg);
        print WebApp::error_msg("WebApp::openRS(): $msg.");
        return UNDEFINED;
      }
  }

  /**
   * From the items of the given array create a listbox recordset
   * (with the fields 'id' and 'label'), and insert it in the $webPage.
   * $arr_options can be an associated array, a simple array, or mixed.
   * In case of the associated array, the keys are used as id-s and 
   * the values are used as labels.
   */
  function add_listbox_rs($rs_id, $arr_options)
  {
    $rs = new EditableRS($rs_id);
    $arr_keys = array_keys($arr_options);
    for ($i=0; $i < sizeof($arr_keys); $i++)
      {
        $key = $arr_keys[$i];
        $value = $arr_options[$key];
        if (is_integer($key))  $key = $value;
        $rs->addRec(array('id' => $key, 'label' => $value));
      }
    global $webPage;
    $webPage->addRecordset($rs);
  }

  /*------ end DB functions ---------------------*/
        
  /*------ mix functions ------------------------*/

  /** Convert the given path to url. */
  function to_url($path)
  {
    if ($path[0]!='/' and $path[1]!=':')
      {
        //$path is a relative path
        $url = APP_URL.$path;
      }
    else
      {
        //$path is an absolute path
        $app_url = APP_URL;
        ereg('^(/~[^/]+)/', $app_url, $regs);
        $user_dir = $regs[1];
        $app_url = ereg_replace("^$user_dir", '', $app_url);
        $DocumentRoot = ereg_replace($app_url.'$', '', APP_PATH);
        $url = str_replace($DocumentRoot, '', $path);
        $url = $user_dir.$url;
      }

    return $url;
  }

  /**
   * When a string is inserted inside a JS code,
   * enclosed in single quotes, like this: 'str',
   * some characters inside the string must be replaced
   * by some others, in order not to break the JS code.
   * These replacements are: '\' to '\\', '\n\' is added
   * before a new line, and single quotes are escaped
   * by a slash.
   */
  function js_encode($str)
  {
    $str = str_replace("\\", "\\\\", $str);
    $str = str_replace("\r", '', $str);
    $str = str_replace("\n", "\\n\\\n", $str);
    $str = str_replace("'", "\\'", $str);
    return $str;
  }

  /**
   * Decodes a string that comes to PHP from JS code.
   * Used in session to convert variable values.
   */
  function js_decode($str)
  {
    $str = str_replace("\\\\", "\\", $str);
    $str = str_replace('\"', '"', $str);
    $str = str_replace("\'", "'", $str);
    $str = str_replace('\n', "\n", $str);
    return $str;
  }

  /**
   * Construct and return a link that can be used to access
   * from outside a certain page of the application, in a certain
   * state.
   */
  function get_external_link()
  {
    global $request;

    $extern_link = APP_URL."index.php";
    $extern_link .= '?sourcePage='.urlencode($request->sourcePage);
    $extern_link .= '&targetPage='.urlencode($request->targetPage);
    $extern_link .= '&sessionVars='.urlencode($request->sessionVars);
    $extern_link .= '&strEvent='.urlencode($request->strEvent);
    $extern_link .= '&phpVars='.urlencode($request->phpVars);

    return $extern_link;
  }

  /** Display a pop-up message box to the user. */
  function message($msg)
  {
    global $webPage;
    $webPage->messages[] = $msg;
  }

  /**
   * Open a pop-up window (in a new browser).
   * Parameters $name, $url, and $features will be used
   * in a javascript statement like this:
   *   name = window.open(url, name, features);
   * $html_content is an html code that is displayed in the
   * window, like this:
   *   name.document.write(content);
   * $timeout is a timeout for closing the window (in milisecs)
   */
  function popup_window($name, $url, $html_content =UNDEFINED,
                        $features =UNDEFINED, $timeout =UNDEFINED)
  {
    global $webPage;
    $webPage->popup_windows[$name]['url'] = $url;
    $webPage->popup_windows[$name]['html_content'] = $html_content;
    $webPage->popup_windows[$name]['features'] = $features;
    $webPage->popup_windows[$name]['timeout'] = $timeout;
  }

  /** Output a debug message. */
  function debug_msg($dbg_msg, $comment ="")
  {
    global $webPage;
    $webPage->addDbgMsg($dbg_msg, $comment);
  }
        
  /** Format and return an error message. */
  function error_msg($err_msg)
  {
    if (!SHOW_ERROR_MESSAGES)  return "";

    $err_msg = str_replace(UP_PATH, '', $err_msg);
    $err_msg = htmlentities($err_msg);
    return "\n<span class='webapp_error'><strong>WebApp Error:</strong> "
      . $err_msg . "</span>\n";
  }

  /** Format and return a warning message. */
  function warning_msg($warn_msg)
  {
    if (!SHOW_ERROR_MESSAGES)  return "";

    $err_msg = str_replace(UP_PATH, '', $err_msg);
    $warn_msg = htmlentities($warn_msg);
    return "\n<span class='webapp_warning'><strong>WebApp Warning:</strong> "
      . $warn_msg . "</span>\n";
  }

  /** Format and return an xml error message. */
  function xml_err_msg($fname, $line_nr, $col_nr, $error_string, $xml_string)
  {
    if (!SHOW_ERROR_MESSAGES)  return "";

    $fname = str_replace(UP_PATH, '', $fname);
    $xml_string = htmlentities($xml_string);
    $xml_string = str_replace('^err^', "<strong>X</strong>",$xml_string);

    $err_msg = "
<div class='xml_error'>
  <strong>XML Error:</strong> $error_string.  At <strong>$fname</strong>, 
  line <strong>$line_nr</strong>, column <strong>$col_nr</strong>:  <br />
  <span class='xml_err_str'>$xml_string</span>.
</div>
";

    return $err_msg;
  }
}
?>