// -*-C-*-
/*
  This file  is part of phpWebApp,  which is a  framework for building
  web application based on relational databases.

  Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  phpWebApp is free software; you can redistribute it and/or modify it
  under the  terms of the GNU  General Public License  as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  phpWebApp is  distributed in  the hope that  it will be  useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along with phpWebApp; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


/**
 * L10n class keeps a list of translatable messages and their
 * translation. It helps to localize messages in the JS code.
 */
function L10n()
{
  this.messages = new Array();

  this.addMsg = l10n_addMsg;
  this.gettext = l10n_gettext;
}

/** Add a new message and its translation in the list. */
function l10n_addMsg(msgid, msgstr)
{
  var msg = { id:msgid, str:msgstr };
  this.messages.push(msg);
}

/** Return the translation of a message. */
function l10n_gettext(msgid)
{
  var i;

  for (i=0; i < this.messages.length; i++)
    if (this.messages[i].id == msgid)
      {
        //found, return the translation
        return this.messages[i].str;
      }

  //not found, return the msgid itself
  return msgid;
}

function T_(msgid)
{
  return l10n.gettext(msgid);
}

function TF_(msgid)
{
  return l10n.gettext(msgid);
}

