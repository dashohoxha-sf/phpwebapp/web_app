<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * Includes the JS L10n messages in the HTML page.
   * @package l10n
   */
class wbJSL10n extends WebObject
{
  function onParse()
  {
    global $webPage;

    $l10n_url = WebApp::to_url(L10N_PATH);
    $include_l10n_js = 
      '  <script type="text/javascript" language="javascript" '
      . "src=\"${l10n_url}class.L10n.js\"></script>\n";
    $webPage->append_to_head($include_l10n_js);
  }

  function onRender()
  {
    global $webPage;  
    WebApp::addVar("JS_L10N_MESSAGES", $webPage->js_l10n_messages_to_js());
  }
}
?>