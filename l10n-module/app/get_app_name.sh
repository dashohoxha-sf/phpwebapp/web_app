#!/bin/bash
### get and output the name of the application

cd $(dirname $0)

l10n_dir=$(pwd)
app_dir=$(dirname $l10n_dir)
app_name=$(basename $app_dir)

echo $app_name
