#!/bin/bash

### go to this dir
cd $(dirname $0)

### get the translatable strings of search, admin and docbook
### from HTML and PHP files
module_list="search admin docbook"
for module in $module_list
do
  dir=../templates/$module
  touch $dir/l10n/$module.po
  find $dir -name '*.php' -o -name '*.js' -o -name '*.html' \
    | xargs xgettext -C --keyword=T_ --join-existing \
                     --output-dir=$dir/l10n/ --output=$module.po
done

### get the translatable strings for the rest of the application
app_name=$(./get_app_name.sh)
touch $app_name.po
find ../templates/ -path '../templates/search'  -prune \
                -o -path '../templates/admin'   -prune \
                -o -path '../templates/docbook' -prune \
                -o -name '*.php'  -print  \
                -o -name '*.js'  -print  \
                -o -name '*.html' -print  \
  | xargs xgettext -C --keyword=T_ --join-existing --output=$app_name.po

