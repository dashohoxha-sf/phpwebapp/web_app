<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

if (USE_PHP_GETTEXT)
  {
    include_once dirname(__FILE__)."/php-gettext/streams.php";
    include_once dirname(__FILE__)."/php-gettext/gettext.php";
  }

/**
 * This class is used by the framework for translating messages and texts.
 * It can use either the PHP-gettext or the GNU gettext packages, since
 * both of them have some advantages or disadvantages.
 *
 * The framework allows to have separate translation (localization) files for
 * each template that is included and for each webobject. If the
 * directory that contains the template (or webbox) contains also
 * a subdirectory named 'l10n', then the framework will look inside
 * this directory for the file that contains the translations of the
 * messages of the template/webbox. It will look for the file
 * l10n/lng/LC_MESSAGES/tplname.mo (or l10n/lng/LC_MESSAGES/webboxid.mo)
 * which contains the translation messages of the template (or webbox).
 *
 * If the subdirectory 'l10n/' does not exist, the framework will 
 * look for the file tplname.mo (or webboxid.mo) in the first 'l10n/'
 * in the parent directory or in the ancestors. If even the application
 * directory does not contain a 'l10n/' subdir, then the last place 
 * to look for will be the system default (usually /usr/share/locale/).
 *
 * About the language code, if it is e.g. 'en_US', then it will search 
 * first the folder 'en_US/' and then the folder 'en/'.
 *
 * If the file tplname.mo (or webboxid.mo) is not found at all in 
 * the searched directories, then the last translation file (*.mo)
 * that was found will be used for translating messages. This is usually
 * the translation file (*.mo) of the containing template or webbox
 * (from which this template/webbox is included directly or indirectly).
 *
 * @package l10n
 */
class L10n
{
  /** path of the localization directory ('l10n/') */
  var $path;

  /** textdomain (basename of the .mo translation file) */
  var $domain;

  /** the code of the target language (e.g. en_US or sq_AL) */
  var $lng;

  /** the codeset of the target language */
  var $codeset;

  /** keeps the states of translation files */
  var $stack = array();

  /** used if the PHP-gettext is used instead of the GNU gettext */
  var $php_gettext;

  /**
   * Constructor.
   *
   * @path    is the localization (l10n) directory
   * @domain  is the name of the translation file (domain.mo)
   * @lng     is the language code, like 'en_US' or 'en', etc.
   * @codeset is the codeset of the language, like 'iso-latin-1', etc.
   */
  function L10n($path =UNDEFINED, $domain =UNDEFINED,
                $lng =LNG, $codeset =CODESET)
  {
    if ($path==UNDEFINED)
      {
        if (file_exists(APP_PATH.'l10n'))
          $path = APP_PATH.'l10n';
        else
          $path = '/usr/share/locale';
      }

    if ($domain==UNDEFINED)
      {
        //set as domain the name of the application
        ereg('/([^/]+)/$', APP_PATH, $regs);
        $domain = $regs[1];
      }

    $this->path = $path;
    $this->domain = $domain;
    $this->lng = $lng;
    $this->codeset = $codeset;
    $this->load_translation_file();
  }

  /**
   * Search the text for strings like T_("...") or TF_("...")
   * and replace them by the translation of the
   * string inside the double quotes.  It is usually
   * called on rendering (generating html pages).
   */
  function translate($text)
  {
    while ( ereg('(TF?_)\\("([^"]*)"\\)', $text, $regs) )
      {
        $str2translate = $regs[0];
        $f_name   = $regs[1];
        $original = $regs[2];

        //replace '\n' by newline 
        $original = ereg_replace("\\\\n(\\\\ *\n)", "\n\\1", $original);

        $translation = $f_name($original);
        $text = str_replace($str2translate, $translation, $text);
      }

    return $text;
  }

  /**
   * Change the language and (optionally) the codeset of the language.
   * It can be called from the application to change the language dynamically.
   */
  function set_lng($lng, $codeset =UNDEFINED)
  {
    if ($lng==$this->lng and $codeset==$this->codeset)  return;

    $this->lng = $lng;
    if ($codeset!=UNDEFINED) $this->codeset = $codeset;

    $this->load_translation_file();
  }

  /**
   * Set the locale and the textdomain for gettext.
   * Set also the encoding, if it is defined.
   * The translation file will be looked for in:
   *    $path/$lng/LC_MESSAGES/$domain.mo
   */
  function load_translation_file()
  {
    $lng     = $this->lng;
    $codeset = $this->codeset;
    $path    = $this->path;
    $domain  = $this->domain;

    if (USE_PHP_GETTEXT)
      {
        $file = $this->find_file_mo($path, $domain);
        if ($file)
          {
            $input = new FileReader($file);
            $this->php_gettext = new gettext_reader($input);
          }
      }
    else  //use GNU gettext functions
      {
        if ($lng != UNDEFINED)
          setlocale(LC_MESSAGES, $lng);
        if ($codeset != UNDEFINED)
          bind_textdomain_codeset($domain, $codeset);

        bindtextdomain($domain, $path);
        textdomain($domain);
      }
    //WebApp::debug_msg('==== '.$this->printState());
  }

  /**
   * Push the current state of the translation on the stack.
   * This is usually called by the rendering class when it
   * starts to render a new template or webbox, since each
   * template or webbox can have its own locatization (l10n)
   * files, and after it is done, we would like to return
   * to the previous translation state.
   */
  function pushState()
  {
    $item =  array(
                   'path'    => $this->path,
                   'domain'  => $this->domain,
                   'lng'     => $this->lng,
                   'codeset' => $this->codeset
                   );
    array_push($this->stack, $item);
    //WebApp::debug_msg('>>> '.$this->printState());
  }

  function printState()
  {
    return $this->path.' '.$this->domain.' '.$this->lng.' '.$this->codeset;
  }

  /**
   * Pop the current state of translation from the stack.
   * If the previous state is different from the current state,
   * load the previous translation file.
   */
  function popState()
  {
    $item = array_pop($this->stack);

    //if the previous state is different from the current state,
    //load the previous translation file
    if ( $item['path']       != $this->path
         or $item['domain']  != $this->domain
         or $item['lng']     != $this->lng
         or $item['codeset'] != $this->codeset )
      {
        $this->path    = $item['path'];
        $this->domain  = $item['domain'];
        $this->lng     = $item['lng'];
        $this->codeset = $item['codeset'];

        $this->load_translation_file();
      }
    //WebApp::debug_msg('<<< '.$this->printState());
  }

  /**
   * Find and load the appropriate translation file (*.mo).
   * This function is usually called by the framework to load
   * the translation file of a template or webbox. If no
   * translation file is found (searching according to certain
   * rules), then nothing is changed and the existing translation
   * file will be used.
   *
   * @dir is the directory containing the template or webbox
   * @id  is the id of the webbox or the filename of the template
   */
  function set_translation_file($dir, $id)
  {
    $dir = $this->find_l10n($dir);
    $l10n = ($dir ? $dir.'l10n' : '/usr/share/locale');

    $file = $this->find_file_mo($l10n, $id);
    if (!$file)  return;

    //a translation file is found, save the parameters and load it
    if ($l10n != $this->path or $id != $this->domain)
      {
        $this->path = $l10n;
        $this->domain = $id;
        $this->load_translation_file();
      }
  }

  /**
   * Look at the given path for a 'l10n' subdirectory; if not found,
   * look also at the parent and all the ancestor paths, up to the 
   * application path. Return the first path that contains a 'l10n'
   * directory, or false if not found. Called by set_translation_file().
   */
  function find_l10n($path)
  {
    if ($path[0] != '/')  $path = APP_PATH.$path;

    while ($path != '')
      {
        if (file_exists($path.'l10n/'))  return $path;

        //get the parent directory
        $path = ereg_replace('[^/]*/?$', '', $path);
      }

    //no 'l10n' dir was found, return false
    return  false;
  }

  /**
   * Return the path of the translation file (domain.mo)
   * in the given l10n/ directory. If the language code is
   * something like 'en_US', then check also with 'en', if not
   * found in 'en_US'. If not found at all, return false.
   * Called by set_translation_file().
   */
  function find_file_mo($l10n, $domain)
  {
    $lng = $this->lng;
    $file = "$l10n/$lng/LC_MESSAGES/$domain.mo";
    if (file_exists($file))  return $file;

    //if not found for lng=en_US, check also for lng=en
    $lng = ereg_replace('_.*', '', $lng);
    $file = "$l10n/$lng/LC_MESSAGES/$domain.mo";
    if (file_exists($file))  return $file;

    //domain.mo is not found, return false
    return false;
  }
}

/**
 * This is an alias for gettext. It should be used in the PHP code to get
 * the translation of a string, like this: $var = T_("...");
 */
function T_($msgid)
{
  //remove the slash at the end of the lines (used for multiple lines)
  $msgid = ereg_replace("\\\\ *\n", "", $msgid);

  if (USE_PHP_GETTEXT)
    {
      global $l10n;
      if (isset($l10n->php_gettext))
        $msgstr = $l10n->php_gettext->translate($msgid);
      else
        $msgstr = $msgid;
    }
  else //use GNU gettext
    {
      $msgstr = gettext($msgid);
    }

  return $msgstr;
}

/**
 * This is another alias for gettext, but it is used for translating
 * framework messages. For the application components and templates
 * the framework finds out outomatically which translation file to use
 * and where it is located, depending on the location of the component
 * its id, and some conventions. However, the translations of the 
 * framework messages are in a separate translation file. So, TF_("...")
 * is used instead of T_("...") in order to distinguish that this is the
 * translation of a framework message.
 * This function is used in the PHP code of the framework classes to get
 * the translation of a string, like this: $var = TF_("...");
 */
function TF_($msgid)
{
  global $l10n;

  $l10n->pushState();

  $l10n->path = WEBAPP_PATH.'l10n';
  $l10n->domain = 'web_app';
  $l10n->load_translation_file();
  $msgstr = T_($msgid);

  $l10n->popState();

  return $msgstr;
}
?>