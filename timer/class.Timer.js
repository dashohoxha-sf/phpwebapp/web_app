//-*- mode: C; -*-//tells emacs to use mode C for this file
/*
  This file  is part of phpWebApp,  which is a  framework for building
  web application based on relational databases.

  Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  phpWebApp is free software; you can redistribute it and/or modify it
  under the  terms of the GNU  General Public License  as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  phpWebApp is  distributed in  the hope that  it will be  useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along with phpWebApp; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/** The class Proc represents the duration of a process. */
function Proc(id, comment)
{
  this.ID = id;
  this.Start = new Date();
  this.End = new Date();
  this.comment = comment;
}

/** The class Timer keeps a list of processes (their duration). */
function Timer()
{
  this.arrProc = new Array();
  this.nr = 0;  //nr of records

  this.find = find;
  this.Start = Start;
  this.Stop = Stop;
  this.toHtmlTable = toHtmlTable;
}

/**
 * Returns the position of a process in the array
 * or -1 if this process cannot be found.
 */
function find(proc_id)
{
  var idx;
  for (idx=0; idx < this.nr; idx++)
    if (this.arrProc[idx].ID == proc_id)
      return idx;
  //not found
  return -1;
}

function Start(proc_id, comment)
{
  var idx = this.find(proc_id);
  if (idx == -1)    //does not exist
    {
      this.arrProc[this.nr] = new Proc(proc_id, comment);
      this.nr++;
    }
  else
    {
      var msg = TF_("'v_proc_id' already exists.");
      msg = msg.replace(/v_proc_id/, proc_id);
      alert("Timer.Start: " + msg);
    }
}

function Stop(proc_id)
{
  var idx = this.find(proc_id);
  if (idx == -1)    //does not exist
    {
      var msg = TF_("'v_proc_id' does not exist.");
      msg = msg.replace(/v_proc_id/, proc_id);
      alert("Timer.Start: " + msg);
    }
  else
    {
      this.arrProc[idx].End = new Date();
    }
}

function toHtmlTable()
{
  var htmlTable;
  htmlTable = "\
<table bgcolor='#aaaaaa' border='0' cellspacing='1' cellpadding='2'>\
  <tr>\
    <td bgcolor='#eeeeff' colspan='3'>\
      Execution Times of JavaScript Code and Loading Times of HTML Code\
    </td>\
  </tr>\
  <tr>\
    <td bgcolor='#eeeeee' nowrap='nowrap'> Process ID </td>\
    <td bgcolor='#eeeeee' nowrap='nowrap'> Execution Time (msec) </td>\
    <td bgcolor='#eeeeee' nowrap='nowrap'> Comment </td>\
  </tr>";

  for (idx=0; idx < this.nr; idx++)
    {
      var proc = this.arrProc[idx];
      executionTime = proc.End - proc.Start;
      htmlTable += "\
  <tr>\
    <td bgcolor='#f9f9f9'> " + proc.ID + " </td>\
    <td bgcolor='#f9f9ff'> " + executionTime + " </td>\
    <td bgcolor='#f9f9f9'> " + proc.comment + " </td>\
  </tr>";
    }
  htmlTable += "</table>\n";

  return htmlTable;
}
