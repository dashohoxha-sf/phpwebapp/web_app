<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * Used for mesuring the execution time of
   * SQL queries and other processes or procedures.
   * @package misc
   * @subpackage timer
   */
class Timer
{
  /** Table that keeps the time of execution */
  var $info;

  function Timer()
  {
    $this->info = array();
  }

  function Start($processID, $comment)
  {
    if (!EXECUTION_TIME_INFO) return;

    $this->info[$processID]["comment"] = $comment;
    $this->info[$processID]["startTime"] = microtime();
  }

  function Stop($processID)
  {
    if (!EXECUTION_TIME_INFO) return;

    $this->info[$processID]["endTime"] = microtime();
  }

  function GetTime($processID)
  {
    if (!EXECUTION_TIME_INFO) return;

    $startTime = $this->info[$processID]["startTime"];
    list($usec1, $sec1) = explode(" ", $startTime);

    $endTime = $this->info[$processID]["endTime"];
    list($usec2, $sec2) = explode(" ", $endTime);

    $micro_sec = ($sec2 - $sec1)*1000000 + ($usec2 - $usec1);
    return $micro_sec / 1000; //return milisecs
  }

  function toHtmlTable()
  {
    if (!EXECUTION_TIME_INFO) return;

    $html = $this->JSTimer_toHtmlTable();
    $html .= "
<table bgcolor='#aaaaaa' border='0' cellspacing='1' cellpadding='2'>
  <tr>
    <td bgcolor='#eeeeff' colspan='3'>
      <span class='h_space'/> Execution Times of Queries and PHP Functions
    </td>
  </tr>
  <tr>
    <td bgcolor='#eeeeee' nowrap='nowrap'> Process ID </td>
    <td bgcolor='#eeeeee' nowrap='nowrap'> Execution Time (msec) </td>
    <td bgcolor='#eeeeee' nowrap='nowrap'> Comment </td>
  </tr>";

    while ( list($processID, $timeInfo) = each($this->info) )
      {
        $comment = $timeInfo["comment"];
        $executionTime = $this->GetTime($processID);
        $html .= "
  <tr>
    <td bgcolor='#f9f9f9' valign='top'> $processID </td>
    <td bgcolor='#f9f9ff' valign='top'> $executionTime </td>
    <td bgcolor='#f9f9f9' valign='top'><xmp>$comment</xmp></td>
  </tr>";
      }
    $html .= "</table>";

    return $html;
  }

  /**
   * Returns a JS code that is appended at the head of the page.
   */
  function JSTimer()
  {
    if (!EXECUTION_TIME_INFO) return "";

    $timer_path = WebApp::to_url(TIMER_PATH);
    $comment = TF_("Time that spends the web-page to be transfered and loaded in browser");
    $js_code = "
<script type=\"text/javascript\" language=\"javascript\" src=\"".$timer_path."class.Timer.js\"></script>
<script type=\"text/javascript\" language=\"javascript\">
//<![CDATA[
  var timer = new Timer();
  var comment = '$comment'; 
  timer.Start('web-page', comment);
//]]>
</script>
";

    global $webPage;
    $webPage->extract_js_l10n_messages(TIMER_PATH."class.Timer.js");

    return $js_code;
  }

  function JSTimer_toHtmlTable()
  {
    $js_code = "
<script type=\"text/javascript\" language=\"javascript\">
//<![CDATA[
  var timerHtmlTable;
  timer.Stop('web-page');
  timerHtmlTable = timer.toHtmlTable();
  document.write(timerHtmlTable);
//]]>
</script>
";
    return $js_code;
  }
}
?>