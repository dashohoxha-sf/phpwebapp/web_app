<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * @package     webobjects
   * @subpackage  dbTable
   */
class table extends WebObject
{
  function init()
  {
    $this->addSVar('query', UNDEFINED);
  }

  function onRender()
  {
    //get the parameters
    $query = $this->params["query"]; 
    $query = WebApp::replaceVars($query);
    $table = $this->params["table"]; 
    $table = WebApp::replaceVars($table);
    $fields = $this->params["fields"];
    $fields = WebApp::replaceVars($fields);
    $where = $this->params["where"];
    $where = WebApp::replaceVars($where);

    //get the recordset "{{table}}->records"
    if ($query=='')
      {
	if ($where!="")  $where = "WHERE $where";
	$query = "SELECT $fields FROM $table $where";
      }
    $this->setSVar('query', $query);
    $table_records = new EditableRS($table.'->records', $query);
    $table_records->Open();

    //get the recordset "{{table}}->fields"
    $table_fields = new EditableRS($table.'->fields');
    foreach ($table_records->Fields() as $fld_name=>$fld_value)
      {
	$table_fields->addRec(array("fld_name"=>$fld_name));
      }

    //add the recordsets to $webPage
    global $webPage;
    $webPage->addRecordset($table_fields);
    $webPage->addRecordset($table_records);
  }
}
?>