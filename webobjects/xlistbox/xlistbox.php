<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * @package     webobjects
   * @subpackage  listbox
   */
class xlistbox extends WebObject
{
  function onRender()
  {
    if ($this->params["mainlist"]=="false")
      {
        $this->add_listsize_rs();
      }
  }

  function add_listsize_rs()
  {
    //build the listsize recordset
    $obj_name = $this->get_obj_name();
    $rs = new EditableRS($obj_name."_listsize");
    $list_size = $this->get_sublist_size();
    for ($i=0; $i < $list_size; $i++)
      {
        $rec = array("dummy"=>"");
        $rs->addRec($rec);
      }

    //add this recordset to the page
    global $webPage;
    $webPage->addRecordset($rs);
  }

  function get_sublist_size()
  {
    //get the query of the slave_list
    global $webPage;
    $rs_id = $this->params["rs"];
    $rs_id = WebApp::replaceVars($rs_id);
    $rs = $webPage->getRecordset($rs_id);
    $query = $rs->query;

    //get the count query
    $cnt_query = $this->get_count_query($query);

    //find the max count
    $max_count = 0;
    $rs = WebApp::sqlQuery($cnt_query);
    while (!$rs->EOF())
      {
        $count = $rs->Field("count");
        if ($count > $max_count)  $max_count = $count;
        $rs->MoveNext();
      }

    return $max_count;
  }

  function get_count_query($query)
  {
    eregi("(SELECT .*)([[:space:]]FROM .*)", $query, $regs);
    $before_from = $regs[1];
    $after_from = $regs[2];

    //get the field name of the master_id
    eregi("SELECT[[:space:]]+(.*)", $before_from, $regs);
    $arr_fields = explode(",", $regs[1]);
    for ($i=0; $i < sizeof($arr_fields); $i++)
      {
        list($fld_name, $fld_alias) = split(" +[Aa][Ss] +", $arr_fields[$i]);
        if (trim($fld_alias)=="master_id") break;
      }
    $arr = split("[[:space:]]+", trim($fld_name));
    $fld_name = array_pop($arr);

    if (eregi("GROUP +BY", $after_from))
      {
        $after_from = eregi_replace("ORDER +BY.*", "", $after_from);
        eregi("GROUP +BY(.*)", $after_from, $regs);
        $group_by_fields = $regs[1];
        $after_from = eregi_replace("GROUP +BY.*", "", $after_from);
        $count_query = "SELECT COUNT(DISTINCT $group_by_fields) AS count $after_from";
      }
    else if (eregi("SELECT +DISTINCT", $before_from))
      {
        $before_from = eregi_replace("SELECT +(DISTINCT.*)", "SELECT COUNT(\\1) AS count", $before_from);
        $count_query = $before_from.$after_from;
      }
    else
      {
        $count_query = "SELECT COUNT(1) AS count" . $after_from;
      }

    $count_query .= " GROUP BY $fld_name";
    return $count_query;
  }
}
?>