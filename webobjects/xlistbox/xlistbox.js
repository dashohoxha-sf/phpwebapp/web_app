// -*-C-*-
/*
  This file  is part of phpWebApp,  which is a  framework for building
  web application based on relational databases.

  Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  phpWebApp is free software; you can redistribute it and/or modify it
  under the  terms of the GNU  General Public License  as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  phpWebApp is  distributed in  the hope that  it will be  useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along with phpWebApp; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


/**
 * Select the item of the list with the given id.
 */
function xlistbox_select(list, id)
{
  for (i=0; i < list.options.length; i++)
    {
      if (list.options[i].value==id)
        list.options[i].selected = true;
      else
        list.options[i].selected = false;
    }
}

/**
 * Refills the sublist with values corresponding to the value selected
 * in the master listbox. Called by onchange of the master list.
 *
 * master     -- the master list
 * sublist_name -- the name of the sublist (that will be refilled)
 * select_id  -- the value to be selected as default in the sublist, optional
 */
function refill_sublist(master, sublist_name, select_id)
{
  var selIdx = master.selectedIndex;
  var master_id = master.options[selIdx].value;
  var sublist = eval("document." + master.form.name + "." + sublist_name);

  fill_sublist(sublist, master_id, select_id);
}

/**
 * Fills the sublist with values corresponding to the given master_id.
 *
 * sublist    -- the the sublist that will be filled
 * master_id  -- the id that will be used to filter the values of the sublist
 * select_id  -- the value to be selected as default in the sublist, optional
 */
function fill_sublist(sublist, master_id, select_id)
{
  var arr_items = eval("arr_" + sublist.name);
  var item, i, j;

  //empty all the content of the listbox
  sublist.options[0].selected = true;
  for (i=1; i < sublist.options.length; i++)
    {
      sublist.options[i].text  = "";
      sublist.options[i].value = "";
      sublist.options[i].selected = false;
    }

  //fill it with new values from array arrDepts
  i = 0;
  for (j=0; j < arr_items.length; j++)
    {
      item = arr_items[j];
      if (item.master_id == master_id)
        {
          i++;
          sublist.options[i].text = item.label;
          sublist.options[i].value = item.id;
          if (item.id==select_id)
            {
              sublist.options[i].selected = true;
              sublist.options[0].selected = false;
            }
        }
    }
}
