<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * @package     webobjects
   * @subpackage  dbTable
   */
class dbTable extends WebObject
{
  function init()
  {
    $this->addSVar("selected_rec", UNDEFINED);
  }

  function on_rmAll($event_args)
  {
    $table  = $this->params["table"]; 
    $table = WebApp::replaceVars($table);
    $where = $this->params["where"]; 
    $condition = ($where=="" ? "1=1" : $where);
      
    //remove all 
    $vars["table"] = $table;
    $vars["condition"] = $condition;
    WebApp::execDBCmd("dbTable::removeAll", $vars);
  }

  function on_rm($event_args)
  {
    $table  = $this->params["table"]; 
    $table = WebApp::replaceVars($table);
    $rec_id = $event_args["rec_id"];
    $rec_id = str_replace("\\'", "'", $rec_id);

    //remove
    $vars["table"] = $table;
    $vars["rec_id"] = $rec_id;
    WebApp::execDBCmd("dbTable::remove", $vars);
  }

  function on_add($event_args)
  {
    $objVars = $this->getObjVars();
    $obj_name = $objVars["obj_name"];
    $editRecId = "editRecord::".$obj_name;
    WebApp::setSVar($editRecId."->rec_id", UNDEFINED);
    WebApp::setSVar($editRecId."->state", "add");
  }

  function on_edit($event_args)
  {
    $rec_id = $event_args["rec_id"];

    $objVars = $this->getObjVars();
    $obj_name = $objVars["obj_name"];
    $editRecId = "editRecord::".$obj_name;
    WebApp::setSVar($editRecId."->rec_id", $rec_id);
    WebApp::setSVar($editRecId."->state", "edit");
  }

  function on_select($event_args)
  {
    $rec_id = $event_args["rec_id"];
    $this->setSVar("selected_rec", $rec_id);
  }

  function onParse()
  {
    $this->add_selectedRec_vars();
  }

  /** add the fields of the selected record as variables */
  function add_selectedRec_vars()
  {
    $rec_id = $this->getSVar("selected_rec");
    if ($rec_id==UNDEFINED)  return;

    $rec_id = str_replace("\\'", "'", $rec_id);
    $vars["rec_id"] = $rec_id;
    $table  = $this->params["table"]; 
    $table = WebApp::replaceVars($table);
    $vars["table"] = $table;
    $rs = WebApp::openRS("dbTable::select", $vars);

    WebApp::addGlobalVars($rs->Fields());
  }

  function onRender()
  {
    //get the key fields and add the variable {{rec_id}}
    $arr_key = $this->getKey();
    $this->add_rec_id($arr_key);

    //get the recordsets "{{table}}->fields" and "{{table}}->records"
    $table_fields = $this->getTableFields();
    $table_records = $this->getTableRecords($arr_key);

    //add the recordsets to $webPage
    global $webPage;
    $webPage->addRecordset($table_fields);
    $webPage->addRecordset($table_records);
  }

  /** 
   * Returns an array of fields that are the keys of the table; 
   */
  function getKey()
  {
    global $cnn;

    $table  = $this->params["table"]; 
    $table  = WebApp::replaceVars($table);

    //get the primary keys
    $definition = $cnn->mdb2->getTableConstraintDefinition($table, 'primary');
    $arr_keys = array_keys($definition['fields']);

    //if no key found, take all the fields as key
    if (sizeof($arr_keys)==0)
      {
	$arr_keys = $cnn->mdb2->listTableFields($table);
      }

    return $arr_keys;
  }

  /**
   * Adds the variable {{rec_id}} which is a query condition
   * that identifies the record that is being or edited or deleted. 
   */
  function add_rec_id($arr_key)
  {
    for ($i=0; $i < sizeof($arr_key); $i++)
      {
        $fld = $arr_key[$i];
        $arr_flds[] = $fld."=\\'{{".$fld."}}\\'";
      }
    $rec_id = implode(" AND ", $arr_flds);
    WebApp::addVar("rec_id", $rec_id);
  }

  /**
   * Returns a recordset with the fields of the 
   * table that will be displayed.
   */
  function getTableFields()
  {
    //get the params of the <WebObject>
    $table  = $this->params["table"]; 
    $table  = WebApp::replaceVars($table);
    $fields = $this->params["fields"];

    $table_fields = new EditableRS($table."->fields");
    if ($fields=="*")
      {
        //all the fields of the table
	global $cnn;
	$cnn->mdb2->loadModule('Manager');
	$arr_fields = $cnn->mdb2->listTableFields($table);
	foreach ($arr_fields as $fld_name)
          {
            $table_fields->addRec(compact('fld_name')); 
          }
      }
    else
      {
        //add the fields selected in the parameter
        $arr_fields = explode(",", $fields);
        for ($i=0; $i < sizeof($arr_fields); $i++)
          {
            $fld_name = $arr_fields[$i];
            $rec = array("fld_name"=>$fld_name);
            $table_fields->addRec($rec);            
          }
      }
    return $table_fields;
  }

  /**
   * Returns the recordset that will fill the table (in the template).
   * Makes sure that key fields are selected as well.
   */
  function getTableRecords($arr_key)
  {
    //get parameter values
    $table  = $this->params["table"]; 
    $fields = $this->params["fields"];
    $where  = $this->params["where"];
    $table = WebApp::replaceVars($table);
    $where = WebApp::replaceVars($where);

    //make sure that the key fields are among the $fields
    if ($fields<>"*")
      {
        $arr_fields = explode(",", $fields);
        for ($i=0; $i < sizeof($arr_key); $i++)
          {
            $fld = $arr_key[$i];
            if (!in_array($fld, $arr_fields))  array_unshift($arr_fields, $fld); 
          }
        $fields = implode(",", $arr_fields);
      }

    //construct the query
    if ($where<>"")  $where = "WHERE $where";
    $query = "SELECT $fields FROM $table $where";

    //create and open the recordset
    $id = $table."->records";
    $table_records = new EditableRS($id, $query);
    $table_records->Open();

    return $table_records;
  }
}
?>