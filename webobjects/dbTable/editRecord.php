<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * @package     webobjects
   * @subpackage  dbTable
   */
class editRecord extends WebObject
{
  function init()
  {
    /** state can be 'add', 'edit' or 'hidden' */
    $this->addSVar("state", "hidden");
    /** the rec_id of the record that is being edited */
    $this->addSVar("rec_id", UNDEFINED);
  }

  function on_cancel($event_args)
  {
    $this->setSVar("state", "hidden");
    $this->setSVar("rec_id", UNDEFINED);
  }

  function on_save($event_args)
  {
    //get the related dbTable
    $edTbl = $this->get_dbTable();

    //get params 'table' and 'fields' of the dbTable
    $table = $edTbl->params["table"];
    $table = WebApp::replaceVars($table);
    $fields = $edTbl->params["fields"];

    //get $values
    while (list($name, $value) = each($event_args))
      {
        $arr_values[] = "$name='$value'";
      }
    $values = implode(", ", $arr_values);

    $state = $this->getSVar("state");
    if ($state=="add")
      {
        //insert
        $vars["table"] = $table;
        $vars["values"] = $values;
        WebApp::execDBCmd("editRecord::insert", $vars);
      }
    else if ($state=="edit")
      {
        //update
        $rec_id = $this->getSVar("rec_id");
        $rec_id = str_replace("\\'", "'", $rec_id);
        $vars = array(
                      "table" => $table,
                      "values" => $values,
                      "rec_id" => $rec_id
                      );
        WebApp::execDBCmd("editRecord::update", $vars);
      }
    else
      {
        $msg = "editRecord::save(): " . T_("invalid state 'v_state'.");
        $msg = str_replace('v_state', $state, $msg);
        print WebApp::warning_msg($msg);
      }

    $this->setSVar("state", "hidden");
  }

  /** returns the related 'dbTable' object */
  function get_dbTable()
  {
    global $webPage;

    $obj_vars = $this->getObjVars();
    $edTable_id = "dbTable::".$obj_vars["obj_name"];
    $dbTable = $webPage->getObject($edTable_id);

    return $dbTable;
  }

  function onRender()
  {
    //get the related dbTable
    $edTbl = $this->get_dbTable();

    //get params 'table' and 'fields' of the dbTable
    $table = $edTbl->params["table"];
    $table = WebApp::replaceVars($table);
    $fields = $edTbl->params["fields"];

    //get the recordset {{table}}->fields
    global $webPage;
    $table_fields = $webPage->getRecordset($table."->fields"); 
     
    $state = $this->getSVar("state");
    if ($state=="add")
      {
        //add empty variables for each field
        $table_fields->MoveFirst();
        while (!$table_fields->EOF())
          {
            $fld_name = $table_fields->Field("fld_name");
            WebApp::addVar($fld_name, "");
            $table_fields->MoveNext();
          }
      }
    else if ($state=="edit")
      {
        //add vars for each field of the selected record
        $rec_id = $this->getSVar("rec_id");
        $rec_id = str_replace("\\'", "'", $rec_id);
        $query = "SELECT $fields FROM $table WHERE $rec_id";
        $rs = new Recordset($edTbl->params['id'].'_editRecord', $query);
        $rs->Open();
        WebApp::addVars($rs->Fields());
      }
  }
}
?>