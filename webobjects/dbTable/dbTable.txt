
WebObject 'dbTable' is used to display and edit the content
of a database table inside a page. It is used like this:

    <Include SRC="{{DBTABLE_PATH}}dbTable.html" />

    <WebObject Class="dbTable" Name="offices" 
               table    = "offices"
               fields   = "off_id,name" 
               where    = "off_id < 10" 
               editable = "true"
               selectable="true" />

The attribute 'table' is the name of the table that will be displayed. 
If it is not specified, its default value is {{obj_name}} (the value
of attribute 'Name').

The attribute 'fields' is the list of fields that will be displayed.
It is optional and its default value is "*" (all fields).

The attribute 'where' is the select condition for selecting the records.
It is optional and its default value is "" (all the records).

The attributes 'editable' and 'selectable' have default values of "false".
In this case the 'dbTable' behaves just like 'table'. 

When 'editable' has a value "true", then the table is editable, 
i.e. its records can be added, deleted or modified. 

When 'selectable' has a value "true", then it is possible to select 
a record from the displayed ones. The fields of the selected record
are declared as global template variables and can be used where they
are needed.

Since WebObject 'dbTable' behaves like 'table' when 'selectable' and 
'editable' are "false", then there is no need to use 'table'. But it
is simpler than 'dbTable' and it can be easier to modify and customize
to fit the needs of a certain application.
