// -*-C-*-
/*
  This file  is part of phpWebApp,  which is a  framework for building
  web application based on relational databases.

  Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  phpWebApp is free software; you can redistribute it and/or modify it
  under the  terms of the GNU  General Public License  as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  phpWebApp is  distributed in  the hope that  it will be  useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along with phpWebApp; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function dbTable_rmAll(obj_id)
{
  var msg = "You are deleting all the records\n"
    + "that are displayed in this table.\n\n"
    + "Are you sure you want to delete them all?";
  if (confirm(msg))
    {
      SendEvent(obj_id, "rmAll");
    }
}

function dbTable_rm(obj_id, rec_id)
{
  var msg = "You are deleting a record.";
  if (confirm(msg))
    {
      SendEvent(obj_id, "rm", "rec_id="+rec_id);
    }
}

function dbTable_add(obj_id)
{
  SendEvent(obj_id, "add");
}

function dbTable_edit(obj_id, rec_id)
{
  SendEvent(obj_id, "edit", "rec_id="+rec_id);
}

function dbTable_select(obj_id, rec_id)
{
  SendEvent(obj_id, "select", "rec_id="+rec_id);
}
