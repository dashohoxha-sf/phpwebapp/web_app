// -*-C-*-
/*
  This file  is part of phpWebApp,  which is a  framework for building
  web application based on relational databases.

  Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  phpWebApp is free software; you can redistribute it and/or modify it
  under the  terms of the GNU  General Public License  as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  phpWebApp is  distributed in  the hope that  it will be  useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along with phpWebApp; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function editRecord_cancel(obj_id)
{
  SendEvent(obj_id, "cancel");
}

function editRecord_save(obj_name)
{
  var obj_id = "editRecord::"+obj_name;
  var event_args = getEventArgs(obj_name);

  SendEvent(obj_id, "save", event_args);
}

/** returns the data of the form 'obj_name' 
 *  as an event arg string */
function getEventArgs(obj_name)
{
  var form = eval("document."+obj_name);
  var args = Array();
  var i, elem, event_args;

  for (i=0; i < form.elements.length; i++)
    {
      elem = form.elements[i];
      if (elem.type=="text")
        {
          args.push(elem.name + "=" + elem.value);
        }
    }

  event_args = args.join(";");
  return event_args;
}
