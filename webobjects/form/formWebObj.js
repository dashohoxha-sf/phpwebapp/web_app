// -*-C-*-
/*
  This file  is part of phpWebApp,  which is a  framework for building
  web application based on relational databases.

  Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  phpWebApp is free software; you can redistribute it and/or modify it
  under the  terms of the GNU  General Public License  as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  phpWebApp is  distributed in  the hope that  it will be  useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along with phpWebApp; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/** 
 * Transmit the data of the form, so that 
 * the changed values are not lost.
 * Must be called before GoTo().
 */
function saveFormData(form)
{
  transmitVar("formData", getFormData(form)); 
}

/** 
 * Returns a string that can be sent as the 
 * arguments of an event in the GoTo() function;
 * this string includes all the inputs of the given form.
 */
function getEventArgs(form)
{
  var formData = getFormData(form);
  //alert(formData);

  var arrFormData = eval(formData);
  var event_args="";
  for (i=0; i<arrFormData.length; i++)
    {
      data = arrFormData[i];
      value = (data.type=="checkbox" ? data.checked : data.value);
      event_args += ";" + data.name + "=" + value;
    }
  event_args = event_args.substr(1);
  return event_args;
}

/** returns all the data filled in the given form */
function getFormData(form)
{
  var arrFormData = new Array();
  var val;

  for (i=0; i<form.length; i++)
    {
      elem = form.elements[i];
      val = encode_str(elem.value);
    
      switch (elem.type)
        {
        case "text":
        case "password":
        case "hidden":
        case "textarea":
          data = {
                   type:  elem.type, 
                   name:  elem.name, 
                   value: encode_arg_value(val)
	         };
          arrFormData.push(data);
          break;

        case "select-one":
          if (elem.selectedIndex==-1)  break;  //nothing selected
          val = elem.options[elem.selectedIndex].value;
          val = encode_str(val);
          data = {type: elem.type, name: elem.name, 
                  value: val, selectedIndex: elem.selectedIndex};
          arrFormData.push(data);
          break;

        case "radio":
          if (i==0)  { radioIdx = 0; }
          else
            {
              prev_elem = form.elements[i-1];
              if (prev_elem.type=="radio" && prev_elem.name==elem.name)
                { radioIdx++; }
              else
                { radioIdx = 0; }
            }

          if (elem.checked)
            {
              data = {type: elem.type, name: elem.name, 
                      value: val, selectedIndex: radioIdx};
              arrFormData.push(data);
            }
          break;

        case "checkbox":
          data = {type: elem.type, name: elem.name,
                  value: val, checked: elem.checked};
          arrFormData.push(data);
          break;
        }    
    }

  return formData2str(arrFormData);
}

/** escapes some chars inside the string, like ", \n, etc. */
function encode_str(str)
{
  str = str.replace(/\n/g,"\\n");
  str = str.replace(/\"/g,"\\\"");
  return str;
}

function formData2str(arrFormData)
{
  var str, str_data;
  var arr_data = new Array();

  for (i=0; i<arrFormData.length; i++)
    {
      data = arrFormData[i];
      str = '{type:"' + data.type + '"'
        + ', name:"' + data.name + '"'
        + ', value:"' + data.value + '"';
      switch (data.type)
        {
        case "hidden":
        case "text":
        case "password":
        case "textarea":
          str += '}';
          break;
        case "select-one":
        case "radio":
          str += ', selectedIndex:' + data.selectedIndex + '}';
          break;
        case "checkbox":
          str += ', checked:' + data.checked + '}';
          break;
        }
      arr_data.push(str);
    }
  str_data = '[' + arr_data.join(',') + ']';
  return str_data;
}

/** fills the form with the given data */
function setFormData(form, formData)
{
  if (formData.charAt(0)!='[')  return;

  var arrFormData = eval(formData);

  for (i=0; i<arrFormData.length; i++)
    {
      data = arrFormData[i];
      elem = eval("form."+data.name);
      if (!elem)  continue;
      switch (data.type)
        {
        case "hidden":
        case "text":
        case "password":
        case "textarea":
          elem.value = data.value;
          break;
        case "select-one":
          selIdx = data.selectedIndex;
          elem.options[selIdx].selected = true;
          break;
        case "radio":
          selIdx = data.selectedIndex;
          elem[selIdx].checked = true;
          break;
        case "checkbox":
          elem.checked = data.checked;
          break;
        }
    }
}
