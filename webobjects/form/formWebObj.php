<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * @package     webobjects
   * @subpackage  formWebObj
   */
class formWebObj extends WebObject
{
  function afterParse()
  {
    global $webPage;

    //prepend js code to the content of this template
    $form_url = WebApp::to_url(FORM_PATH);
    $js_code = "<script type=\"text/javascript\" language=\"javascript\" "
      . "src=\"".$form_url."formWebObj.js\"></script>\n";
    $webPage->append_to_head($js_code);

    //append js code to the content of this template
    global $formData;  //form data comes with transmitVar() as a global var
    if (isset($formData))
      {
        $form_name = $this->get_form_name();
        $js_code = "
<script type=\"text/javascript\" language=\"javascript\">
//<![CDATA[
  var form = document.$form_name;
  var formData = '$formData';
  setFormData(form, formData);
//]]>
</script>
";
        $this->contents .= $js_code;
      }
  }

  /** finds and returns the name of the form in this template */
  function get_form_name()
  {
    $pattern = '/<form[^>]*name=("|\')([^\\1]+)\\1/';
    preg_match($pattern, $this->contents, $matches);
    $form_name = $matches[2];
    return $form_name;
  }

  /**
   * $record is an associative array where keys are named after
   * the fields of the table. It is assumed that $table has
   * a primary key that is an autoincrement number.
   * The function builds and executes a query that inserts a row
   * in the table.
   */
  function insert_record($record, $table, $conn =UNDEFINED)
  {
    if ($conn==UNDEFINED)
      {
	global $cnn;
	$conn = $cnn;
      }

    $record = $this->fix_record($record, $table, $conn);
    //$record = $this->pad_record($record, $table, $conn);
    //$values = $this->get_query_values($record);

    $conn->mdb2->loadModule('Extended');
    $nr_rows = $conn->mdb2->extended->autoExecute($table, $record,
						  MDB2_AUTOQUERY_INSERT);
  }

  /**
   * $record is an associative array where keys are named after
   * the fields of the table. $keys is the fields of the primary key.
   * The function builds and executes a query that updates the
   * fields given in the $record (which includes the primary key as well).
   */
  function update_record($record, $table, $keys, $conn =UNDEFINED)
  {
    if ($conn==UNDEFINED)
      {
	global $cnn;
	$conn = $cnn;
      }

    $record = $this->fix_record($record, $table, $conn);

    //build the WHERE condition of the primary key
    $arr_keys = explode(',', $keys);
    $arr_conditions = array();
    for ($i=0; $i < sizeof($arr_keys); $i++)
      {
        $key = $arr_keys[$i];
        $val = $record[$key];
	$val = $cnn->mdb2->quote($val);
        $arr_conditions[] = "($key = $val)";
      }
    $condition = implode(' AND ', $arr_conditions);

    $conn->mdb2->loadModule('Extended');
    $nr_rows = $conn->mdb2->extended->autoExecute($table, $record,
						  MDB2_AUTOQUERY_UPDATE,
						  $condition);
  }

  /** Get an array of the field names of the table. */
  function get_table_fields($table_name, $conn =UNDEFINED)
  {
    if ($conn==UNDEFINED)
      {
	global $cnn;
	$conn = $cnn;
      }

    $conn->mdb2->loadModule('Manager');
    $arr_fields = $conn->mdb2->listTableFields($table_name);

    return $arr_fields;
  }

  /**
   * $record is an associative array with values for some fields of
   * the given table. This function removes any fields that are not
   * in the table.
   */
  function fix_record($record, $table_name, $conn =UNDEFINED)
  {
    //get an array of the field names of the table
    $arr_fields = $this->get_table_fields($table_name, $conn);

    //remove from the record any fields that does not exist in the table
    while (list($fld_name,$fld_value) = each($record))
      {
        if (!in_array($fld_name,$arr_fields))  unset($record[$fld_name]);
      }

    return $record;
  }

  /**
   * $record is an associative array with values for some fields of
   * the given table. This function adds empty values for the missing fields.
   */
  function pad_record($record, $table_name, $conn =UNDEFINED)
  {
    //get an array of the field names of the table
    $arr_fields = $this->get_table_fields($table_name, $conn);

    //pad with empty values any missing fields
    for ($i=0; $i < sizeof($arr_fields); $i++)
      {
        $fld_name = $arr_fields[$i];
        if (!isset($record[$fld_name]))  $record[$fld_name] = '';
      }

    return $record;
  }

  /**
   * $record is an associative array of field names and their values.
   * The function constructs and returns a list of values that can be
   * used in an insert or update query, like this:
   * fld1='val1', fld2='val2', fld3='val3'
   */
  function get_query_values($record)
  {
    while (list($fld_name, $fld_value) = each($record))
      {
        if ($fld_value=='NULL')
          {
            $arr_values[] = "$fld_name = NULL";
          }
        else
          {
            $arr_values[] = "$fld_name = '$fld_value'";
          }
      }
    $values = implode(",\n    ", $arr_values);
    return $values;
  }

  /**
   * From the items of the given array create a listbox recordset
   * (with the fields 'id' and 'label'), and insert it in the $webPage.
   * $arr_options can be an associated array, a simple array, or mixed.
   * In case of the associated array, the keys are used as id-s and 
   * the values are used as labels.
   */
  function add_listbox_rs($rs_id, $arr_options)
  {
    $rs = new EditableRS($rs_id);
    $arr_keys = array_keys($arr_options);
    for ($i=0; $i < sizeof($arr_keys); $i++)
      {
        $key = $arr_keys[$i];
        $value = $arr_options[$key];
        if (is_integer($key))  $key = $value;
        $rs->addRec(array('id' => $key, 'label' => $value));
      }
    global $webPage;
    $webPage->addRecordset($rs);
  }
}
?>
