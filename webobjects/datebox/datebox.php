<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * @package     webobjects
   * @subpackage  datebox
   */
class datebox extends WebObject
{
  function onRender()
  {
    //get parameters 'value' and 'format'
    $date = $this->params["value"];
    $date = WebApp::replaceVars($date);
    $format = $this->params["format"];
    $format = WebApp::replaceVars($format);

    //build and declare {{text_value}}
    $arr_date = explode("-", $date);
    if (sizeof($arr_date)<>3)
      {
        $text_value = $date;
      }
    else
      {
        list($year,$month,$day) = $arr_date;
        $text_value = $format;
        $text_value = str_replace("YYYY", $year, $text_value);
        $text_value = str_replace("MM", $month, $text_value);
        $text_value = str_replace("DD", $day, $text_value);
      }
    WebApp::addVar("text_value", $text_value);
  }
}
?>