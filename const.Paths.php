<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * Constants for the paths of the application framework.
   */
  /** */

  //package paths 
define("PARSER_PATH",           WEBAPP_PATH."parser/");
define("SESSION_PATH",          WEBAPP_PATH."session/");
define("L10N_PATH",             WEBAPP_PATH."l10n-module/");
define("DB_PATH",               WEBAPP_PATH."database/");
define("APPEND_PATH",           WEBAPP_PATH."append/");
define("TIMER_PATH",            WEBAPP_PATH."timer/");

//WebObject paths
define("WEBOBJ_PATH",           WEBAPP_PATH."webobjects/");
define("FORM_PATH",             WEBOBJ_PATH."form/");
define("DATEBOX_PATH",          WEBOBJ_PATH."datebox/");
define("LISTBOX_PATH",          WEBOBJ_PATH."listbox/");
define("XLISTBOX_PATH",         WEBOBJ_PATH."xlistbox/");
define("TABLE_PATH",            WEBOBJ_PATH."table/");
define("DBTABLE_PATH",          WEBOBJ_PATH."dbTable/");
define("TABS_PATH",             WEBOBJ_PATH."tabs/");

//WebBox paths
define("WEBBOX_PATH",           WEBAPP_PATH."boxes/");
define("FOLDERLISTING_PATH",    WEBBOX_PATH."folderListing/");
define("FOLDERMANAGER_PATH",    WEBBOX_PATH."folderManager/");
define("EDITMENUS_PATH",        WEBBOX_PATH."editMenus/");
define("FILEVIEW_PATH",         WEBBOX_PATH."fileView/");
define("CODEVIEWER_PATH",       WEBBOX_PATH."codeViewer/");
define("DOCBOOK_PATH",          WEBBOX_PATH."docbook/");
define("WEBNOTES_PATH",         WEBBOX_PATH."webnotes/");

//tools paths
define("TOOLS_PATH",            WEBAPP_PATH."tools/");
define("BROWSER_PATH",          TOOLS_PATH."fileBrowser/");
?>
