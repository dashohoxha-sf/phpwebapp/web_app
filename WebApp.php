<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  //paths of the application framework
include_once WEBAPP_PATH."const.Paths.php";

//include localization component
include_once L10N_PATH."class.L10n.php";

//include session
include_once SESSION_PATH."class.Request.php";
include_once SESSION_PATH."class.Event.php";
include_once SESSION_PATH."class.Session.php";

//include parse_render component
include_once PARSER_PATH."package.ParseRender.php";

//include database component
include_once DB_PATH."package.DB.php";

include_once WEBAPP_PATH."class.WebApp.php";

//declare constants
define("UNDEFINED", "undefined");
define("WEBAPP_URL", WebApp::to_url(WEBAPP_PATH));

//global variables
$request = new Request; 
$tplVars = new VarStack;
$webPage = new WebPage;
$l10n    = new L10n;
$parser  = new Parser;
$render  = new Render;
$event   = new Event;
$session = new Session;
?>