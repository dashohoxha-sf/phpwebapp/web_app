<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


include_once DB_PATH."class.Recordset.php";

/**
 * Same as class Recordset, but it is opened only once in a page.
 * This is created by tag <Recordset> when it has the atttribute
 * static. It is used for recordsets that are used many times in a
 * page without changing, e.g. when a recordset fills a listbox
 * and this listbox is used many times in the page.
 *
 * @package database
 */
class StaticRS extends Recordset
{
  /** true when the recordset is opened */ 
  var $opened;

  function StaticRS($id =UNDEFINED, $query =UNDEFINED, $conn =UNDEFINED)
  {
    $this->Recordset($id, $query, $conn);
    $this->type = "StaticRS";
    $this->opened = false;
  }

  function Open($conn =UNDEFINED)
  {
    if ($this->opened)
      {
        $this->MoveFirst();
        return; //don't open it a second time
      }
    if ($this->query<>"")  Recordset::Open($conn);
    $this->opened = true;
  }

  /**
   * Override the Close() of superclass 
   * with an empty function.
   */
  function Close() {}
}
?>