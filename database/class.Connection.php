<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * Abstract class for creating a connection to the database.
   * @package database
   */
class Connection
{
  /** The connection to the DB. */
  var $mdb2;

  /** Opens a connection using the given parameters. */
  function Connection($MDB2_DSN, $MDB2_OPTIONS =array())
  {
    $this->mdb2 =& MDB2::factory($MDB2_DSN, $MDB2_OPTIONS);
    if (PEAR::isError($this->mdb2))
      {
	$this->show_error($this->mdb2->getMessage());
      }
    else
      {
	$this->mdb2->setFetchMode(MDB2_FETCHMODE_ASSOC);
      }
  }
        
  /** 
   * Returns a two-dimentional array, indexed by
   * row nr and field name. The parameter $query
   * must be a SELECT. In case of any problems 
   * returns false.
   */
  function query($query, $query_id =UNDEFINED)
  {
    global $webPage;

    $proc_id = $this->get_proc_id($query_id);
    $dsn = $this->getDSN();
    $comment = "DSN:  $dsn \nQuery: \n$query";
    $webPage->timer->Start($proc_id, $comment);

    $result = $this->mdb2->query($query);
    if (PEAR::isError($result))
      {
	$error_msg = $result->getMessage().' :: '.$result->getUserInfo();
	$this->show_error($error_msg, $query);
	$table = false;
      }
    else
      {
	$table = $this->result2table($result);
      }

    $webPage->timer->Stop($proc_id);
                
    return $table;
  }

  /** Convert the $result to a two-dimentional table. */
  function result2table(&$result)
  {                
    $arr_result = array();
    $i = 0;
    while ( $row = $result->fetchRow(MDB2_FETCHMODE_ASSOC) )
      {
	//$arr_result[$i] = $row;
        while ( list($fld_name, $fld_value) = each($row) )
          {
            if (!isset($fld_value))  $fld_value = NULL_VALUE;
            $arr_result[$i][$fld_name] = $fld_value;
          }
        $i++; //next row
      }
    $result->free();

    return $arr_result;
  }

  /**
   * Executes a manipulation query (like insert, update, etc.).
   * Returns the number of the affected rows or false.
   */
  function exec($query, $query_id =UNDEFINED)
  {
    global $webPage;

    $proc_id = $this->get_proc_id($query_id);
    $dsn = $this->getDSN();
    $comment = "DSN:  $dsn \nQuery: \n$query";
    $webPage->timer->Start($proc_id, $comment);

    $result = $this->mdb2->exec($query);
    if (PEAR::isError($result))
      {
	$error_msg = $result->getMessage().' :: '.$result->getUserInfo();
	$this->show_error($error_msg, $query);
	$affected = false;
      }
    else
      {
	$affected = $result;
      }

    $webPage->timer->Stop($proc_id);
                
    return $affected;
  }

  /** Set the limit for the next query that will be executed */
  function setLimit($limit, $offset =UNDEFINED)
  {
    if ($offset==UNDEFINED)
      $this->mdb2->setLimit($limit);
    else
      $this->mdb2->setLimit($limit, $offset);
  }

  /**
   * Returns a process id to uniquely identify a query in the timer.
   */
  function get_proc_id($q_id =UNDEFINED)
  {
    if (!EXECUTION_TIME_INFO)  return;

    global $webPage;
    static $id = "_001";
                
    if ($q_id===UNDEFINED)
      {
        $proc_id = "query".$id++;
      }
    else
      {
        $proc_id = "rs_".$q_id;
        if (isset($webPage->timer->info[$proc_id]))     $proc_id .= "_01";
        while (isset($webPage->timer->info[$proc_id]))  $proc_id++;
      }

    return $proc_id;
  }

  function show_error($err_msg, $query ='')
  {
    $err_msg = htmlentities($err_msg);

    if (trim($query) != '')
      {
	$query = '<pre>' . htmlentities($query) . '</pre>';
      }

    $error_html = "
<div class='db_error'>
<strong>DB Error:</strong> $err_msg
$query</div>";
    print $error_html;
  }

  /** Returns the dsn of the MDB2 object as a string (for debugging). */
  function getDSN()
  {
    $arr_dsn = $this->mdb2->dsn;
    $str_dsn = $arr_dsn['dbsyntax'] . '://'
      . $arr_dsn['username'] . '@' . $arr_dsn['hostspec']
      . '/' . $arr_dsn['database'];
    return $str_dsn;
  }
}
?>