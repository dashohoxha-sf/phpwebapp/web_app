<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

include_once DB_PATH."class.StaticRS.php";

/**
 * Same as class StaticRS, but editable: the programer can add,
 * insert, remove or change its records.
 * 
 * @package database
 */
class EditableRS extends StaticRS
{
  function EditableRS($id =UNDEFINED, $query =UNDEFINED, $conn =UNDEFINED)
  {
    $this->StaticRS($id, $query, $conn);
    $this->type = "EditableRS";
    $this->opened = ($query==UNDEFINED ? true : false);
  }

  /** 
   * Applies the function $fun_name on each record
   * of the recordset. $fun_name is a global function
   * that takes as parameter an associated array by reference.  
   */
  function apply($fun_name)
  {
    for ($i=0; $i < $this->count; $i++)
      {
        $fun_name($this->contents[$i]);
      }
  }

  /** Sets a new value to the given field (in the current record). */
  function setFld($fld_name, $fld_value)
  {
    if ($this->EOF())
      {
        $msg = TF_("pointer is at EOF");
        print WebApp::error_msg("EditableRS::setFld(): $msg.\n");
        return;
      }
    if ($this->BOF())
      {
        $msg = TF_("pointer is at BOF");
        print WebApp::error_msg("EditableRS::setFld(): $msg.\n");
        return;
      }

    $rec = &$this->contents[$this->pos];
    if (!isset($rec[$fld_name]))
      {
        $msg = "field 'v_fld_name' does not exist";
        $msg = str_replace('v_fld_name', $fld_name, $msg);
        print WebApp::error_msg("EditableRS::setFld(): $msg.\n");
        return;         
      }
    $rec[$fld_name] = $fld_value;
  }

  /** Changes some fields of the current recordset. */
  function setFlds($arr_flds)
  {
    while (list($fld_name,$fld_value) = each($arr_flds))
      {
        $this->setFld($fld_name,$fld_value);
      }
  }

  /**
   * Adds a new record after the current record,
   * the new record becomes current.
   */
  function addRec($rec)
  {
    if ($this->count==0) //recordset empty
      {
        $this->count++;
        $this->contents[0] = $rec;
        $this->pos = 0;
        return;
      }

    //for a non-empty recordset
    for ($i=$this->count; $i > ($this->pos+1); $i--)
      {
        $this->contents[$i] = $this->contents[$i-1];
      }
    if (!$this->BOF())
      {
        $this->contents[$i] = $this->contents[$i-1];
      }
    $this->count++;
    $this->pos++;
    if ($this->EOF())  $this->pos--;
    $this->setFlds($rec);
  }

  //ToDo: The implementation of this function can be more efficient
  function addRecs($arr_recs)
  {
    for ($i=0; $i < sizeof($arr_recs); $i++)
      {
        $rec = $arr_recs[$i];
        $this->addRec($rec);
      }
  }

  /**
   * Insert a new record before the current record;
   * the new record becomes the current record.
   */
  function insRec($rec)
  {
    if ($this->count==0)  //recordset empty
      {
        $this->count++;
        $this->contents[0] = $rec;
        $this->pos = 0;
        return;
      }

    //for a non-empty recordset
    for ($i=$this->count; $i > ($this->pos + 1); $i--)
      {
        $this->contents[$i] = $this->contents[$i-1];
      }
    if (!$this->BOF())
      {
        $this->contents[$i] = $this->contents[$i-1];
      }
    $this->count++;
    if ($this->BOF())  $this->pos++;
    $this->setFlds($rec);
  }

  //ToDo: The implementation of this function can be more efficient
  function insRecs($arr_recs)
  {
    for ($i=0; $i < sizeof($arr_recs); $i++)
      {
        $rec = $arr_recs[$i];
        $this->insRec($rec);
      }
  }
  
  /** Append the content of the given $rs to $this RS. */
  function append($rs)
  {
    $pos = $this->pos;
    $this->MoveLast();
    $rs->MoveFirst();
    while (!$rs->EOF())
      {
        $rec = $rs->Fields(); 
        $this->addRec($rec);
        $rs->MoveNext();
      }
    $this->pos = $pos;
  }

  function rmRec()
  {
    $this->rmRecs(1);
  }

  /**
   * Removes $nr recs from the recordset,
   * starting with the current record.
   * If $nr is not given or exeeds the EOF,
   * then removes all the recs up to the EOF.
   */
  function rmRecs($nr =UNDEFINED)
  {
    if ( ($nr===UNDEFINED) or 
         ($this->pos + $nr > $this->count) )
      {
        //set $nr to the number of all remaining
        //recs up to the EOF (including the current rec)
        $nr = $this->count - $this->pos;
      }

    //get the number of the remaining elements
    //from the current rec up to the EOF
    $rem = $this->count - $this->pos - $nr;

    //move up all the recs that will remain
    for ($i=0; $i < $rem; $i++)
      {
        $i1 = $this->pos + $i;        //new position
        $i2 = $this->pos + $i + $nr;  //old position
        if ($i1 >= 0) //at BOF, $this->pos is -1
          {
            $this->contents[$i1] = $this->contents[$i2];
          }
      }

    //delete the last recs
    for ($i=$this->pos+$rem; $i < $this->count; $i++)
      {
        unset($this->contents[$i]);
      } 
    //update the count of the recs
    $this->count -= $nr;
  }

  /**
   * Adds a new column to the recordset, which has the
   * given name and an optional default value.
   */
  function addCol($fld_name, $default_value =UNDEFINED)
  {
    if (isset($this->contents[0][$fld_name]))  return;

    for ($i=0; $i < $this->count; $i++)
      {
        $this->contents[$i][$fld_name] = $default_value;
      }
  }

  /**
   * Change the name of a column.
   */
  function renameCol($fld_name, $new_fld_name)
  {
    if (isset($this->contents[0][$new_fld_name]))
      {
        $msg = TF_("there is already a field named 'v_fld_name'");
        $msg = str_replace('v_fld_name', $new_fld_name, $msg);
        print WebApp::error_msg("EditableRS::renameCol(): $msg.\n");
        return;         
      }

    for ($i=0; $i < $this->count; $i++)
      {
        $this->contents[$i][$new_fld_name] = $this->contents[$i][$fld_name];
        unset($this->contents[$i][$fld_name]);
      }
  }
}
?>