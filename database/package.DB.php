<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * @package database
   */

require_once 'MDB2.php';

include_once DB_PATH."class.Connection.php";
include_once DB_PATH."class.Recordset.php";
include_once DB_PATH."class.StaticRS.php";
include_once DB_PATH."class.EditableRS.php";
include_once DB_PATH."class.PagedRS.php";
include_once DB_PATH."class.TableRS.php";


/**
 * Create a default connection for the application. Parameters 
 * of this connection are specified in 'config/const.Options.php'.
 * This connection will be used for each interaction with a DB,
 * if no other connection is specified. The application, however,
 * can create and use other connections as well, or override this one.
 */
if (defined('MDB2_DSN'))
  {
    $cnn = new Connection(MDB2_DSN, $MDB2_OPTIONS);
  }

/**
 * Create a connection that is used by web_app to connect to a database.
 * This connection can be different from the one that is used by the
 * application. However, if WEBAPP_MDB2_DSN is not defined, then the
 * default database of the application is used.
 */
if (defined('WEBAPP_MDB2_DSN'))
  {
    $webapp_cnn = new Connection(WEBAPP_MDB2_DSN, $WEBAPP_MDB2_OPTIONS);
  }
 else
   {
     $webapp_cnn = new Connection(MDB2_DSN, $MDB2_OPTIONS);
   }
?>