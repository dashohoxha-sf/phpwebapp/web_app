<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /** 
   * This class represents a recordset (the table returned by the
   * execution of a query). It executes the query using a given
   * connection with a database. If no connection is specified, then
   * it uses the global connection of the framework: $cnn.
   *
   * @package database
   */
class Recordset
{
  /** The ID of the Recordset. */
  var $ID;

  /** The type of the recordset: Recordset, PagedRS, 
   *  StaticRS, DynamicRS, EditableRS, TableRS */
  var $type;
        
  /** Query as string (may contain {{variables}}). */
  var $query;
  /** The connection that will execute the query. */
  var $cnn;
        
  /** The result after executing the query (2 dimensional array). */
  var $content;
  /** The current position in the array $content. */
  var $pos;
  /** The number of rows in the array $content, */
  var $count;
               

  function EOF()
  {
    return $this->pos >= $this->count;
  }

  function BOF()
  {
    return $this->pos < 0;
  }

  function MoveNext()
  {
    if ( !$this->EOF() )      $this->pos++;
  }

  function MovePrev()
  {
    if ( !$this->BOF() )      $this->pos--;
  }

  function MoveFirst()
  {
    $this->pos = 0;
  }

  function MoveLast()
  {
    $this->pos = $this->count - 1;
  }

  /**
   * Returns the value of the given field.
   * Returns UNDEFINED if there is no such field, or EOF or BOF.
   */
  function Field($fld_name)
  {
    if ( isset($this->contents[$this->pos][$fld_name]) )
      return $this->contents[$this->pos][$fld_name];
    else
      return UNDEFINED;
  }

  /** Returns all the fields at the current position as an associated array. */
  function Fields()
  {
    $fields = $this->contents[$this->pos];
    if (!is_array($fields))  $fields = array();
    return $fields;
  }

  /** Constructor */
  function Recordset($id =UNDEFINED, $query =UNDEFINED, $conn =UNDEFINED)
  {
    static $auto_recordset_id = "Recordset_01";
    global $cnn;

    $this->ID = ($id==UNDEFINED ? $auto_recordset_id++ : $id);
    $this->query = $query;
    if ($conn==UNDEFINED) 
      $this->cnn = clone($cnn);
    else
      $this->cnn = clone($conn);
    $this->contents = array();
    $this->count = 0;
    $this->pos = 0;
    $this->type = "Recordset";
  }

  /**
   * Executes the query and puts the result into $this->contents.
   * If $conn is given, it is used instead of its own connection.
   */
  function Open($conn =UNDEFINED)
  {
    $query = $this->query;
    if ($query==UNDEFINED)  return;

    $this->Close();           //empty the content

    $query = WebApp::replaceVars($query);
    $connection = ($conn==UNDEFINED ? $this->cnn : $conn);
    if (!is_object($connection))
      {
        $errmsg = TF_("Cannot open query because there is no valid connection.");
	Connection::show_error($errmsg, $query);
        return;
      }
    $result = $connection->query($query, $this->ID);

    if ($result)
      {
        $this->contents = $result;
        $this->count = count($result);
        $this->pos = 0;
      }
  }
        
  function Close()
  {
    if (isset($this->contents))   unset($this->contents);
    $this->count = 0;
    $this->pos = 0;
  }

  /**
   * Returns a new recordset that contains a slice of this recordset.
   * @see array_slice
   */
  function slice($offset =0, $length =UNDEFINED)
  {
    $slice = array();
    if ($length===UNDEFINED)
      {
        $slice = array_slice($this->contents, $offset);
      }
    else
      {
        $slice = array_slice($this->contents, $offset, $length);
      }

    //create a new recordset of the same type as this
    $rs = new $this->type;

    $rs->contents = $slice;
    $rs->count = sizeof($slice);
    $rs->pos = 0;

    return $rs;
  }

  /**
   * Returns true if the record at the given position
   * matches the $condition (usually used by the functions
   * find() and filter()). If no position is given
   * matches the current record.
   * Currently the $condition supports only one field.
   * @see find(), filter()
   */
  function match($condition, $pos=UNDEFINED)
  {
    //get the field name, pattern and match type from $condition
    list($fld_name,$pattern) = split("=", $condition, 2);
    $fld_name = trim($fld_name);
    $pattern = trim($pattern);
    $match_type = substr($pattern, 0, 1); //get first char
    //remove first and last chars
    $pattern = substr($pattern, 1, sizeof($pattern)-2); 

    //get the field value
    if ($pos===UNDEFINED)  $pos = $this->pos;
    $fld_value = $this->contents[$pos][$fld_name];

    //match with pattern
    if ($match_type=="'")
      {
        $result = ($fld_value==$pattern);
      }
    else
      {
        $result = ereg($pattern, $fld_value);
      }

    return $result;
  }

  /**
   * Returns a new recordset with the recs that match the
   * given condition, $condition is like the WHERE condition
   * of a query, but it matches using regexp-s, e.g.
   * <kbd>(username=/^d.*a/ AND (firstname=/[^a]+/ OR NOT lastname='Hoxha'))</kbd>
   *
   * @todo Currently it supports only one field, no AND, OR, (, ) etc.
   */
  function filter($condition)
  {
    $rs = new $this->type;
    $rs->Open();
    for ($i=0; $i < $this->count; $i++)
      {
        if ($this->match($condition, $i))
          {
            $rec = $this->contents[$i]; 
            $rs->addRec($rec);
          }
      }
    $rs->MoveFirst();
    return $rs;
  }

  /**
   * Finds the subset of the recordset that matches the
   * condition and positions the pointer to the first
   * record found. $condition is like the condition in filter().
   * If called without parameter, if finds the next one,
   * and so on up to EOF. Returns false if it couldn't find
   * another record.
   * @see filter()
   */
  function find($condition =UNDEFINED)
  {
    if ($condition<>UNDEFINED)   $this->search($condition);
    return $this->find_next();
  }

  /**
   * Used by find(), finds all the records that match
   * the condition and stores their indeces in
   * the array $this->found_positions.
   * @see find()
   */
  function search($condition)
  {
    $this->found_positions = array();
    $this->current_found = -1;
    for ($i=0; $i < $this->count; $i++)
      {
        if ($this->match($condition, $i))
          {
            $this->found_positions[] = $i;
          }
      }
  }

  /**
   * Used by find(), sets $this->pos to the next
   * found record; returns true if found,
   * or false if there are no more records.
   * @see find()
   */
  function find_next()
  {
    $found_nr = sizeof($this->found_positions);
    if ($this->current_found + 1 < $found_nr )
      {
        $this->current_found += 1;
        $idx = $this->current_found;
        $this->pos = $this->found_positions[$idx];
        return true;
      }
    else
      {
        return false;
      }
  }

  /** Returns an array with the values of the specified field. */
  function getColumn($fld_name)
  {
    $column = array();
    for ($i=0; $i < $this->count; $i++)
      {
        if ( isset($this->contents[$i][$fld_name]) )
          {
            $column[] = $this->contents[$i][$fld_name];
          }
      }
    return $column;
  }

  /**
   * Returns a new recordset that contains only the specified
   * columns. $fld_names is a comma separated list of field names.
   */
  function getColumns($fld_names)
  {
    $rs = new $this->type;
    $rs->Open();
    $arr_flds = explode(",", $fld_names);
    for ($i=0; $i < $this->count; $i++)
      {
        $rec = array();
        for ($j=0; $j < sizeof($arr_flds); $j++)
          {
            $fld = $arr_flds[$j];
            $rec[$fld] = $this->contents[$i][$fld];
          }
        $rs->addRec($rec);
      }
    $rs->MoveFirst();
    return $rs;
  }

  /** Sends the recordset as a tab delimited file for download. */
  function sendTabDelimitedFile($fname)
  {
    header("Content-Disposition: attachment; filename=\"$fname\"");
    header("Content-Type: application/vnd.ms-excel");
    print $this->toTabDelimited();
    exit;
  }

  /**
   * Returns the data of the recordset in the tab-delimited format
   */
  function toTabDelimited()
  {
    $output = '';
    if ($this->count == 0)  return $output;

    $this->MoveFirst();
    $output .= implode("\t", array_keys($this->Fields())) . "\n"; 
    while (!$this->EOF())
      {
	$rec = $this->Fields();
	$rec = $this->clean_data($rec);
	$output .= implode("\t", array_values($rec)) . "\n";
	$this->MoveNext();
      }
    $this->MoveFirst();

    return $output;
  }

  /** 
   * Escape tab and new line characters by replacing them by '\t' and '\n'.
   * Used to convert
   */
  function clean_data($assoc_arr)
  {
    $assoc_arr_1 = array();
    foreach($assoc_arr as $key=>$value)
      {
	$value = str_replace("\r", '', $value);
	$value = preg_replace("/\n/", "\\n", $value);
	$value = preg_replace("/\t/", "\\t", $value);
	$assoc_arr_1[$key] = $value;
      }
    return $assoc_arr_1;
  }
        
  /**
   * Convert the recordset to an excel file. 
   * If $fname is given, save it the given file, otherwise send it
   * to the browser for download.
   */
  function toExcelFile($title, $fname =UNDEFINED)
  {
    //include the excel writer
    require_once "Spreadsheet/Excel/Writer.php";

    //create a workbook
    if ($fname==UNDEFINED)
      $xls =& new Spreadsheet_Excel_Writer();
    else
      $xls =& new Spreadsheet_Excel_Writer($fname);

    //create a worksheet
    $worksheet = substr($title, 0, 30);
    $wks =& $xls->addWorksheet($worksheet);

    //create a format for the title
    $format =& $xls->addFormat();
    $format->setFontFamily('Helvetica');
    $format->setBold();
    $format->setSize('12');
    $format->setColor('navy');
    $format->setAlign('merge');

    //write the title to the top left cell of the worksheet
    $wks->writeString(0, 0, $title, $format);
    //merge with empty cells
    for ($i=0; $i < 10; $i++)  $arr[] = '';
    $wks->writeRow(0, 1, $arr, $format);
    $wks->setRow(0, 20);  //set row height

    //create a format for column headings
    $format =& $xls->addFormat();
    $format->setBold();
    $format->setFontFamily('Helvetica');
    $format->setBold();
    $format->setSize('11');
    $format->setAlign('center');

    //write column headings
    $this->MoveFirst();
    $colNames = array_keys($this->Fields());
    $wks->writeRow(1, 0, $colNames, $format);
    $wks->setRow(1, 15, $format);

    //freeze the top 2 rows (title and column headings)
    $freeze = array(2,0,2,0);
    $wks->freezePanes($freeze);

    //the current excel row number
    $row_nr = 2;

    //loop through the data, adding it to the sheet
    while (!$this->EOF())
      {
	$fields = $this->Fields();
	$col_nr = 0;
	foreach ($fields as $key=>$value)
	  {
	    $wks->writeString($row_nr, $col_nr, $value);		
	    $col_nr++;
	  }

	//move to the next row
	$row_nr++;
	$this->MoveNext();
      }

    //close the Spreadsheet to the browser
    $xls->send("$title.xls");
    $xls->close();

    if ($fname==UNDEFINED) exit;
  }

  /**
   * Returns the recordset as an HTML table
   * (mostly for debuging purposes).
   */
  function toHtmlTable($no_header =false)
  {
    $type = $this->type;
    if ($type=="PagedRS")
      {
        $type .= "(current_page='$this->current_page',"
          ." recs_per_page='$this->recs_per_page',"
          ."nr_of_recs='$this->nr_of_recs')";
      }
    $dsn = $this->cnn->getDSN();
    $htmlTable = "
<br />
<table border='0' cellspacing='1' cellpadding='0'>";

    if ($no_header==false)
      {
	$htmlTable .= "
  <tr><td>
    <table bgcolor='#aaaaaa' border='0' cellspacing='1' cellpadding='2'>
      <tr>
        <td bgcolor='#eeeeee' align='right'>ID:</td>
        <td bgcolor='#f9f9f9'>$this->ID</td>
      </tr>
      <tr>
        <td bgcolor='#eeeeee' align='right'>Type:</td>
        <td bgcolor='#f9f9f9'>$type</td>      
      </tr>
      <tr>
        <td bgcolor='#eeeeee' align='right'>DSN:</td>
        <td bgcolor='#f9f9f9'>$dsn</td>      
      </tr>
      <tr>
        <td bgcolor='#eeeeee' valign='top' align='right'>Query:</td>
        <td bgcolor='#f9f9f9'>$this->query</td>
      </tr>
    </table
  </td></tr>";
      }

    $htmlTable .= "
  <tr><td>
    <table bgcolor='#aaaaaa' border='0' cellspacing='1' cellpadding='2'>
";
    if ($this->count > 0)
      {
        $this->MoveFirst();
        $rec = $this->Fields();
        $htmlTable .= "\t<tr>\n";
        while ( list($fld_name, $fld_value) = each($rec) )
          {
            $htmlTable .= "\t\t<th bgcolor='#eeeeee'> $fld_name </th>\n";
          }
        $htmlTable .= "\t</tr>\n";
        while (!$this->EOF())
          {
            $rec = $this->Fields();
            $htmlTable .= "\t<tr>\n";
            while ( list($fld_name, $fld_value) = each($rec) )
              {
                $htmlTable .= "\t\t<td bgcolor='white'> $fld_value </td>\n";
              }
            $htmlTable .= "\t</tr>\n";
            $this->MoveNext();
          }
      }
    $htmlTable .= "
    </table>
  </td></tr>
</table>
";
        
    $this->MoveFirst();
    return $htmlTable;
  }
}
?>