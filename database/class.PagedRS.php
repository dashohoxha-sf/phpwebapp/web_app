<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


include_once DB_PATH."class.EditableRS.php";

/**
 * Same as class EditableRS, but gets from the DB only a certain
 * page out of all the results that can return the query. A page
 * is a group of consecutive records; all the pages have the same
 * number of records and they form a partition of the results.
 *
 * @package database
 */
class PagedRS extends EditableRS
{
  /**
   * How many recs will be read from DB for one page,
   * if 0 then all the recs will be read in one page 
   */
  var $recs_per_page;   

  /** Which page will be read (starting from 1). */
  var $current_page;

  /** Number of all the recs that can return the query. */
  var $nr_of_recs;


  function PagedRS($id, $query =UNDEFINED, $rp =UNDEFINED, $conn =UNDEFINED)
  {
    $this->EditableRS($id, $query, $conn);
    $this->type = "PagedRS";
    $this->recs_per_page = ($rp==UNDEFINED ? 0 : $rp);
    $this->current_page = $this->get_current_page();
  }

  /**
   * Get and return the state var
   * for the current page of this recordset.
   */
  function get_current_page()
  {
    $cp = WebApp::getSVar($this->ID."->current_page");
    if ($cp==UNDEFINED)
      {
        $cp = 1;
        WebApp::setSVar($this->ID."->current_page", $cp);
      }
    return $cp;
  }
        
  /** Executes the query and puts the result into $this->contents. */
  function Open($cp =UNDEFINED, $conn =UNDEFINED)
  {
    if ($this->opened)
      {
        $this->MoveFirst();
        return; //don't open it a second time
      }

    if ($cp==UNDEFINED)
      {
        //default is to get it from SVars
        $this->current_page = $this->get_current_page();
      }
    else
      {
        $this->current_page = $cp; 
      }
    $this->nr_of_recs = $this->get_nr_of_recs();
    $nr_of_pages = ceil($this->nr_of_recs / $this->recs_per_page);
    if ($this->current_page > $nr_of_pages)  $this->current_page = $nr_of_pages;
    if ($this->current_page < 1) $this->current_page = 1;
                                                                
    //modify and execute the query
    $offset = ($this->current_page - 1)*$this->recs_per_page;
    $query = WebApp::replaceVars($this->query);
    //$query .= " LIMIT $offset, ".$this->recs_per_page;
    if ($conn==UNDEFINED)
      {
	$this->cnn->setLimit($this->recs_per_page, $offset);
        $result = $this->cnn->query($query, $this->ID);
      }
    else
      {
	$conn->setLimit($this->recs_per_page, $offset);
        $result = $conn->query($query, $this->ID);
      }

    $this->contents = $result;
    $this->count = count($result);
    $this->pos = 0;
    $this->opened = true;
  }

  /**
   * Returns an array of some variables that can be used in the
   * template to indicate the next page, previous page, etc.
   */
  function getPageVars()
  {
    $nr_of_pages = ceil($this->nr_of_recs / $this->recs_per_page);
    if ($this->current_page>$nr_of_pages) $this->current_page=$nr_of_pages;

    $next_page = $this->current_page + 1;
    if ($next_page > $nr_of_pages)  $next_page = $nr_of_pages;

    $prev_page = $this->current_page - 1;
    if ($prev_page < 1)  $prev_page = 1;

    $first_rec = 1 + ($this->current_page - 1)*$this->recs_per_page;
    $last_rec   = $this->current_page * $this->recs_per_page;
    if ($first_rec > $this->nr_of_recs)  $first_rec = $this->nr_of_recs; 
    if ($last_rec  > $this->nr_of_recs)  $last_rec  = $this->nr_of_recs; 

    $page_vars = 
      array(
            //the number of the first record of the page
            "FirstRec" => $first_rec,
            //the number of the last record of the page               
            "LastRec"  => $last_rec,
            //the number of all records that can be retrieved by the query
            "AllRecs"  => $this->nr_of_recs,
            //current page of records that is retrieved
            "CurrPage" => $this->current_page,
            //the number of the next page
            "NextPage" => $next_page,
            //the number of the previous page
            "PrevPage" => $prev_page,
            //the number of the last page
            "LastPage" => $nr_of_pages
            );
    return $page_vars;
  }
        
  /**
   * Returns the total number of records that the query
   * of the recordset can return.
   */
  function get_nr_of_recs()
  {
    //get it from session
    $nrRecs = WebApp::getSVar($this->ID."->nrRecs");
    $recount = WebApp::getSVar($this->ID."->recount");

    //if the number of the records is undefined 
    //or the recount is requested explicitly by the application
    //find the number of recs by executing a COUNT query
    if ( ($nrRecs==UNDEFINED) or ($recount=="true") )
      {
        $query = WebApp::replaceVars($this->query);
        $query = $this->get_count_query($query);
        $rs = new Recordset($this->ID."_countQuery", $query);
        $rs->Open();
        $nrRecs = $rs->Field("COUNT_OF_RECS");

        //save it in session and turn off recounting
        WebApp::setSVar($this->ID."->nrRecs", $nrRecs);
        WebApp::setSVar($this->ID."->recount", "false");
      }

    return $nrRecs;
  }

  /**
   * From the query of the recordset, builds and returns a COUNT query
   * which gets the number of records of the original query.
   * Called by $this->get_nr_of_recs().
   * @see get_nr_of_recs()
   */
  function get_count_query($query)
  {
    $query = eregi_replace("[[:space:]]from[[:space:]]", " FROM ", $query);
    $from_pos = strpos($query, " FROM ");
    $before_from = substr($query, 0, $from_pos);
    $after_from = substr($query, $from_pos);

    if (eregi("ORDER +BY", $after_from))
      {
        $after_from = eregi_replace("ORDER +BY.*", "", $after_from);
      }

    if (eregi("GROUP +BY", $after_from))
      {
        $after_from = eregi_replace("ORDER +BY.*", "", $after_from);
        eregi("GROUP +BY(.*)", $after_from, $regs);
        $group_by_fields = $regs[1];
        $after_from = eregi_replace("GROUP +BY.*", "", $after_from);
        $count_query = "SELECT COUNT(DISTINCT $group_by_fields) AS COUNT_OF_RECS $after_from";
      }
    else if (eregi("SELECT +DISTINCT", $before_from))
      {
        $before_from = eregi_replace("SELECT +(DISTINCT.*)", "SELECT COUNT(\\1) AS COUNT_OF_RECS", $before_from);
        $count_query = $before_from.$after_from;
      }
    else
      {
        $count_query = "SELECT COUNT(1) AS COUNT_OF_RECS" . $after_from;
      }

    return $count_query;
  }
}
?>
