// -*-C-*-
/*
  This file  is part of phpWebApp,  which is a  framework for building
  web application based on relational databases.

  Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  phpWebApp is free software; you can redistribute it and/or modify it
  under the  terms of the GNU  General Public License  as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  phpWebApp is  distributed in  the hope that  it will be  useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along with phpWebApp; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * Session class keeps a list of variables.
 * These variables can be read, changed, etc.
 */
function Session()
{
  this.Vars = new Array();

  this.find = session_find;
  this.isset    = session_isset;
  this.addVar   = session_addVar;
  this.getVar   = session_getVar;
  this.setVar   = session_setVar;
  this.rmVar    = session_rmVar;
  this.toStr    = toStr;
}

/** 
 * Returns the index of a variable in the array
 * or -1 if there is no such variable in array.
 */
function session_find(var_name)
{
  var idx;
  for (idx=0; idx < this.Vars.length; idx++)
    if (this.Vars[idx].Name == var_name)
      return idx;   //found
  //not found
  return -1;
}

/**
 * Returns true if the given var_name egzists in the session,
 * false otherwise.
 */
function session_isset(var_name)
{
  var idx = this.find(var_name);
  if (idx == -1)  return false;
  else            return true;
}

function session_addVar(var_name, var_value)
{
  if (this.isset(var_name))
    {
      var msg = TF_("There is a 'v_var_name' already in the list, use setVar to change its value.");
      msg = msg.replace(/v_var_name/, var_name);
      alert("Session.addVar: " + msg);
    }
  else
    {
      var sess_var = {Name:var_name, Value:var_value};
      this.Vars.push(sess_var);
    }
}

function session_getVar(var_name)
{
  var idx = this.find(var_name);
  if (idx == -1)    //not found
    {
      var msg = TF_("'v_var_name' doesn't exist.");
      msg = msg.replace(/v_var_name/, var_name);
      alert("Session.getVar: " + msg);
      return "UNDEFINED";
    }
  else
    return this.Vars[idx].Value;
}

function session_setVar(var_name, var_value)
{
  var idx = this.find(var_name);
  if (idx == -1)    //not found
    {
      var msg = TF_("'v_var_name' doesn't exist.");
      msg = msg.replace(/v_var_name/, var_name);
      alert("Session.setVar: " + msg);
    }
  else
    this.Vars[idx].Value = var_value;
}

function session_rmVar(var_name)
{
  var idx = this.find(var_name);
  if (idx == -1)  //not found
    {
      var msg = TF_("'v_var_name' doesn't exist.");
      msg = msg.replace(/v_var_name/, var_name);
      alert("Session.rmVar: " + msg);
      return;
    }

  //remove it from the array
  this.Vars.splice(idx, 1);
}

/** Returns a string of all variables and their values concatenated. */
function toStr()
{
  var name, value;
  var strVars = "";
  var i;
  for (i=0; i < this.Vars.length; i++)
    {
      name = this.Vars[i].Name;
      value = this.Vars[i].Value;
      value = value.replace(/\n/g,"\\n");
      strVars += name + "=" + value + "&";
    }
  return strVars;
}
