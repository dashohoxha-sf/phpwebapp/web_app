<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * Used to keep session (state, persistent) variables.
   * @package state
   */
class Session
{
  /** Associated array that keeps the session vars. */
  var $Vars;

  /** True if the application uses a database. */
  var $uses_db;

  /** The connection to the database. */
  var $db_cnn;

  /** Associated array that keeps the DB session vars. */
  var $dbVars;


  function Session()
  {
    global $webapp_cnn;
    $this->db_cnn = $webapp_cnn;

    $this->uses_db = defined('MDB2_DSN');
    $this->Vars = array();
    $this->dbVars = array();

    global $request;
    if ($request->firstTime)
      {
        //new session
        $this->init();
      }
    else
      {
        $this->getVars($request->sessionVars);
        $this->getDBVars();
      }

    $this->extractPhpVars($request->phpVars);
  }

  /** Initialize a new session. */
  function init()
  {
    $ip = $_SERVER["REMOTE_ADDR"];
    $sess_id = "IP: $ip; DATE: ".date("Y-m-d H:i:s");
    $this->Vars["sess_id"] = $sess_id;

    if ($this->uses_db)  //create a new record for the session
      {
        //$this->create_table_session();

        //insert a new record for this session
        $vars = serialize($this->dbVars);
        $db_cmd = "INSERT INTO session (id, vars) "
          . "VALUES ('$sess_id', '$vars')";
        WebApp::sqlExec($db_cmd, $this->db_cnn);
      }
  }

  /**
   * Helper function that explodes the string given as parameter
   * and returns an associated array of var_name and var_value.
   * The format of $strVars is:  <kbd>var1=val1&var2=val2&...</kbd>
   */
  function str2arr($strVars)
  {
    $arrVars = array();
    $arr = explode("&", $strVars);
    while (list($i,$strVar) = each($arr))
      {
        if ($strVar=="")      continue;
        list($var_name,$var_value) = split("=", $strVar, 2);
        $arrVars[$var_name] = $var_value;
      }
    return $arrVars;
  }

  /**
   * Explodes the variable $sessVars
   * and fills the array $this->Vars with the session variables.
   * The format of $sessVars is:  <kbd>var1=val1&var2=val2&...</kbd>
   */
  function getVars($sessVars)
  {
    $arrVars = $this->str2arr($sessVars);
    while (list($var_name,$var_value) = each($arrVars))
      {
        $var_value = WebApp::js_decode($var_value);
        $this->addVar($var_name, $var_value);
      }
  }

  /** Get dbVars from DB. Requires the table session(id,vars) in DB. */
  function getDBVars()
  {
    if ($this->uses_db)
      {
        $id = $this->Vars["sess_id"];
        $query = "SELECT vars FROM session WHERE id='$id'";
        $rs = WebApp::sqlQuery($query, $this->db_cnn);
        $vars = $rs->Field("vars");
        $this->dbVars = unserialize($vars);
        if (!is_array($this->dbVars))  
          {
            //something went wrong
            $this->dbVars = array();
          }
      }
  }

  /**
   * Explodes $phpVars and declares some PHP global vars.
   * The format of $phpVars is:  <kbd>var1=val1&var2=val2&...</kbd>
   */
  function extractPhpVars($phpVars)
  {
    $arrVars = $this->str2arr($phpVars);
    while (list($var_name,$var_value) = each($arrVars))
      {
        //global PHP variable
        global $$var_name;
        $$var_name = $var_value;
      }
  }

  /**
   * Adds a new variable in the session.
   * The third parameter is optional and if it is true
   * then the variable is added as a DB (secure) variable.
   */
  function addVar($var_name, $var_value, $db =false)
  {
    if ($db)  //add DB var
      {
        if (isset($this->dbVars[$var_name]))
          {
            //this variable is already initialized
            return;
          }
        if (isset($this->Vars[$var_name]))
          {
            $msg = TF_("DB var 'v_var_name' shadows an existing JS var with the same name.");
            $msg = str_replace('v_var_name', $var_name, $msg);
            print WebApp::warning_msg("Session::addVar(): $msg");            
          }
        $this->dbVars[$var_name] = $var_value;
      }
    else   //add JS var
      {
        if (isset($this->dbVars[$var_name]))
          {
            $msg = TF_("JS var 'v_var_name' is shadowed by an existing DB var with the same name.");
            $msg = str_replace('v_var_name', $var_name, $msg);
            print WebApp::warning_msg("Session::addVar(): $msg");
          }
        if (isset($this->Vars[$var_name]))
          {
            //this variable is already initialized
            return;
          }
        $this->Vars[$var_name] = $var_value;
      }
  }

  /** Removes a variable from the session. */
  function rmVar($var_name)
  {
    unset($this->Vars[$var_name]);
    unset($this->dbVars[$var_name]);
  }

  /**
   * Sets a new value for a variable;
   * DB vars have priority over JS vars.
   */
  function setVar($var_name, $var_value)
  {
    if (isset($this->dbVars[$var_name]))
      {
        $this->dbVars[$var_name] = $var_value;
      }
    else
      {
        $this->Vars[$var_name] = $var_value;
      }
  }

  /**
   * Returns the value of a variable, or UNDEFINED
   * if such a variable does not exist.
   * DB vars have priority over JS vars.
   */
  function getVar($var_name)
  {
    if (isset($this->dbVars[$var_name]))
      {
        $var_value = $this->dbVars[$var_name];
      }
    else if (isset($this->Vars[$var_name]))
      {
        $var_value = $this->Vars[$var_name];
      }
    else
      {
        $var_value = UNDEFINED;
      }

    return $var_value;
  }

  /**
   * Get all the state variables of a webobject
   * and return them as an associative array.
   * The optional parameter $type accepts the values "DB"
   * and "JS", and depending on this parameter the function
   * returns DB vars, JS vars or all of them (if undefined).
   */
  function getObjVars($obj_id, $type =UNDEFINED)
  {
    switch ($type)
      {
      case "DB":
        $arr_vars = $this->dbVars;
        break;
      case "JS":
        $arr_vars = $this->Vars;
        break;
      default:
        print WebApp::warning_msg("WebObject::getSVars(): unreckognized \$type '$type'.");
      case UNDEFINED:
        $arr_vars = array_merge($this->Vars, $this->dbVars);
        break;
      }

    $stateVars = array();
    $pattern = "^".$obj_id."->(.*)";
    while (list($var_name,$var_value) = each($arr_vars))
      {
        if (ereg($pattern, $var_name, $regs))
          {
            $v_name = $regs[1];
            $stateVars[$v_name] = $var_value;
          }
      }

    return $stateVars;
  }

  /**
   * Builds and returns the variable $session_vars which is used to insert
   * the session vars inside the page as JavaScript code.
   */
  function to_JS()
  {
    $session_vars = '<script type="text/javascript" language="javascript">'
      . "\n//<![CDATA[\n"
      . "  session = new Session();\n";
    reset($this->Vars);
    //add each variable of the session
    while (list($var_name,$var_value) = each($this->Vars))
      {
        $var_value = WebApp::js_encode($var_value);
        $session_vars .= "  session.addVar('$var_name','$var_value');\n";
      }
    $session_vars .= "//]]>\n"
      . "</script>";

    return $session_vars;
  }

  /** Save dbVars in DB. */
  function saveDBVars()
  {
    if ($this->uses_db)
      {
        $id = $this->Vars["sess_id"];
        $vars = serialize($this->dbVars);
        $query = "UPDATE session SET vars='$vars' WHERE id='$id'";
        WebApp::sqlExec($query, $this->db_cnn);
      }
    else
      {
        //no DB, no DB connection
        if (sizeof($this->dbVars) > 0)
          {
            //convert DB vars to JS vars
            $this->Vars = array_merge($this->Vars, $this->dbVars);
            $this->dbVars = array();
            //give a warning
            $msg = TF_("session DB vars saved as JS vars, there is no DB \
connection, check 'config/const.Options.php'.");
            print WebApp::warning_msg($msg);
          }
      }
  }

  /**
   * Builds an HTML table containing the session vars
   * and returns it as a string (for debug).
   */
  function to_HTML_table()
  {
    //build the table of vars
    $html = "
<br />
<a name='session' id='session' />[<a href='tree'>Top</a>]
<table bgcolor='#aaaaaa' border='0' cellspacing='1' cellpadding='2'>
  <tr>
    <td bgcolor='#eeeeff' colspan='3'>Session</td>
  </tr>
  <tr>
    <td bgcolor='#eeeeee'><span class='h_space'/></td>
    <td bgcolor='#eeeeee'>var_name</td>
    <td bgcolor='#eeeeee'>var_value</td>
  </tr>";

    //add JS vars
    reset($this->Vars);
    while (list($var_name,$var_value) = each($this->Vars))
      {
        //don't display the box state vars
        if (strpos($var_name, "->")===false)
          {
            $html .= "
  <tr>
    <td bgcolor='#f9f9f9'><span class='h_space'/></td>
    <td bgcolor='#f9f9f9'>$var_name</td>
    <td bgcolor='#f9f9ff'>$var_value</td>
  </tr>";
          }
      }

    //add DB vars
    reset($this->dbVars);
    while (list($var_name,$var_value) = each($this->dbVars))
      {
        //don't display the box state vars
        if (strpos($var_name, "->")===false)
          {
            $html .= "
  <tr>
    <td bgcolor='#f9f9f9'>DB</td>
    <td bgcolor='#f9f9f9'>$var_name</td>
    <td bgcolor='#f9f9ff'>$var_value</td>
  </tr>";
          }
      }
    $html .= "</table>";

    return $html;
  }

  /**
   * The table of the session should be created with this query:
   *
   * CREATE TABLE session (
   *     id    VARCHAR(255) NOT NULL,
   *     vars  TEXT NOT NULL,
   *     PRIMARY KEY (id)
   *   );
   *
   * No need to create it automatically.
   *
   * --------------------------------------
   *
   * Create the table 'session' in database,
   * if such a table does not exist.
   */
  /*
  function create_table_session()
  {
    global $webapp_cnn;

    //get the name of the database
    $query = "SELECT DATABASE()";
    $rs = WebApp::sqlQuery($query, $this->db_cnn);
    $db_name = $rs->Field("DATABASE()");

    //get a list of the tables in this database
    $query = "SHOW TABLES";
    $rs = WebApp::sqlQuery($query, $this->db_cnn);

    //search for the table 'session'
    $found = false;
    $table_name = "Tables_in_".$db_name;
    while (!$rs->EOF())
      {
        $tab_name = $rs->Field($table_name);
        if ($tab_name=="session")
          {
            $found = true;
            break;
          }
        $rs->MoveNext();
      }
    if ($found)  return; //do nothing
          
    //not found, create the table 'session'
    $db_cmd = "CREATE TABLE 
                      session 
                      (
                          id      VARCHAR(255) NOT NULL,
                          vars    TEXT NOT NULL,
                                  PRIMARY KEY (id)
                      )";
    WebApp::sqlExec($db_cmd, $this->db_cnn);
  }
  */
}
?>
