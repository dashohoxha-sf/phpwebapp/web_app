<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * Includes the session inside the HTML page.
   * @package state
   */
class wbSession extends WebObject
{
  function onParse()
  {
    global $webPage;

    $session_url = WebApp::to_url(SESSION_PATH);
    $func_goto = '  <script type="text/javascript" language="javascript" '
      . "src=\"${session_url}func.GoTo.js\"></script>\n";
    $webPage->append_to_head($func_goto);

    $webPage->extract_js_l10n_messages(SESSION_PATH."class.Session.js");
    //$webPage->extract_js_l10n_messages(SESSION_PATH."func.GoTo.js");
  }

  function onRender()
  {
    global $session, $event;  

    //save dbVars in DB
    $session->saveDBVars();

    //add the variable {{method}}
    $method = (DISPLAY_CGI_VARS ? "get" : "post");
    WebApp::addVar("method", $method);

    WebApp::addVar("sourcePage", $event->targetPage);

    //add the template variable {{SESSION_VARS}} 
    //which embeds the session into the page
    WebApp::addVar("SESSION_VARS", $session->to_JS());
  }
}
?>