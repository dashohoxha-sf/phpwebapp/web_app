// -*-C-*-
/*
  This file  is part of phpWebApp,  which is a  framework for building
  web application based on relational databases.

  Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  phpWebApp is free software; you can redistribute it and/or modify it
  under the  terms of the GNU  General Public License  as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  phpWebApp is  distributed in  the hope that  it will be  useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along with phpWebApp; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * Send an event to the server-side (PHP) code.
 * @param obj_id string  The object to which the event is sent
 * @param event_name string The name of the event
 * @param event_args string The arguments of the event, optional
 * @param gate (optional) is the script through which the event is sent;
 *        if missing, then it is the same as the current url;
 *        if 'index' then the file part is removed from the current url;
 * @see GoTo()
 */
function SendEvent(obj_id, event_name, event_args, gate)
{
  if (event_args==null)  event_args = "";
  else  event_args = "(" + event_args + ")";

  GoTo("thisPage?event=" + obj_id + "." + event_name + event_args, gate);
}

/**
 * This function sends the var_name (which has the given var_value)
 * to the server side as a PHP global variable. It can also be used
 * in the template as a tpl variable: {{var_name}}, since all PHP
 * global variables can be used as tpl variables. It should be used
 * carefully, because any tpl variable with the same name will 
 * override its value.
 * It must be called before GoTo().
 */
function transmitVar(var_name, var_value)
{
  var webAppForm = document.WebAppForm;
  webAppForm.phpVars.value += "&" + var_name + "=" + var_value;
}

/**
 * Similar to transmitVar() but can transmit more than one variable.
 * The format of var_list is: var1=val1&var=val2&var3=val3.
 * Must be called before GoTo().
 */
function transmitVars(var_list)
{
  var webAppForm = document.WebAppForm;
  webAppForm.phpVars.value += "&" + var_list;
}

/** 
 * Replace the semicolons ';' in the given string 
 * by '#semicolon#' and return the result. This is done to send
 * an event arg that contains ';' (because the semicolons
 * are used to separate the event args from each-other).
 */
function encode_arg_value(str)
{
  str = str.replace(/;/g, '#semicolon#');
  return str;
}

/** 
 * This variable is made true when the programer makes 'true'
 * the DEBUG_GOTO constant in 'const.Settings.php'.
 */
var debug_GoTo = false;

/**
 * Submits the form of the page to the 'action' specified as parameter
 * by taking care to serialize and transmit the session vars as well.
 *
 * @param target is something like this: 
 *               "page1.html?event=list.add(event_args)"
 *               target_page and event are separated by '?';
 *               if target_page is 'thisPage' then the target 
 *               is the same as the source.
 * @param action (optional) is the action to which the form is submitted;
 *               if missing, then it is the same as the current url;
 *               if 'index' then the file part is removed from the current url;
 */
function GoTo(target, action)
{
  var idx, target_page, event;
  var form, href, app_href;

  //debug
  if (debug_GoTo==true) alert("GoTo('"+target+"')");

  //set the action of the form
  if (action==null)  action = location.href;
  if (action=='index')
    {
      href = location.href;
      action = href.replace(/[^\/\?]+(\?.*)?$/, '');
    }

  //the 'target' parameter is something like this:
  //"page1.html?event=list.add(event_args)"
  //target_page and event are separated by '?'
  idx = target.indexOf("?");
  if (idx==-1)
    {
      target_page = target;
      event = "";
    }
  else
    {
      target_page = target.slice(0,idx);
      event = target.slice(idx+1);
    }
    
  //fill the form inputs
  form = document.WebAppForm;   
  form.sessionVars.value = session.toStr();
  form.strEvent.value = event;
  if (target_page=="" || target_page=="thisPage")
    {
      form.targetPage.value = form.sourcePage.value;
    }
  else
    {
      form.targetPage.value = target_page;
    }
    
  //now submit the form
  form.action = action;
  form.submit();
}

/**
 * Same as SendEvent, but display the result in the given window.
 * @param win_name string  The name of the window where the output will be
 *        displayed (created e.g. by window.open(url, win_name, features))
 * @param gate (optional) is the script through which the event is sent;
 *        if missing, then it is the same as the current url;
 *        if 'index' then the file part is removed from the current url;
 * @see SendEvent()
 */
function wSendEvent(win_name, obj_id, event_name, event_args, gate)
{
  document.WebAppForm.target = win_name;
  SendEvent(obj_id, event_name, event_args, gate);
  document.WebAppForm.target = '';
}

/**
 * Same as GoTo, but display the result in the given window.
 * @param win_name string  The name of the window where the output will be
 *        displayed (created e.g. by window.open(url, win_name, features))
 * @see GoTo()
 */
function wGoTo(win_name, target, action)
{
  document.WebAppForm.target = win_name;
  GoTo(target, action);
  document.WebAppForm.target = '';
}
