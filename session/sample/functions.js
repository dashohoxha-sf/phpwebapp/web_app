//-*- mode: C; -*-//tells emacs to use mode C for this file
/*
  This file  is part of phpWebApp,  which is a  framework for building
  web application based on relational databases.

  Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
  Dashamir Hoxha, dashohoxha@users.sourceforge.net

  phpWebApp is free software; you can redistribute it and/or modify it
  under the  terms of the GNU  General Public License  as published by
  the Free  Software Foundation; either  version 2 of the  License, or
  (at your option) any later version.

  phpWebApp is  distributed in  the hope that  it will be  useful, but
  WITHOUT  ANY   WARRANTY;  without  even  the   implied  warranty  of
  MERCHANTABILITY or  FITNESS FOR A  PARTICULAR PURPOSE.  See  the GNU
  General Public License for more details.

  You should  have received a copy  of the GNU  General Public License
  along with phpWebApp; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function on_Add()
{
  alert("Add clicked");

  //do input validation and other processing here
  var form = document.addVarForm;
  var var_name = form.var_name.value;
  var var_value = form.var_value.value;
  if (var_name!="") session.addVar(var_name, var_value);

  //now make the request for the next page
  GoTo("test.Session.php?event=nothing&test1=value 1&test2=value 2&arr[]=val1&arr[]=val2");
}

function on_Back()
{
  alert("Back clicked");
  history.go(-1);
}

function on_GoTo(page)
{
  alert("GoTo clicked");
  //do some client side processing here

  //make a request to the server
  GoTo(page);
}
