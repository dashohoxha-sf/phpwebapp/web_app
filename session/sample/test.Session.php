<?php
  /** @package state */
  /** */

$session_path = "../";
include $session_path."class.Session.php";
$session = new Session;
$SESSION = $session->to_JS($session_path);
$SESSION_VARS = $session->to_HTML_table();              //for debug
?>

<html>
<head>
<script type="text/javascript" language="javascript" src="functions.js"></script>
</head>

<body>
<!-- <?echo $SESSION?> -->

<!-- this hidden input does not allow the page to be cached -->
<input type="hidden" name="timestamp" id="timestamp" value="<?echo time()?>">

  <h1> Page: test.Session.php </h1>
<h3> Timestamp: <?echo date("d/m/Y H:i:s")?> </h3>

<form name="addVarForm" id="addVarForm">
  Add a new variable to the session:<br />

var_name : <input type="text" name="var_name" id="var_name" value=""><br />
var_value: <input type="text" name="var_value" id="var_value" value=""><br />

<a href="JavaScript:on_Add()">
  <img src="add.png" border="0" />
  </a>
</form>
<p>
<a href="JavaScript:on_Back()">
  <img src="back.png" border="0" />
  </a>
<p>
<a href="JavaScript:on_GoTo('test.Session.php')">
  GoTo('test.Session.php')
  </a>

<p>
<b>DEBUG:</b><br />
<b><?print "\$sessionVars = '$sessionVars'";?></b><br />
<b><?print "\$phpVars = '$phpVars'";?></b><br />
<b><?print "\$event = '$event'";?></b><br />
<b><?print "\$test1='".$test1."'";?></b><br />
<b><?print "\$test2='".$test2."'";?></b><br />
<b><?print "\$arr[0]='".$arr[0]."'";?></b><br />
<b><?print "\$arr[1]='".$arr[1]."'";?></b><br />
<p>
<!-- display session vars for debug -->
<?phpecho $SESSION_VARS?>

</body>
</html>