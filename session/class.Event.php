<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * Used to keep the data of an event.
   * @package state
   * @subpackage transition
   */
class Event
{
  /** The page that sent the event. */
  var $sourcePage;

  /** The page that should receive the event. */
  var $targetPage;
        
  /** The name of the event. */
  var $name;

  /** The target webobject. */
  var $target;

  /** Array that keeps the arguments of the event. */
  var $args;

        
  /** Constructs an event from an event string. */
  function Event()
  {
    global $request;
                
    $this->sourcePage = $request->sourcePage;
    $this->targetPage = $request->targetPage;
                
    $this->parse($request->strEvent);
  }
        
  /**
   * An event string has this format:
   * <kbd>event=target.name(arg1=val1;arg2=val2;arg3=val3)</kbd>
   */
  function parse($strEvent)
  {
    if ($strEvent==UNDEFINED)  $strEvent = "";
    list($x, $event_str) = split("=", $strEvent, 2);
    //extract event name and event args from $event_str
    $args_pos = strpos($event_str, "(");
    if ($args_pos)
      {
        $event_name = substr($event_str, 0, $args_pos);
        $event_args = substr($event_str, $args_pos);
      }
    else
      {
        $event_name = $event_str;
        $event_args = "";
      }
                
    //extract event target and event name from $event_name
    $dot_pos = strpos($event_name, ".");
    if ($dot_pos)
      {
        $this->target = substr($event_name, 0, $dot_pos);
        $this->name = substr($event_name, $dot_pos+1);
      }
    else
      {
        $this->target = UNDEFINED;
        $this->name = $event_name;
      }
                
    //put the arguments in $this->args
    $this->args = array();
    if ($event_args<>"")
      {
        //strip off parantheses
        $event_args = substr($event_args, 1, strlen($event_args)-2);

        $args = explode(";", $event_args);
        while (list($i,$arg) = each($args))
          {
            if ($arg=="")     continue;
            list($arg_name,$arg_value) = split("=", $arg, 2);
            $arg_value = $this->decode_arg_value($arg_value);
            $this->args[$arg_name] = $arg_value;
          }
      }
  }

  function decode_arg_value($str)
  {
    $str = str_replace('#semicolon#', ';', $str);
    $str = WebApp::js_decode($str);
    return $str;
  }
        
  /**
   * Returns an array of all the arguments of the event,
   * only their values.
   */
  function getArgs()
  {
    while ( list($arg_name,$arg_value) = each($this->args) )
      {
        $arg_values[] = $arg_value;
      }
    return $arg_values;
  }
        
  /** Prints out the event (for debug). */
  function to_text()    
  {
    print "Name: $this->name\n";
    print "Target: $this->target\n";
    print "Args: ";
    while (list($arg_name,$arg_value) = each($this->args))
      {
        print "$arg_name=$arg_value;";
      }
    print "\n\n";
  }
}
?>