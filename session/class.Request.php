<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * Keeps the information comming from the browser.
   * @package state
   * @subpackage transition
   */
class Request
{
  //these variables have the state of the application for this page
  var $sourcePage;
  var $targetPage;
  var $sessionVars;
  var $strEvent;
  var $phpVars;

  /** Is true only for the first page in the session. */
  var $firstTime;

  function Request()
  {
    $browser_vars = (DISPLAY_CGI_VARS ? $_GET : $_POST);

    $this->sourcePage  = ( isset($browser_vars["sourcePage"]) ?
                           $browser_vars["sourcePage"] : UNDEFINED );
    $this->targetPage  = ( isset($browser_vars["targetPage"]) ?
                           $browser_vars["targetPage"] : UNDEFINED );
    $this->sessionVars = ( isset($browser_vars["sessionVars"]) ?
                           $browser_vars["sessionVars"] : UNDEFINED );
    $this->strEvent    = ( isset($browser_vars["strEvent"]) ?
                           $browser_vars["strEvent"] : UNDEFINED );
    $this->phpVars     = ( isset($browser_vars["phpVars"]) ?
                           $browser_vars["phpVars"] : UNDEFINED );
    $this->firstTime = ($this->targetPage==UNDEFINED ? true : false);
  }
}
?>