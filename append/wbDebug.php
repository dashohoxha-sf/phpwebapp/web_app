<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */

  /**
   * @package misc
   * @subpackage append
   */
class wbDebug extends WebObject
{
  function onRender()
  {
    global $webPage;

    $debug_goto = '
<script type="text/javascript" language="javascript">
//<![CDATA[
  debug_GoTo = true;
//]]>
</script>
';
    WebApp::addVar("debug_goto", $debug_goto);

    $webPage->print_dbg_messages();

    $debug_recordsets = (DEBUG_RECORDSETS ? $webPage->recordsets_to_html() : "");
    WebApp::addVar("debug_recordsets", $debug_recordsets);

    $webPage->timer->Stop("WebApp");
    $timer_results = (EXECUTION_TIME_INFO ? $webPage->timer->toHtmlTable() : "");
    WebApp::addVar("timer_results", $timer_results);
  }
}
?>