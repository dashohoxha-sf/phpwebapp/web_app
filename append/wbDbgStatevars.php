<?php
  /*
   This file is  part of phpWebApp, which is  a framework for building
   web application based on relational databases.

   Copyright 2001, 2002, 2003, 2004, 2005, 2006, 2007
   Dashamir Hoxha, dashohoxha@users.sourceforge.net

   phpWebApp is  free software; you can redistribute  it and/or modify
   it under the  terms of the GNU General  Public License as published
   by the Free  Software Foundation; either version 2  of the License,
   or (at your option) any later version.

   phpWebApp is  distributed in the hope  that it will  be useful, but
   WITHOUT  ANY  WARRANTY;  without   even  the  implied  warranty  of
   MERCHANTABILITY or  FITNESS FOR A PARTICULAR PURPOSE.   See the GNU
   General Public License for more details.

   You should have  received a copy of the  GNU General Public License
   along  with   phpWebApp;  if  not,  write  to   the  Free  Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
  */


  /**
   * @package misc
   * @subpackage append
   */
class wbDbgStatevars extends WebObject
{
  function onRender()
  {
    if (!DEBUG_STATEVARS)  return;

    print $this->webobjects_to_tree();
    print $this->webobjects_to_html();
  }

  function webobjects_to_tree()
  {
    $tree = "<hr />\n";
    $tree .= "<a name='tree' id='tree'/>";
    $tree .= "<strong>WebObjects and their state variables:</strong>\n";
    $tree .= "<pre>";

    global $webPage;
    $id = $webPage->rootTpl->id;
    $tpl = $webPage->rootTpl->body;
    $indent = "  ";
    $tree .= $indent."+--><a href='#session'>".$id."</a>\n";

    $arr_tpl_id = $tpl->get_subtemplates();
    for ($i=0; $i < sizeof($arr_tpl_id); $i++)
      {
        $tpl_id = $arr_tpl_id[$i];

        //don't display the webobjects of append
        if (strpos('append.html', $tpl_id))
          {
            $tree .= "</pre>\n";
            return $tree;
          }

        $subtpl = $webPage->getTemplate($tpl_id);
        //increase the indentation if the template is a webobject
        $indent_1 = ($subtpl->type=="WebObjectTpl") 
          ? $indent . "|   " 
          : $indent;
        $tree .= $this->webobj_to_tree($subtpl, $indent_1);
      }
    $tree .= "</pre>\n";
    return $tree;
  }

  /** returns the structure of the parsed objects as a tree */
  function webobj_to_tree($tpl, $indent)
  {
    global $webPage;
                
    //display only if it is a webobject template
    if ($tpl->type=="WebObjectTpl")
      {
        $tree = $indent."|\n";
        $tree .= $indent."+--><a href='#".$tpl->id."'>".$tpl->id."</a>\n";
      }

    $arr_tpl_id = $tpl->get_subtemplates();
    for ($i=0; $i < sizeof($arr_tpl_id); $i++)
      {
        $tpl_id = $arr_tpl_id[$i];
        $subtpl = $webPage->getTemplate($tpl_id);
        //increase the indentation if the template is a webobject
        $indent_1 = ( ($subtpl->type=="WebObjectTpl") ? 
                      $indent . "|   " 
                      : $indent );
        $tree .= $this->webobj_to_tree($subtpl, $indent_1);
      }
    return $tree;
  }

  function webobjects_to_html()
  {
    global $session, $webPage;

    $html = $session->to_HTML_table();
    reset($webPage->tpl_collection);
    while ( list($tplId,$tpl) = each($webPage->tpl_collection) )
      {
        //don't display the webobjects of append
        if (ereg('web_app/append/append.html$', $tplId))
          {
            return $html;
            $html .= "<hr />\n";
          }

        if ($tpl->type=="WebObjectTpl")
          {
            $html .= $tpl->state2html();
          }
      }
    $html .= "<hr />\n";
    return $html;
  }
}
?>